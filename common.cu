#include "common.h"

cudaDeviceProp deviceProp;

inline void check_cuda_errors(const char *filename, const int line_number)
{

  cudaThreadSynchronize();
  cudaError_t error = cudaGetLastError();
  if(error != cudaSuccess)
  {
    printf("CUDA error at %s:%i: %s\n", filename, line_number, cudaGetErrorString(error));
    exit(-1);
  }

}

// COMMON kernels
void get_name(char* full_name, char* &mat_name){
	std::string name(full_name);

	int first_slash_index = 0;
	for (int i = name.size()-1; i >= 0; i--)
	{
		if(name[i] == '/'){
			first_slash_index = i;
			break;
		}
	}

	if(first_slash_index == 0){
		mat_name = full_name;
		return;
	}

	int fin_length = name.size() - first_slash_index - 1;
	mat_name = (char*) malloc(sizeof(char)*(fin_length+1));

	for (int i = 0; i < fin_length; ++i)
	{
		mat_name[i]	 = name[first_slash_index+1+i];
	}
	mat_name[fin_length] = '\0';

}

// Fills an array with the given value.
__global__
void fill_array_with_value_kernel(int* array , int value, int size){
	int tid = (blockIdx.z*(gridDim.x*gridDim.y) + blockIdx.y*(gridDim.x) + blockIdx.x) * blockDim.x + threadIdx.x;
	if(tid < size){
		array[tid] = value;
	}
}

__global__
void fill_array_with_tid_kernel(int* indices, int size){
	int tid = (blockIdx.z*(gridDim.x*gridDim.y) + blockIdx.y*(gridDim.x) + blockIdx.x) * blockDim.x + threadIdx.x;
	if(tid < size){
		indices[tid] = tid;
	}
}

// Fills an array with tid+base_offset value.
__global__
void fill_array_with_tid_plus_base_offset(int* offsets, int base_offset, int size){
	int tid = (blockIdx.z*(gridDim.x*gridDim.y) + blockIdx.y*(gridDim.x) + blockIdx.x) * blockDim.x + threadIdx.x;
	if(tid < size){
		offsets[tid] = tid + base_offset;
	}
}

__global__
void set_zero_for_unset_flag_in_psum(int* psum_array, int* index_array, int size){
	int tid = (blockIdx.z*(gridDim.x*gridDim.y) + blockIdx.y*(gridDim.x) + blockIdx.x) * blockDim.x + threadIdx.x;
	if(tid > 0 && tid < size){
		if(psum_array[tid] != psum_array[tid-1]){
			index_array[tid] = psum_array[tid];
		}
	}
}

__global__
void multiply_array_by_minus_one(int* array,int size){
	int tid = (blockIdx.z*(gridDim.x*gridDim.y) + blockIdx.y*(gridDim.x) + blockIdx.x) * blockDim.x + threadIdx.x;
	if(tid < size){
		array[tid] = -1*array[tid];
	}

}

__global__
void mark_flags(int* orig_array, int* bool_array, int size){
	int tid = (blockIdx.z*(gridDim.x*gridDim.y) + blockIdx.y*(gridDim.x) + blockIdx.x) * blockDim.x + threadIdx.x;
	if(tid < size){
		if(orig_array[tid] != 0)
			bool_array[tid] = 1;
	}
}

__global__
void fill_values_with_requested_indices(int* indices, int* read_values, int* write_values, int size){
	int tid = (blockIdx.z*(gridDim.x*gridDim.y) + blockIdx.y*(gridDim.x) + blockIdx.x) * blockDim.x + threadIdx.x;
	if(tid < size){
		write_values[tid] = read_values[indices[tid]];
	}

}

__global__
void trasnspose_band_data_inplace(float* d_orig_data, float* d_new_data, int* d_orig_offsets, int rows, int size){
	int tid = (blockIdx.z*(gridDim.x*gridDim.y) + blockIdx.y*(gridDim.x) + blockIdx.x) * blockDim.x + threadIdx.x;

	int index = tid/rows;
	int row = tid%rows;
	int offset, dest_row;

	if(tid < size){
		offset = d_orig_offsets[index];
		dest_row = row+offset;
		if(dest_row >= 0 && dest_row < rows ){
			d_new_data[index*rows + dest_row] = d_orig_data[index*rows + row];
		}
	}

}

void transpose_band_matrix(band_matrix &band_mat){
	dim3 grids,blocks;
	int temp;

	// Exchanding rows and cols
	temp = band_mat.rows;
	band_mat.rows = band_mat.cols;
	band_mat.cols = temp;

	// Exchangin left and right offsets
	temp = band_mat.left_offset;
	band_mat.left_offset = -1*band_mat.right_offset;
	band_mat.right_offset = -1*temp;

	float* d_new_data;
	allocate_and_memset_device_float_array(&d_new_data, band_mat.count * band_mat.rows);
	
	find_dimensions_of_blocks_and_threads(band_mat.count * band_mat.rows, grids, blocks);
	trasnspose_band_data_inplace<<<grids,blocks>>>(band_mat.data, d_new_data, band_mat.offsets, band_mat.rows, band_mat.count * band_mat.rows);

	cudaDeviceSynchronize();
	cudaFree(band_mat.data); // Freeing the memory
	cudaDeviceSynchronize();

	// changing the pointer to new data
	band_mat.data = d_new_data;

	find_dimensions_of_blocks_and_threads(band_mat.count, grids, blocks);
	multiply_array_by_minus_one<<<grids,blocks>>>(band_mat.offsets,band_mat.count);

	cudaDeviceSynchronize();

}

// SPLITTING related
__global__
void find_nnz_in_dia_kernel(int* row_indices, int* column_indices, int rows, int* nnz_in_dia,  int size){

	int tid = (blockIdx.z*(gridDim.x*gridDim.y) + blockIdx.y*(gridDim.x) + blockIdx.x) * blockDim.x + threadIdx.x;
	
	if(tid < size){
		int offset = column_indices[tid] - row_indices[tid];
		atomicAdd(&nnz_in_dia[offset+rows-1],1);

	}
}

// count of elements in the diagonals
void find_total_in_dia(int* total_in_dia, int rows, int cols){

	int start = 0;
	int end = rows+cols-2;

	// filling the symmetric portion
	int runner = min(rows,cols);
	for(int i=0;i<runner;i++){
		total_in_dia[start] = i+1;
		total_in_dia[end] = i+1;
		start++;
		end--;
	}
	
	// filling the middle portion
	while(start <= end){
		total_in_dia[start] = runner;
		total_in_dia[end] = runner;
		start++;
		end--;
	}
}

void pluck_band_using_cumulative_method(int* nnz_in_dia, int* total_in_dia, int A_rows, int A_cols, int &l_index, int &r_index, float FILL_PERC){
	/*
		In this approach we start from the main diagonal and traverse 
		towarse left corner and stop when we find the cumulative fill
		percentage is less than FILL_PERC
	*/
	int zero_index = A_rows - 1;
	int left_offset = 0;
	
	float ratio;
	int sum_numerator = nnz_in_dia[zero_index];
	int sum_denominator = total_in_dia[zero_index];
	// finding left index of band
	for(int i=zero_index-1;i>=0;i--){
		sum_numerator += nnz_in_dia[i];
		sum_denominator += total_in_dia[i];

		// In case of dia where nnz = 0 Check whether the next dia has some elements
		if(nnz_in_dia[i] == 0){
			if(! (i!=0 && nnz_in_dia[i-1] != 0)){
				left_offset = i+1;
				break;
			}
		}
		ratio = ((float)sum_numerator)/sum_denominator;
		if(ratio <= FILL_PERC){
			left_offset = i+1;
			break;
		}

	}
	left_offset -= (A_rows-1);
	
	// finding right index of band
	int right_offset = A_rows + A_cols-2;
	sum_numerator = nnz_in_dia[zero_index];
	sum_denominator = total_in_dia[zero_index];
		
	for(int i=zero_index+1;i<A_rows+A_cols-1;i++){
		sum_numerator += nnz_in_dia[i];
		sum_denominator += total_in_dia[i];

		// In case of dia where nnz = 0 Check whether the next dia has some elements
		if(nnz_in_dia[i] == 0){
			if(! (i!=A_rows+A_cols-2 && nnz_in_dia[i+1] != 0)){
				right_offset = i-1;
				break;	
			}
		}
		ratio = ((float)sum_numerator)/sum_denominator;
		if(ratio <= FILL_PERC){
			right_offset = i-1;
			break;
		}
	}
	right_offset -= (A_rows-1);

	// setting the left and right of band
	l_index = left_offset;
	r_index = right_offset;
}

void pluck_band_using_stencil_average_method(int* nnz_in_dia, int* total_in_dia, int A_rows, int A_cols, int &l_index, int &r_index, float FILL_PERC){
	/*
		For each dia we find cumulative fill percentage of current, left and right
	*/
	int zero_index = A_rows - 1;
	int left_offset = 0;
	
	int cur_cum_numerator;
	float cur_cum_perc, cur_perc;

	// finding left index of band
	for(int i=zero_index-1;i>=0;i--){
		// (i+1) because it is through and it lifts the current one
		// (i-1) becuase if i sucks but by including (i-1) required average it maintaine then ok right.

		cur_cum_numerator = nnz_in_dia[i-1] + nnz_in_dia[i] + nnz_in_dia[i+1];
		cur_cum_perc = ((float) cur_cum_numerator)/ (3*A_rows);
		cur_perc = (float) (nnz_in_dia[i])/A_rows;

		// In case of dia where nnz = 0 Check whether the next dia has some elements
		if(cur_cum_perc <= FILL_PERC && (cur_perc < FILL_PERC)){
			left_offset = i+1;
			break;
		}

	}
	left_offset -= (A_rows-1);
	
	// finding right index of band
	int right_offset = A_rows + A_cols-2;
		
	for(int i=zero_index+1;i<A_rows+A_cols-1;i++){
		cur_cum_numerator = nnz_in_dia[i-1] + nnz_in_dia[i] + nnz_in_dia[i+1];
		cur_cum_perc = ((float) cur_cum_numerator)/ (3*A_rows);
		cur_perc = (float) (nnz_in_dia[i])/ A_rows;

		// In case of dia where nnz = 0 Check whether the next dia has some elements
		if(cur_cum_perc <= FILL_PERC && (cur_perc < FILL_PERC)){
			right_offset = i-1;
			break;
		}
	}
	right_offset -= (A_rows-1);

	// setting the left and right of band
	l_index = left_offset;
	r_index = right_offset;
}

int find_bands_in_a_matrix(int* nnz_in_dia, int* total_in_dia, int rows, int cols, int nnz, int* &band_starts, int* &band_counts, float BAND_OCCUPANCY){

	int DEBUG = 0;

	// Find all diagonals that are greater than BAND_OCCUPANCY
	int all_dia_count = rows+cols-1;
	int base_offset = rows-1;
	
	std::vector<std::pair<float,int> > q_dia;	

	// Finding number of non-zero diagonals
	float dia_perc;
	int nnz_diagonals = 0;
	for (int i = 0; i < all_dia_count; ++i)
	{	
		if(nnz_in_dia[i] != 0){
			nnz_diagonals++;

			// here dia_perc denominator is rows because we don't  local occupancy.
			dia_perc = ((float)nnz_in_dia[i]/rows)*100;
			if(dia_perc >= BAND_OCCUPANCY){
				std::pair<float,int> temp(dia_perc, i);
				q_dia.push_back(temp);
			}
		}
	}

	// sorting by the dias with max in front.
	std:sort(q_dia.rbegin(), q_dia.rend());

	// set band start to left offset
	// set band count to right-left+1
	int band_nnz = 0; // nnz in the band component of matrix
	std::vector<std::pair<int,int> > bands;

	while(!q_dia.empty()){
		int chosen_dia = q_dia.begin()->second;
		float chosen_dia_perc = q_dia.begin()->first;

		if(DEBUG) printf("choosing dia %d->%d\n", chosen_dia, chosen_dia- base_offset);

		int found_left_offset = 0;
		int found_right_offset = 0;

		int left_offset = chosen_dia - base_offset;
		int right_offset = chosen_dia - base_offset;

		int cum_band_nnz = nnz_in_dia[chosen_dia];
		int cum_occu_nnz = rows - abs(chosen_dia - base_offset);
		float cum_band_perc = ((float)nnz_in_dia[chosen_dia]/(rows - abs(chosen_dia-base_offset)))*100; // local band occupancy

		// Traverse left and find left offset
		for (int j = chosen_dia-1; !found_left_offset ; j--)
		{	
			cum_band_nnz += nnz_in_dia[j];
			cum_occu_nnz += rows - abs(j - base_offset);
			cum_band_perc = ((float)cum_band_nnz/cum_occu_nnz)*100;

			int next_dia_occu = ((float)nnz_in_dia[j-1]/(rows - abs(j-1-base_offset)))*100;

			// If we found a dia where band_occupancy is less then make sure that the next dia is not more the half the band_occupancy
			if(cum_band_perc < BAND_OCCUPANCY && (next_dia_occu < BAND_OCCUPANCY/2) ){
				left_offset = (j+1) - base_offset;
				found_left_offset = 1;
				band_nnz += (cum_band_nnz - nnz_in_dia[j]);
			}else{
				// remove the diagonal from vector if exists
				for (int k = 0; k < q_dia.size(); ++k)
				{
					if(q_dia[k].second == j){
						q_dia.erase(q_dia.begin()+k);
						break;
					}
				}
			}

		}

		// Resetting band perc
		cum_band_nnz = nnz_in_dia[chosen_dia];
		cum_occu_nnz = rows - abs(chosen_dia - base_offset);
		cum_band_perc = ((float)nnz_in_dia[chosen_dia]/(rows - abs(chosen_dia-base_offset)))*100; // local band occupancy;

		// Traverse right and find right offset
		for (int j = chosen_dia+1;  !found_right_offset; ++j)
		{
			
			cum_band_nnz += nnz_in_dia[j];
			cum_occu_nnz += rows - abs(j - base_offset);
			cum_band_perc = ((float)cum_band_nnz/cum_occu_nnz)*100;

			int next_dia_occu = ((float)nnz_in_dia[j+1]/(rows - abs(j+1-base_offset)))*100;			

			// average of dia, dia-1, dia+1 should be more than FILL_PERC and cumulative band occupancy should be >= given band occupancy
			if((cum_band_perc < BAND_OCCUPANCY) && (next_dia_occu < BAND_OCCUPANCY/2)){
				right_offset = (j-1) - base_offset;
				found_right_offset = 1;
				band_nnz += (cum_band_nnz - nnz_in_dia[j]);
			}else{
				// remove the diagonal from vector if exists
				for (int k = 0; k < q_dia.size(); ++k)
				{
					if(q_dia[k].second == j){
						q_dia.erase(q_dia.begin()+k);
						break;
					}
				}
			}

		}

		// Add it to bands
		bands.push_back(std::pair<int,int> (left_offset,right_offset - left_offset + 1));

		// As the pivot is added twice we remove once
		band_nnz -= nnz_in_dia[chosen_dia];

		// remove the diagonal from vector if exists
		q_dia.erase(q_dia.begin());

		if(DEBUG) printf("left %d right %d\n", left_offset, right_offset);
		

	}

  	// consolidate the bands

  	std::sort(bands.begin(), bands.end());

  	std::vector<std::pair<int,int> > cnsld_bands;
  	int temp_offset, temp_count;

	std::vector<std::pair<int,int> >::iterator fin_it = bands.begin();
  	while(fin_it != bands.end()){
  		temp_offset = fin_it->first;
  		temp_count = fin_it->second;

  		fin_it++;	

  		// if consecutive elements have same dia then take the maximum count
  		while(fin_it != bands.end() && fin_it->first == temp_offset){
  			temp_count = max(temp_count, fin_it->second);
  			fin_it++;
  		}

  		cnsld_bands.push_back(std::pair<int,int> (temp_offset, temp_count));

  	}
  	
    
  	band_starts = (int*) calloc(cnsld_bands.size(), sizeof(int));
  	band_counts = (int*) calloc(cnsld_bands.size(), sizeof(int));

  	int non_band_nnz = (nnz - band_nnz);
  	int num_dias_in_non_bands = nnz_diagonals;

  	for (int i = 0; i < cnsld_bands.size(); ++i)
  	{
  		num_dias_in_non_bands -= cnsld_bands[i].second;
  	}

  	// In case it is zero we don't want div/0 error
  	num_dias_in_non_bands = max(1,num_dias_in_non_bands);

  	int avg_nnz_in_non_band_dia = ceil(non_band_nnz/num_dias_in_non_bands);

  	if(DEBUG)
  		printf("%d avg_nnz_in_non_band_dia\n", avg_nnz_in_non_band_dia);

  	// Removing the zero diagonals appended on either sides of bands.
  	for (int i = 0; i < cnsld_bands.size(); ++i)
  	{
		int temp_left = cnsld_bands[i].first;
		int temp_right = cnsld_bands[i].first + cnsld_bands[i].second - 1;

		//while((float)(nnz_in_dia[temp_left + base_offset]/rows)*100 <= avg_dia_occu_in_non_band){
		while(nnz_in_dia[temp_left + base_offset] <= avg_nnz_in_non_band_dia){
			temp_left++;
		}

		// while((float)(nnz_in_dia[temp_right + base_offset]/rows)*100 <= avg_dia_occu_in_non_band){
		while(nnz_in_dia[temp_right + base_offset] <= avg_nnz_in_non_band_dia){
			temp_right--;
		}

		cnsld_bands[i].first = temp_left;
		cnsld_bands[i].second = temp_right - temp_left + 1;

  	}

  	// Setting final band_starts and band_counts
  	for (int i = 0; i < cnsld_bands.size() ; ++i)
  	{
  		band_starts[i] = cnsld_bands[i].first;
  		band_counts[i] = cnsld_bands[i].second;
  	}

  	return cnsld_bands.size();

}


__global__
void fill_band_data_with_flags_kernel(int rows, float* csr_data, int* csr_rowIndex, int* csr_colIndex,
	float* band_data, int* d_band_offset_indices, int* flags, int size){

	int tid = (blockIdx.z*(gridDim.x*gridDim.y) + blockIdx.y*(gridDim.x) + blockIdx.x) * blockDim.x + threadIdx.x;
	int row, col, offset, index;

	if(tid < size){
		row = csr_rowIndex[tid];
		col = csr_colIndex[tid];
		offset = col - row;
		index = d_band_offset_indices[offset+rows-1];

		// search whether the offset is there int the cumulative band offsets
		if(index > 0){
			flags[tid] = 0;
			band_data[(index-1)*rows + row] += csr_data[tid];
		}
	}

}


__global__
void mark_band_elements_in_coosr(int rows, int* csr_rowIndex, int* csr_colIndex, int* d_band_offset_indices, int* flags, int size){

	int tid = (blockIdx.z*(gridDim.x*gridDim.y) + blockIdx.y*(gridDim.x) + blockIdx.x) * blockDim.x + threadIdx.x;
	int row, col, offset, index;

	if(tid < size){
		row = csr_rowIndex[tid];
		col = csr_colIndex[tid];
		offset = col - row;
		index = d_band_offset_indices[offset+rows-1];

		// search whether the offset is there int the cumulative band offsets
		if(index > 0){
			flags[tid] = 1;
		}
	}

}


__global__
void fill_coo_data_kernel(float* data, int* rowIndex, int* colIndex,
						float* sparse_data, int* sparse_rowIndex, int* sparse_colIndex,int* band_flags, int size){

	int tid = (blockIdx.z*(gridDim.x*gridDim.y) + blockIdx.y*(gridDim.x) + blockIdx.x) * blockDim.x + threadIdx.x;
	if(tid < size){
		int index = band_flags[tid];
		if(index != band_flags[tid+1]){
			sparse_data[index] = data[tid];
			sparse_rowIndex[index] = rowIndex[tid];
			sparse_colIndex[index] = colIndex[tid];
		}
	}

}

void split_matrix_in_gpu(cusparseHandle_t cusparse_handle, coosr_matrix d_A_coosr, float* &d_band_data, int* &d_band_offsets, int* &d_band_offset_indices, int &num_dias, band_matrix* &d_band_mats, int &num_bands, coosr_matrix &d_sparse_mat, coosr_matrix &d_dia_in_coosr_mat, int &nz_diagonals, float &split_time, float BAND_OCCUPANCY, int USE_MAIN_ONLY){

	int DEBUG = 0;

	GpuTimer find_nnz_timer, fill_band_timer, fill_sparse_timer;

	dim3 grids,blocks;

	// setting independent band parameters
	int all_dia_count = d_A_coosr.rows+d_A_coosr.cols-1;

	// Finding nnz of all diagonals
	int* d_nnz_in_dia, *h_nnz_in_dia;
	int* d_dia_flags;
	int* h_total_in_dia;

	allocate_host_int_array(&h_nnz_in_dia, all_dia_count);
	allocate_host_int_array(&h_total_in_dia, all_dia_count);
	allocate_and_memset_device_int_array(&d_nnz_in_dia, all_dia_count);
	allocate_and_memset_device_int_array(&d_dia_flags, all_dia_count);

	// finding nnz of diagonals
	find_nnz_timer.Start();
		find_dimensions_of_blocks_and_threads(d_A_coosr.count, grids, blocks);
		find_nnz_in_dia_kernel<<<grids,blocks>>>(d_A_coosr.rowIndex, d_A_coosr.colIndex, d_A_coosr.rows, d_nnz_in_dia, d_A_coosr.count);	

		transfer_int_array_device_to_host(d_nnz_in_dia, h_nnz_in_dia, all_dia_count);
	find_nnz_timer.Stop();

	// Finding count of nz diagonals
	find_dimensions_of_blocks_and_threads(all_dia_count, grids, blocks);
	mark_flags<<<grids,blocks>>>(d_nnz_in_dia, d_dia_flags, all_dia_count);

	thrust::device_ptr<int> d_dia_flags_thrust(d_dia_flags);
	nz_diagonals = thrust::reduce(d_dia_flags_thrust, d_dia_flags_thrust + all_dia_count);
	cudaDeviceSynchronize();

	if(DEBUG) print_device_int_array(d_nnz_in_dia, all_dia_count);

	// finding total dia on CPU (not required but makes code easy to understand)	
	find_total_in_dia(h_total_in_dia, d_A_coosr.rows, d_A_coosr.cols); 


	// Testing band plucking code
	int* band_starts, *band_counts;

	double find_bands_start_time, find_bands_end_time;
	find_bands_start_time = timer();
		num_bands = find_bands_in_a_matrix(h_nnz_in_dia, h_total_in_dia, d_A_coosr.rows, d_A_coosr.cols, d_A_coosr.count, band_starts, band_counts, BAND_OCCUPANCY);
	find_bands_end_time = timer();

	//printf("Finding bands time %lf\n", (find_bands_end_time- find_bands_start_time)*1000 );

	if(DEBUG){
	for (int i = 0; i < num_bands; ++i)
		{
			printf("%d %d\n", band_starts[i], band_counts[i]);
		}
	}

	/*
	// MANUAL BAND INPUT
	num_bands = 5;
	band_starts = (int*) calloc(num_bands, sizeof(int));
	band_counts = (int*) calloc(num_bands, sizeof(int));

	band_starts[0] = -221; band_starts[1] = -41; band_starts[2] = -11; band_starts[3] = 31; band_starts[4] = 211;
	band_counts[0] = 11;  band_counts[1] = 11; band_counts[2] = 23; band_counts[3] = 11; band_counts[4] = 11;
	*/

	if(USE_MAIN_ONLY){
		int max_index = 0;
		int max_count = band_counts[0];
		int found = 0;

		for (int i = 0; i < num_bands && !found; ++i)
		{	
			// Getting the maxium band incase the diagonal is not present.
			if(band_counts[i] > max_count){
				max_index = i;
				max_count = band_counts[i];
			}
			// if main diagonal band is found then stop
			if(band_starts[i] <= 0 && band_starts[i]+band_counts[i]-1 >= 0){
				found = 1;
				// Making found bands to only main diagonal
				num_bands = 1;
				band_starts[0] = band_starts[i];
				band_counts[0] = band_counts[i];
			}
		}

		if(!found){
			num_bands = 1;
			band_starts[0] = band_starts[max_index];
			band_counts[0] = band_counts[max_index];
		}

	}

		
	// Cumulative band block
	float *h_band_data;
	int *h_band_offsets , *h_band_offset_indices;

	num_dias = 0;
	for (int i = 0; i < num_bands; ++i)
	{
		num_dias += band_counts[i];
	}

	// filling offsets
	h_band_offsets = (int*) calloc(num_dias, sizeof(int));
	h_band_offset_indices = (int*) calloc(all_dia_count, sizeof(int));

	int pos = 0;
	for (int i = 0; i < num_bands; ++i)
	{
		for (int j = 0; j < band_counts[i]; ++j)
		{
			h_band_offsets[pos] = band_starts[i] + j;
			h_band_offset_indices[h_band_offsets[pos]+d_A_coosr.rows-1] = pos+1;
			pos++;
		}
	}


	// allocating band memory and offsets
	allocate_device_int_array(&d_band_offsets, num_dias);
	allocate_device_int_array(&d_band_offset_indices, all_dia_count);

	transfer_int_array_host_to_device(h_band_offsets, d_band_offsets, num_dias);
	transfer_int_array_host_to_device(h_band_offset_indices, d_band_offset_indices, all_dia_count);
	allocate_and_memset_device_float_array(&d_band_data, num_dias * d_A_coosr.rows);

	if(DEBUG) print_device_int_array(d_band_offsets, num_dias);
	if(DEBUG) print_device_int_array(d_band_offset_indices, all_dia_count);
	
	
	int* d_sparse_flags;
	// populating band data and flags
	fill_band_timer.Start();
		allocate_and_memset_device_int_array(&d_sparse_flags, d_A_coosr.count+1); // for marking the band elements

		// setting flags to one
		find_dimensions_of_blocks_and_threads(d_A_coosr.count, grids, blocks);
		fill_array_with_value_kernel<<<grids,blocks>>>( d_sparse_flags , 1, d_A_coosr.count);
		cudaDeviceSynchronize();

		find_dimensions_of_blocks_and_threads(d_A_coosr.count, grids, blocks);
		fill_band_data_with_flags_kernel<<<grids,blocks>>>(d_A_coosr.rows, d_A_coosr.data, d_A_coosr.rowIndex, d_A_coosr.colIndex,
													d_band_data, d_band_offset_indices, d_sparse_flags,
													d_A_coosr.count);
		cudaDeviceSynchronize();
		thrust::device_ptr<int> d_sparse_flags_thrust(d_sparse_flags);
		
		// finding indices for sparse elements
		thrust::exclusive_scan(d_sparse_flags_thrust, d_sparse_flags_thrust+d_A_coosr.count+1, d_sparse_flags_thrust);
		cudaDeviceSynchronize();

		int sparse_nnz;
		cudaMemcpy(&sparse_nnz, d_sparse_flags+d_A_coosr.count, 1*sizeof(int), cudaMemcpyDeviceToHost);

	fill_band_timer.Stop();

	// allocating memory for sparse matrix
	d_sparse_mat.rows = d_A_coosr.rows; d_sparse_mat.cols = d_A_coosr.cols; d_sparse_mat.count = sparse_nnz;
	allocate_and_memset_device_float_array(&d_sparse_mat.data, sparse_nnz);
	allocate_and_memset_device_int_array(&d_sparse_mat.rowIndex, sparse_nnz);
	allocate_and_memset_device_int_array(&d_sparse_mat.colIndex, sparse_nnz);
	allocate_and_memset_device_int_array(&d_sparse_mat.rowPtr, d_A_coosr.rows+1);

	fill_sparse_timer.Start();
		// populating sparse data and flags
		find_dimensions_of_blocks_and_threads(d_A_coosr.count , grids, blocks);
		fill_coo_data_kernel<<<grids,blocks>>>(d_A_coosr.data, d_A_coosr.rowIndex, d_A_coosr.colIndex,
										d_sparse_mat.data, d_sparse_mat.rowIndex, d_sparse_mat.colIndex,
										d_sparse_flags, d_A_coosr.count);
		cudaDeviceSynchronize();

		// computing rowcsr for sparse matrix
		cusparseXcoo2csr(cusparse_handle, d_sparse_mat.rowIndex,
	                sparse_nnz, d_A_coosr.rows, d_sparse_mat.rowPtr, CUSPARSE_INDEX_BASE_ZERO);
		cudaDeviceSynchronize();
	fill_sparse_timer.Stop();

	// freeing sparse flags
	cudaFree(d_sparse_flags);

	// --------------------------------------------
	// To generate band as sparse matrix
		int* d_band_flags;
		allocate_and_memset_device_int_array(&d_band_flags, d_A_coosr.count+1);
		GpuTimer fill_band_as_csr_timer;

		// mark
		find_dimensions_of_blocks_and_threads(d_A_coosr.count , grids, blocks);
		mark_band_elements_in_coosr<<<grids,blocks>>>(d_A_coosr.rows, d_A_coosr.rowIndex, d_A_coosr.colIndex, d_band_offset_indices, d_band_flags, d_A_coosr.count);
		// scan
		cudaDeviceSynchronize();
		thrust::device_ptr<int> d_band_flags_thrust(d_band_flags);
		
		// finding indices for sparse elements
		thrust::exclusive_scan(d_band_flags_thrust, d_band_flags_thrust+d_A_coosr.count+1, d_band_flags_thrust);
		cudaDeviceSynchronize();

		int band_nnz;
		cudaMemcpy(&band_nnz, d_band_flags+d_A_coosr.count, 1*sizeof(int), cudaMemcpyDeviceToHost);

		allocate_and_memset_device_coosr_matrix(d_A_coosr.rows, d_A_coosr.cols, band_nnz, d_dia_in_coosr_mat);

		fill_band_as_csr_timer.Start();
			// populating sparse data and flags
			find_dimensions_of_blocks_and_threads(d_A_coosr.count , grids, blocks);
			fill_coo_data_kernel<<<grids,blocks>>>(d_A_coosr.data, d_A_coosr.rowIndex, d_A_coosr.colIndex,
											d_dia_in_coosr_mat.data, d_dia_in_coosr_mat.rowIndex, d_dia_in_coosr_mat.colIndex,
											d_band_flags, d_A_coosr.count);
			cudaDeviceSynchronize();

			// computing rowcsr for sparse matrix
			cusparseXcoo2csr(cusparse_handle, d_dia_in_coosr_mat.rowIndex,
		                band_nnz, d_A_coosr.rows, d_dia_in_coosr_mat.rowPtr, CUSPARSE_INDEX_BASE_ZERO);
			cudaDeviceSynchronize();
		fill_band_as_csr_timer.Stop();

		//printf("TIME for filling band as csr %f\n", fill_band_as_csr_timer.Elapsed());

		// freeing sparse flags
		cudaFree(d_band_flags);
	
	// -------------------------------------------------
	// temp info
	if(DEBUG){
		coosr_matrix temp;
		allocate_and_transfer_coosr_device_to_host(d_sparse_mat, temp);
		print_coosr_matrix(temp);
	
		band_matrix temp1;
		print_device_float_array(d_band_data, num_dias* d_A_coosr.rows);
		print_device_int_array(d_band_offsets, num_dias);
	}


	// transfering cumulative band data on to CPU
	
	allocate_and_transfer_float_array_device_to_host(d_band_data, &h_band_data, num_dias*d_A_coosr.rows);

	// Creating band structures [ For easier implementation: Other wise the data can be accessed from cum band data itself]
	d_band_mats = (band_matrix*) calloc(num_bands,sizeof(band_matrix));

	int dia_index, base_index,count;
	for (int i = 0; i < num_bands; ++i)
	{	
		dia_index = band_starts[i] + d_A_coosr.rows - 1;
		base_index = h_band_offset_indices[dia_index] - 1;
		count = band_counts[i];

		allocate_device_float_array(&d_band_mats[i].data, count*d_A_coosr.rows);
		allocate_device_int_array(&d_band_mats[i].offsets, count);

		cudaMemcpy(d_band_mats[i].data, h_band_data + base_index*d_A_coosr.rows , count*d_A_coosr.rows*sizeof(float), cudaMemcpyHostToDevice );
		cudaMemcpy(d_band_mats[i].offsets, h_band_offsets + base_index, count*sizeof(int), cudaMemcpyHostToDevice);

		if(DEBUG){
			printf("BAND %d\n",i );
			print_device_float_array(d_band_mats[i].data, count*d_A_coosr.rows);
			print_device_int_array(d_band_mats[i].offsets, count);
		}


		d_band_mats[i].rows = d_A_coosr.rows;
		d_band_mats[i].cols = d_A_coosr.cols;
		d_band_mats[i].count = count;
		d_band_mats[i].left_offset = band_starts[i];
		d_band_mats[i].right_offset = band_starts[i] + count - 1;


	}

	split_time = find_nnz_timer.Elapsed() +  fill_band_timer.Elapsed() + fill_sparse_timer.Elapsed();

	//printf("%.2f %.2f %.2f %.2f\n", split_time , find_nnz_timer.Elapsed() ,  fill_band_timer.Elapsed() , fill_sparse_timer.Elapsed());
	
}


// DIA DIA related
__global__
void mark_band_band_output_diagonals(int rows, int cols, int* d_A_dia_offsets, int A_num_dias, int* d_B_dia_offsets, int B_num_dias, int* flags, int size){
	int tid = (blockIdx.z*(gridDim.x*gridDim.y) + blockIdx.y*(gridDim.x) + blockIdx.x) * blockDim.x + threadIdx.x;
	
	int first_index = tid%A_num_dias;
	int second_index = tid/A_num_dias;

	if(tid < size){
		int output_offset = d_A_dia_offsets[first_index] + d_B_dia_offsets[second_index];
		if(output_offset > -1*rows && output_offset < cols){
			flags[output_offset+rows-1] = 1;
		}
	}
}

__global__
void gather_output_offsets(int rows, int* d_output_offset_indices, int* d_output_offsets, int size){
	int tid = (blockIdx.z*(gridDim.x*gridDim.y) + blockIdx.y*(gridDim.x) + blockIdx.x) * blockDim.x + threadIdx.x;

	if(tid < size){
		int index = d_output_offset_indices[tid];
		if(index != 0){
			d_output_offsets[index-1] = tid - (rows-1);
		}
	}
}


void dia_dia_preprocessing(int rows, int cols, int* d_A_dia_offsets, int A_num_dias, int* d_B_dia_offsets, int B_num_dias, int &output_dia_count, int* &d_output_offsets, float* &d_output_data, int* &d_output_offset_indices){

	int DEBUG = 0;

	int all_dia_count = rows+cols-1;

	dim3 grids,blocks;
	int* d_temp_output_offset_indices;
	find_dimensions_of_blocks_and_threads(A_num_dias*B_num_dias , grids, blocks);
	allocate_and_memset_device_int_array(&d_output_offset_indices, all_dia_count);
	allocate_and_memset_device_int_array(&d_temp_output_offset_indices, all_dia_count);

	mark_band_band_output_diagonals<<<grids,blocks>>>(rows,cols, d_A_dia_offsets, A_num_dias, d_B_dia_offsets, B_num_dias, d_temp_output_offset_indices, A_num_dias*B_num_dias);
	cudaDeviceSynchronize();

	if(DEBUG) print_device_int_array(d_temp_output_offset_indices, all_dia_count);
		
	thrust::device_ptr<int> d_indices_thrust(d_temp_output_offset_indices);
	output_dia_count = thrust::reduce(d_indices_thrust, d_indices_thrust + all_dia_count);
	cudaDeviceSynchronize();

	if(DEBUG) printf("^output diagoal count %d , %d\n", output_dia_count, rows*output_dia_count);

	thrust::inclusive_scan(d_indices_thrust, d_indices_thrust+all_dia_count, d_indices_thrust);
	cudaDeviceSynchronize();	


	find_dimensions_of_blocks_and_threads(all_dia_count , grids, blocks);
	set_zero_for_unset_flag_in_psum<<<grids, blocks>>>(d_temp_output_offset_indices, d_output_offset_indices, all_dia_count);
	cudaDeviceSynchronize();

	if(DEBUG) print_device_int_array(d_output_offset_indices, all_dia_count);

	// Allocating offsets and data
	allocate_and_memset_device_int_array(&d_output_offsets, output_dia_count);
	allocate_and_memset_device_float_array(&d_output_data, output_dia_count * rows);

	find_dimensions_of_blocks_and_threads(all_dia_count , grids, blocks);
	gather_output_offsets<<<grids,blocks>>>(rows, d_output_offset_indices, d_output_offsets, all_dia_count);

	if(DEBUG) print_device_int_array(d_output_offsets, output_dia_count);

}

__global__
void dia_dia_multiplication_kernel(float* d_A_data, int* d_A_offsets,int A_rows,int A_cols,float* d_B_data,int* d_B_offsets,int B_rows,int B_cols,int* d_output_offset_indices, float* d_output_data){

	int A_index = blockIdx.x;
	int B_index = blockIdx.y;

	int A_offset = d_A_offsets[blockIdx.x];
	int B_offset = d_B_offsets[blockIdx.y];

	int row = blockIdx.z*blockDim.x + threadIdx.x;
	int output_row = row + A_offset;
	int output_col = row + A_offset + B_offset;

	if(row < A_rows && (output_row >= 0 && output_row < B_rows) && (output_col >= 0 && output_col < B_cols)){
		
		int output_index = d_output_offset_indices[A_offset + B_offset + A_rows - 1] - 1;

		float value = d_A_data[A_index*A_rows + row] * d_B_data[B_index*B_rows + output_row];
		atomicAdd(&d_output_data[output_index*A_rows + row],value);
	}
}


// SPARSE BAND related
__global__
void set_out_of_bound_element_flags_to_zero(int* colIndex, int cols, int* block_start_flags, int left_offset, int right_offset, int size){
	int tid = (blockIdx.z*(gridDim.x*gridDim.y) + blockIdx.y*(gridDim.x) + blockIdx.x) * blockDim.x + threadIdx.x;

	if(tid < size){
		int index = colIndex[tid];
		if(index+right_offset < 0 || index+left_offset >= cols ){
			block_start_flags[tid] = 0;
		}
	}
}

__global__
void find_block_start_positions(int* rowIndex, int* colIndex, int left_offset, int right_offset, int* block_start_flags, int size){
	int index = (blockIdx.z*(gridDim.x*gridDim.y) + blockIdx.y*(gridDim.x) + blockIdx.x) * blockDim.x + threadIdx.x;

	if(index > 0 && index < size){
		// To take of inter row checking
		if(rowIndex[index] == rowIndex[index-1]){
			// To unmark the merged column Indices
			int column_index = colIndex[index];
			int column_index_prev = colIndex[index-1];


			if( (block_start_flags[index] != 0) // Current element is not out of bound
				&& (block_start_flags[index-1] != 0) // The prev element is not out of bound
				&& (column_index + left_offset <= column_index_prev + right_offset) ){ // It overlaps with prev one
					block_start_flags[index] = 0;
				} 
				

		}
	}
}

__global__
void gather_block_start_positions(int* d_A_block_start_pos_flags, int* d_block_start_positions, int size){
	int index = (blockIdx.z*(gridDim.x*gridDim.y) + blockIdx.y*(gridDim.x) + blockIdx.x) * blockDim.x + threadIdx.x;

	if(index < size){
		int output_index = d_A_block_start_pos_flags[index];
		if((d_A_block_start_pos_flags[index+1] - output_index) != 0){
			d_block_start_positions[output_index] = index;
		}
	}
}

__global__
void construct_block_start_positions_rowPtr(int* d_A_rowcsr_rowPtr,int* d_A_block_start_pos_flags, int* d_A_block_sart_positions_rowPtr, int A_rows){
	int index = (blockIdx.z*(gridDim.x*gridDim.y) + blockIdx.y*(gridDim.x) + blockIdx.x) * blockDim.x + threadIdx.x;

	if(index <= A_rows){
		int csr_pos_start = d_A_rowcsr_rowPtr[index];
		d_A_block_sart_positions_rowPtr[index] = d_A_block_start_pos_flags[csr_pos_start];
	}

}

__global__
void compute_uniband_blockCount_and_base_indices(int* d_A_block_start_positions,int* d_A_rowcsr_colIndex, int* d_A_rowcsr_rowIndex, int A_rowcsr_count, int l_index, int r_index, int* d_A_blockCount, int* d_A_base_rowIndex, int* d_A_base_colIndex, int B_cols, int size){
	int index = (blockIdx.z*(gridDim.x*gridDim.y) + blockIdx.y*(gridDim.x) + blockIdx.x) * blockDim.x + threadIdx.x;

	if(index < size){
		int csr_pos_start = d_A_block_start_positions[index];
		int csr_pos_end;

		int col_left = max(0,d_A_rowcsr_colIndex[csr_pos_start]+l_index);
		int col_right;

		if(index == size-1){
			csr_pos_end = A_rowcsr_count-1;
		}else{
			csr_pos_end = d_A_block_start_positions[index+1]-1;
		}
			
		col_right = min(d_A_rowcsr_colIndex[csr_pos_end]+r_index, B_cols-1);

		d_A_base_colIndex[index] = col_left;
		d_A_base_rowIndex[index] = d_A_rowcsr_rowIndex[csr_pos_start];
		d_A_blockCount[index] = col_right - col_left + 1;

	}
}

__global__
void compute_index_of_last_outofbound_element_in_row(int* colIndex, int* rowPtr, int last_allwd_colIndex, int* last_proper_csr_index, int size){
	int row = (blockIdx.z*(gridDim.x*gridDim.y) + blockIdx.y*(gridDim.x) + blockIdx.x) * blockDim.x + threadIdx.x;

	// Find the csr element in the row which is the highest one that is <= last_allwd_colIndex

	if(row < size){
		int csr_start = rowPtr[row];
		int csr_end = rowPtr[row+1]-1;

		// Check if there are any elements
		if(csr_end - csr_start >= 0){
			int middle = (csr_start+csr_end)/2;
			while(csr_start != middle){
				if(last_allwd_colIndex >= colIndex[middle]){
					csr_start = middle;
				}else{
					csr_end = middle;
				}
				middle = (csr_start+csr_end)/2;
			}

			last_proper_csr_index[row] = middle;
		}
	}
}


__global__
void compute_blockCount_and_base_indices(int* d_A_block_start_positions,int* d_A_rowcsr_colIndex, int* d_A_rowcsr_rowIndex, int* d_A_rowcsr_rowPtr, int A_rowcsr_count, int l_index, int r_index, int* d_A_blockCount, int* d_A_base_rowIndex, int* d_A_base_colIndex, int B_cols, int size){
	int index = (blockIdx.z*(gridDim.x*gridDim.y) + blockIdx.y*(gridDim.x) + blockIdx.x) * blockDim.x + threadIdx.x;

	if(index < size){
		int csr_pos_start = d_A_block_start_positions[index];
		int csr_pos_end;

		int col_left = max(0,d_A_rowcsr_colIndex[csr_pos_start]+l_index);
		int col_right;

		int cur_row;

		// Handling last element
		if(index == size-1){
			csr_pos_end = A_rowcsr_count-1;

			cur_row = d_A_rowcsr_rowIndex[csr_pos_start];
			// Last block end csr should include out of bound elements
			for (int i = csr_pos_start+1; i < d_A_rowcsr_rowPtr[cur_row+1]; ++i)
			{
				if(d_A_rowcsr_colIndex[i]+r_index < B_cols){
					csr_pos_end = i;
				}
			}

		}else{
			// if(cur_row == next_row) csr_pos_end is the below
			csr_pos_end = d_A_block_start_positions[index+1]-1;

			cur_row = d_A_rowcsr_rowIndex[csr_pos_start];
			int next_row = d_A_rowcsr_rowIndex[ csr_pos_end+1 ];

			if(cur_row != next_row){
				csr_pos_end = csr_pos_start;

				// Last csr element that is not out of bound in that row .
				int done = 0;
				for (int i = d_A_rowcsr_rowPtr[cur_row+1]-1; i >= csr_pos_start+1  && !done; --i)
				{
					if(d_A_rowcsr_colIndex[i]+l_index < B_cols){
						csr_pos_end = i;
						done = 1;
					}
				}
			}
		}

				
			
		col_right = min(d_A_rowcsr_colIndex[csr_pos_end]+r_index, B_cols-1);

		// printf("csr_pos_start %d colIndex[csr_pos_start] %d left_offset %d right_offset %d\n", csr_pos_start, d_A_rowcsr_colIndex[csr_pos_start], l_index, r_index);

		d_A_base_colIndex[index] = col_left;
		d_A_base_rowIndex[index] = cur_row;
		d_A_blockCount[index] = col_right - col_left + 1;

	}
}

__global__
void construct_sparse_band_output_rowPtr(int* d_A_blockCount, int* d_A_block_start_positions_rowPtr, int* d_sparse_band_output_rowPtr, int A_rows){
	int index = (blockIdx.z*(gridDim.x*gridDim.y) + blockIdx.y*(gridDim.x) + blockIdx.x) * blockDim.x + threadIdx.x;

	if(index <= A_rows){
		int csr_pos = d_A_block_start_positions_rowPtr[index];
		d_sparse_band_output_rowPtr[index] = d_A_blockCount[csr_pos];
	}
}


__global__
void mark_last_element_in_each_row(int* d_A_blockCount, int* d_output_colIndex, int  merged_block_count){
	int index = (blockIdx.z*(gridDim.x*gridDim.y) + blockIdx.y*(gridDim.x) + blockIdx.x) * blockDim.x + threadIdx.x;

	if(index < merged_block_count){
		int csr_pos_start = d_A_blockCount[index];
		int csr_pos_end = d_A_blockCount[index+1]-1;

		if(csr_pos_end - csr_pos_start >= 0){
			d_output_colIndex[csr_pos_end] = 1;
		}
	}
}

__global__
void fill_output_column_and_row_indices(int* d_A_blockCount, int* d_block_mappings, int* d_A_base_rowIndex, int* d_A_base_colIndex, int* d_output_rowIndex, int* d_output_colIndex, int h_sparse_band_output_nnz){
	int index = (blockIdx.z*(gridDim.x*gridDim.y) + blockIdx.y*(gridDim.x) + blockIdx.x) * blockDim.x + threadIdx.x;

	if(index < h_sparse_band_output_nnz){
		int block_index = d_block_mappings[index];
		int start_index = d_A_blockCount[block_index];
		int row = d_A_base_rowIndex[block_index];
		int col = d_A_base_colIndex[block_index] + (index-start_index);

		d_output_colIndex[index] =  col;
		d_output_rowIndex[index] = row;
	}
}

// HELPER Function
void print_flags_in_csr_format(int* d_flags, int size, int rows, int* d_rowPtr){

	int* h_flags, *h_rowPtr;
	allocate_and_transfer_int_array_device_to_host(d_flags, &h_flags, size+1);
	allocate_and_transfer_int_array_device_to_host(d_rowPtr, &h_rowPtr, rows+1);


	for (int i = 0; i < rows; ++i)
	{
		int row_start = h_rowPtr[i];
		int row_count = h_rowPtr[i+1] - row_start;
		if(row_count > 0){
			for (int j = 0; j < row_count; ++j)
				printf("%d ",h_flags[row_start+j] );
				printf(" | ");
		}else{
			printf("  | ");
		}
	}
	printf(" %d\n", h_flags[size]);

}

void sparse_band_preprocessing(coosr_matrix d_A_rowcsr, band_matrix d_B_band,
						int A_rows, int A_cols, int B_cols,
						int* &d_output_rowPtr, int* &d_output_rowIndex, int* &d_output_colIndex, int &h_output_nnz,
						int* &d_A_blockCount, int* &d_A_base_rowIndex, int* &d_A_base_colIndex, int* &d_A_block_start_positions_rowPtr){

	
	int DEBUG = 0;
	dim3 grids, blocks;

	// allocating output data
	allocate_and_memset_device_int_array(&d_output_rowPtr, (d_A_rowcsr.rows+1));

	GpuTimer sparse_band_preprocessing_timer;
	int num_blocks, num_threads_per_block;

	int* d_A_block_start_pos_flags; // flag denoting whether a row element is start of a block
	int* d_A_block_start_positions; // csr indices of first element of all blocks

	allocate_and_memset_device_int_array(&d_A_block_start_pos_flags, d_A_rowcsr.count+1);
	allocate_and_memset_device_int_array(&d_A_block_start_positions_rowPtr, d_A_rowcsr.rows+1);

	// Setting all flags to 1
	find_dimensions_of_blocks_and_threads(d_A_rowcsr.count, grids, blocks);
	fill_array_with_value_kernel<<<grids,blocks>>>(d_A_block_start_pos_flags, 1, d_A_rowcsr.count);

	if(DEBUG){
		printf("SETTING ALL FLAGS\n");
		print_flags_in_csr_format(d_A_block_start_pos_flags, d_A_rowcsr.count, d_A_rowcsr.rows, d_A_rowcsr.rowPtr);
	}

	// Setting Out of bound element flags to 0
	find_dimensions_of_blocks_and_threads(d_A_rowcsr.count, grids, blocks);
	set_out_of_bound_element_flags_to_zero<<<grids,blocks>>>(d_A_rowcsr.colIndex, d_A_rowcsr.cols, d_A_block_start_pos_flags, d_B_band.left_offset, d_B_band.right_offset, d_A_rowcsr.count);

	if(DEBUG){
		printf("UNSETTING OUT OF BOUND ELEMENTS\n");
		print_flags_in_csr_format(d_A_block_start_pos_flags, d_A_rowcsr.count, d_A_rowcsr.rows, d_A_rowcsr.rowPtr);
	}

	// Setting overlapping element flags to 0 (Only block starts remain)
	find_dimensions_of_blocks_and_threads(d_A_rowcsr.count, grids, blocks);	
	find_block_start_positions<<<grids,blocks>>>(d_A_rowcsr.rowIndex, d_A_rowcsr.colIndex, d_B_band.left_offset, d_B_band.right_offset, d_A_block_start_pos_flags, d_A_rowcsr.count);

	if(DEBUG){
		printf("FINDING BLOCK START POSITIONS\n");
		print_flags_in_csr_format(d_A_block_start_pos_flags, d_A_rowcsr.count, d_A_rowcsr.rows, d_A_rowcsr.rowPtr);
	}


	// Finding the last csr element in each row
	int* d_last_proper_row_elements;
	allocate_and_memset_device_int_array(&d_last_proper_row_elements, d_A_rowcsr.rows);
	find_dimensions_of_blocks_and_threads(d_A_rowcsr.rows, grids, blocks);
	compute_index_of_last_outofbound_element_in_row<<<grids,blocks>>>(d_A_rowcsr.colIndex, d_A_rowcsr.rowPtr, d_A_rowcsr.cols+d_B_band.left_offset, d_last_proper_row_elements, d_A_rowcsr.rows);

	if(DEBUG){
		printf("LAST IN BOUND CSR ROW ELEMENTS\n");
		print_device_int_array(d_last_proper_row_elements, d_A_rowcsr.rows);
	}


	// To index all found blocks
	thrust::device_ptr<int> d_A_block_start_pos_flags_thrust(d_A_block_start_pos_flags);
	thrust::exclusive_scan(d_A_block_start_pos_flags_thrust, d_A_block_start_pos_flags_thrust+(d_A_rowcsr.count+1), d_A_block_start_pos_flags_thrust);

	if(DEBUG){
		printf("AFTER INDEXING BLOCKS\n");
		print_flags_in_csr_format(d_A_block_start_pos_flags, d_A_rowcsr.count, d_A_rowcsr.rows, d_A_rowcsr.rowPtr);
	}
		
	int merged_block_count;
	cudaMemcpy(&merged_block_count, &d_A_block_start_pos_flags[d_A_rowcsr.count], sizeof(int), cudaMemcpyDeviceToHost);
	if(DEBUG) printf("MERGED BLOCK COUNT %d \n",merged_block_count);	

	// allocating memory for csr indices of first element in blocks
	allocate_and_memset_device_int_array(&d_A_block_start_positions, merged_block_count);

	// Gather all found blocks in to an array.
	find_dimensions_of_blocks_and_threads(d_A_rowcsr.count, grids, blocks);
	gather_block_start_positions<<<grids, blocks>>>(d_A_block_start_pos_flags,  d_A_block_start_positions, d_A_rowcsr.count);

	if(DEBUG) {
		printf("CSR INDICES OF BLOCK START POSITIONS\n");
		print_device_int_array(d_A_block_start_positions, merged_block_count);
	}

	// construct block start positions rowPtr
	find_dimensions_of_blocks_and_threads(d_A_rowcsr.rows+1, grids, blocks);
	construct_block_start_positions_rowPtr<<<grids, blocks>>>(d_A_rowcsr.rowPtr, d_A_block_start_pos_flags, d_A_block_start_positions_rowPtr, d_A_rowcsr.rows);

	if(DEBUG) {
		printf("ROWPTR OF BLOCKS\n");
		print_device_int_array(d_A_block_start_positions_rowPtr, d_A_rowcsr.rows+1);
	}


	
	allocate_and_memset_device_int_array(&d_A_base_colIndex, merged_block_count);
	allocate_and_memset_device_int_array(&d_A_base_rowIndex, merged_block_count);
	allocate_and_memset_device_int_array(&d_A_blockCount, merged_block_count+1);

	// Computing blockCounts, base row and colum Indices
	find_dimensions_of_blocks_and_threads(merged_block_count, grids, blocks);
	compute_blockCount_and_base_indices<<<grids, blocks>>>(d_A_block_start_positions, d_A_rowcsr.colIndex, d_A_rowcsr.rowIndex, d_A_rowcsr.rowPtr, d_A_rowcsr.count, d_B_band.left_offset, d_B_band.right_offset, d_A_blockCount, d_A_base_rowIndex, d_A_base_colIndex, d_B_band.cols, merged_block_count);
	//compute_uniband_blockCount_and_base_indices<<<grids, blocks>>>(d_A_block_start_positions, d_A_rowcsr.colIndex, d_A_rowcsr.rowIndex, d_A_rowcsr.count, d_B_band.left_offset, d_B_band.right_offset, d_A_blockCount, d_A_base_rowIndex, d_A_base_colIndex, d_B_band.cols, merged_block_count);

	if(DEBUG) {
		printf("BLOCK COUNTS, BASE ROW AND COLUMN INDICES\n");
		printf("ROW INDEX"); print_device_int_array(d_A_base_rowIndex, merged_block_count);
		printf("COL INDEX"); print_device_int_array(d_A_base_colIndex, merged_block_count);
		printf("BLCK CNTS"); print_device_int_array(d_A_blockCount, merged_block_count);
	}

	// Exclusive sum of block counts
	thrust::device_ptr<int> d_A_blockCount_thrust(d_A_blockCount);
	thrust::exclusive_scan(d_A_blockCount_thrust, d_A_blockCount_thrust+(merged_block_count+1), d_A_blockCount_thrust);

	if(DEBUG) {
		printf("BLOCK COUNTS EXCLUSIVE SCAN\n");
		printf("BLCK CNTS"); print_device_int_array(d_A_blockCount, merged_block_count+1);
	}

	// Computation of Sparse band output rowPtr
	find_dimensions_of_blocks_and_threads(d_A_rowcsr.rows+1, grids, blocks);
	construct_sparse_band_output_rowPtr<<<grids, blocks>>>(d_A_blockCount, d_A_block_start_positions_rowPtr, d_output_rowPtr, d_A_rowcsr.rows);


	// allocating output for SPARSE X BAND
	cudaMemcpy(&h_output_nnz, d_A_blockCount + merged_block_count,1*sizeof(int),cudaMemcpyDeviceToHost);
	allocate_and_memset_device_int_array(&d_output_colIndex, h_output_nnz); // because I flag it first and then find index
	allocate_and_memset_device_int_array(&d_output_rowIndex, h_output_nnz);

	int* d_block_mappings;
	allocate_and_memset_device_int_array(&d_block_mappings, h_output_nnz+1);

	// Setting last element in each block 
	find_dimensions_of_blocks_and_threads(merged_block_count, grids, blocks);
	mark_last_element_in_each_row<<<grids,blocks>>>(d_A_blockCount, d_block_mappings, merged_block_count);

	// Assigning corresponding block number for each element
	thrust::device_ptr<int> d_block_mappings_thrust(d_block_mappings);
	thrust::exclusive_scan(d_block_mappings_thrust, d_block_mappings_thrust+(h_output_nnz+1), d_block_mappings_thrust);



	// Expanding base row and column indices
	find_dimensions_of_blocks_and_threads(h_output_nnz, grids, blocks);
	fill_output_column_and_row_indices<<<grids,blocks>>>(d_A_blockCount, d_block_mappings, d_A_base_rowIndex, d_A_base_colIndex, d_output_rowIndex, d_output_colIndex, h_output_nnz);

	if(DEBUG) {
		printf("SPARSE BAND OUTPUT ROWINDEX COLINDEX AND ROWPTR\n");
		printf("ROW INDEX"); print_device_int_array(d_output_rowIndex, h_output_nnz);
		printf("COL INDEX"); print_device_int_array(d_output_colIndex, h_output_nnz);
		printf("ROWPTR   "); print_device_int_array(d_output_rowPtr, d_A_rowcsr.rows+1);
	}

	cudaDeviceSynchronize();



	// freeing buffer memory
//	cudaFree(d_A_block_start_pos_flags); cudaFree(d_A_block_start_positions);

}

__global__
void rowcsr_band_matrix_multiplication_optimized_kernel(float* d_A_rowcsr_data,int* d_A_rowcsr_colIndex, int* d_A_rowcsr_rowIndex, int* d_A_rowcsr_rowPtr,
											int* d_A_blockCount, int* d_A_blockCount_rowPtr, int* d_A_baseColIndex,
											int A_rows, int A_cols,
											float* d_B_band_data,int* d_B_band_indices,
											int B_rows, int B_cols,
											int A_nnz, int B_band_count,
											float* d_output_rowcsr_values,int* d_output_rowcsr_colIndex,int* d_output_rowcsr_rowPtr){

	int tid = (blockIdx.z*(gridDim.x*gridDim.y) + blockIdx.y*(gridDim.x) + blockIdx.x) * blockDim.x + threadIdx.x;

	if(tid < A_nnz*B_band_count){

		int B_index = tid/A_nnz;
		int B_offset = d_B_band_indices[B_index];

		int csr_pos = tid%A_nnz;

		int output_row = d_A_rowcsr_rowIndex[csr_pos];
		int colIndex = d_A_rowcsr_colIndex[csr_pos];

		int output_col = colIndex + B_offset;
		float value;

		// finding appropriate output in rowcsr and putting it there.
		if(output_col >=0 && output_col < B_cols){

			value = d_A_rowcsr_data[csr_pos] * d_B_band_data[B_index*B_rows + colIndex];

			int l_index = d_A_blockCount_rowPtr[output_row]; 
			int r_index = d_A_blockCount_rowPtr[output_row+1] - 1;
			int m_index = (l_index+r_index)/2;
			int fin_index = m_index;
			int output_index;


			while(r_index - l_index > 1){
				if(output_col >= d_A_baseColIndex[m_index]){
					l_index = m_index;
				}else{
					r_index = m_index-1;
				}

				m_index = (l_index+r_index)/2;	
			}

			// finding the proper block
			if(output_col >= d_A_baseColIndex[l_index] && output_col < d_A_baseColIndex[r_index]){
				fin_index = l_index;
			}else{
				fin_index = r_index;
			}

			// getting the final index in the output rowcsr of SPARSEX BAND
			output_index = d_A_blockCount[fin_index] + (output_col- d_A_baseColIndex[fin_index]);

			/*			
			if(output_row == 0){	
				printf("left_index %d right_index %d\n",l_index, r_index );
				printf("output_index %d fin_index %d band offset %d output_col %d d_A_baseColIndex %d value %f\n",output_index, fin_index, B_offset, output_col, d_A_baseColIndex[fin_index], value);
			}
			*/
			
			atomicAdd(&d_output_rowcsr_values[output_index], value);

		}
	}
	
}

void merge_coosr_matrices(coosr_matrix* d_coosr_matrices, int num_mats, coosr_matrix &d_merged_coosr_output, float &time_taken){
	time_taken = 0;
	if(num_mats < 1){
		printf("HOW CAN I CONSOLIDATE BANDS LESS THAN 1\n");
		return;
	}

	GpuTimer timers[num_mats-1];

	coosr_matrix d_merged_coosr[num_mats], d_unique_coosr[num_mats];

	// First is the merged and unique rowcsr
	d_merged_coosr[0] = d_coosr_matrices[0];
	d_unique_coosr[0] = d_coosr_matrices[0];

	for (int i = 1; i < num_mats; ++i)
	{
		timers[i-1].Start();
			merge_two_csr_reps(d_unique_coosr[i-1], d_coosr_matrices[i], d_merged_coosr[i]);
			group_unique_matrix_elements(d_merged_coosr[i], d_unique_coosr[i]);
		timers[i-1].Stop();
		
		// Free d_merged_coosr[i]
		cudaFree(d_merged_coosr[i].data); cudaFree(d_merged_coosr[i].rowIndex); cudaFree(d_merged_coosr[i].colIndex); cudaFree(d_merged_coosr[i].rowPtr);
		if(i>1){
			// Free d_merged_coosr[i-1]
			cudaFree(d_unique_coosr[i-1].data); cudaFree(d_unique_coosr[i-1].rowIndex); cudaFree(d_unique_coosr[i-1].colIndex); cudaFree(d_unique_coosr[i-1].rowPtr);
		}
		
	}

	// Final unique rowcsr
	d_merged_coosr_output = d_unique_coosr[num_mats-1];

	// Final time
	for (int i = 0; i < num_mats-1; ++i)
	{
		time_taken += timers[i].Elapsed();
	}
	

}

// BAND SPARSE related

// SPARSE SPARSE related

// OVERLAP Optimization related

__global__
void move_overlap_of_coo_in_dia_kernel(int* non_overlap_flags, int rows, float* sparse_data, int* rowIndex, int* colIndex, float* dia_data, int* dia_offset_indices, int size){
	int tid = (blockIdx.z*(gridDim.x*gridDim.y) + blockIdx.y*(gridDim.x) + blockIdx.x) * blockDim.x + threadIdx.x;

	if(tid < size){
		int row = rowIndex[tid];
		int col = colIndex[tid];
		int offset = col - row;
		int offset_index = dia_offset_indices[offset + rows - 1];

		if(offset_index != 0){
			non_overlap_flags[tid] = 0;
			atomicAdd(&dia_data[rows*(offset_index-1) + row], sparse_data[tid]);
		}
	}
}

__global__
void gather_marked_indices_kernel(int* scanned_flags, int* marked_indices, int size){
	int tid = (blockIdx.z*(gridDim.x*gridDim.y) + blockIdx.y*(gridDim.x) + blockIdx.x) * blockDim.x + threadIdx.x;

	if(tid < size){
		int index = scanned_flags[tid];
		if(scanned_flags[tid+1] - index != 0){
			marked_indices[index] = tid;
		}
	}
}

__global__
void fill_non_compressed_data_kernel(int* indices, float* orig_data, int* orig_rowIndex, int* orig_colIndex, float* cmprsd_data, int* cmprsd_rowIndex, int* cmprsd_colIndex, int size){
	int tid = (blockIdx.z*(gridDim.x*gridDim.y) + blockIdx.y*(gridDim.x) + blockIdx.x) * blockDim.x + threadIdx.x;

	if(tid < size){
		int index = indices[tid];
		cmprsd_data[tid] = orig_data[index];
		cmprsd_rowIndex[tid] = orig_rowIndex[index];
		cmprsd_colIndex[tid] = orig_colIndex[index];
	}
}

__global__
void fill_non_compressed_rowPtr_kernel(int* scanned_flags, int* orig_rowPtr, int* cmprsd_rowPtr, int size){
	int tid = (blockIdx.z*(gridDim.x*gridDim.y) + blockIdx.y*(gridDim.x) + blockIdx.x) * blockDim.x + threadIdx.x;

	if(tid < size){
		cmprsd_rowPtr[tid] = scanned_flags[orig_rowPtr[tid]];
	}

}

void move_overlap_of_csr_in_dia(coosr_matrix &d_mband_sparse_output, float* d_dia_dia_output_data, int* d_dia_dia_output_offset_indices, float &msp_dia_overlap_time, float &msp_dia_overlap_perc){

	int DEBUG = 0;

	msp_dia_overlap_time = 0;
	msp_dia_overlap_perc = 0;

	GpuTimer non_overlap_timer;

	dim3 grids,blocks;
	int original_nnz = d_mband_sparse_output.count;
	int cmprsd_nnz, non_cmprsd_nnz;

	non_overlap_timer.Start();

		int* d_non_overlap_flags;
		allocate_and_memset_device_int_array(&d_non_overlap_flags, d_mband_sparse_output.count+1);

		// Initially we assume no element overlaps
		find_dimensions_of_blocks_and_threads(d_mband_sparse_output.count, grids, blocks);
		fill_array_with_value_kernel<<<grids,blocks>>>(d_non_overlap_flags, 1, d_mband_sparse_output.count);
		move_overlap_of_coo_in_dia_kernel<<<grids, blocks>>>(d_non_overlap_flags, d_mband_sparse_output.rows, d_mband_sparse_output.data, d_mband_sparse_output.rowIndex, d_mband_sparse_output.colIndex, d_dia_dia_output_data, d_dia_dia_output_offset_indices, d_mband_sparse_output.count);

		// scan flags to find indices
		thrust::device_ptr<int> d_non_overlap_flags_thrust(d_non_overlap_flags);
		thrust::exclusive_scan(d_non_overlap_flags_thrust, d_non_overlap_flags_thrust + d_mband_sparse_output.count + 1, d_non_overlap_flags_thrust);

		// Get the compressed nnz
		transfer_int_array_device_to_host(&d_non_overlap_flags[d_mband_sparse_output.count], &non_cmprsd_nnz, 1);

		// If overlap is less than 50 percent don't do it
		msp_dia_overlap_perc = ((float)(original_nnz - non_cmprsd_nnz)*100)/(original_nnz+1);
		if(msp_dia_overlap_perc < 50){
			non_overlap_timer.Stop();
			msp_dia_overlap_time = non_overlap_timer.Elapsed();
			return;
		}
		
		// allocating space for compressed indices
		int* d_non_cmprsd_indices;
		allocate_and_memset_device_int_array(&d_non_cmprsd_indices, non_cmprsd_nnz);

		// Gather indices of overlap elements
		find_dimensions_of_blocks_and_threads(d_mband_sparse_output.count, grids, blocks);
		gather_marked_indices_kernel<<<grids,blocks>>>(d_non_overlap_flags,d_non_cmprsd_indices, d_mband_sparse_output.count);

		// Using overlap indices gather data
		coosr_matrix non_cmprsd_output;
		allocate_and_memset_device_coosr_matrix(d_mband_sparse_output.rows, d_mband_sparse_output.cols, non_cmprsd_nnz, non_cmprsd_output);

		// Filling Non overlapped data
		find_dimensions_of_blocks_and_threads(non_cmprsd_nnz, grids, blocks);
		fill_non_compressed_data_kernel<<<grids,blocks>>>(d_non_cmprsd_indices, d_mband_sparse_output.data, d_mband_sparse_output.rowIndex, d_mband_sparse_output.colIndex, non_cmprsd_output.data, non_cmprsd_output.rowIndex, non_cmprsd_output.colIndex, non_cmprsd_output.count);
		
		// Filling Compressed row Pointer
		find_dimensions_of_blocks_and_threads(d_mband_sparse_output.rows+1, grids, blocks);
		fill_non_compressed_rowPtr_kernel<<<grids,blocks>>>(d_non_overlap_flags, d_mband_sparse_output.rowPtr, non_cmprsd_output.rowPtr, d_mband_sparse_output.rows+1);

	non_overlap_timer.Stop();

	// Free the original data and replace the pointer
	cudaFree(d_non_overlap_flags);
	cudaFree(d_mband_sparse_output.data); cudaFree(d_mband_sparse_output.rowIndex); cudaFree(d_mband_sparse_output.colIndex); cudaFree(d_mband_sparse_output.rowPtr);
	
	cudaDeviceSynchronize();

	d_mband_sparse_output = non_cmprsd_output;
	if(DEBUG) print_device_coosr_matrix(d_mband_sparse_output);

	msp_dia_overlap_time = non_overlap_timer.Elapsed();
	
}


void move_overlap_of_csc_in_dia(coosc_matrix &d_mband_sparse_output, float* d_dia_dia_output_data, int* d_dia_dia_output_offset_indices, float &msp_dia_overlap_time, float &msp_dia_overlap_perc){

	int DEBUG = 0;

	msp_dia_overlap_time = 0;
	msp_dia_overlap_perc = 0;

	GpuTimer non_overlap_timer;

	dim3 grids,blocks;
	int original_nnz = d_mband_sparse_output.count;
	int cmprsd_nnz, non_cmprsd_nnz;

	non_overlap_timer.Start();

		int* d_non_overlap_flags;
		allocate_and_memset_device_int_array(&d_non_overlap_flags, d_mband_sparse_output.count+1);

		// Initially we assume no element overlaps
		find_dimensions_of_blocks_and_threads(d_mband_sparse_output.count, grids, blocks);
		fill_array_with_value_kernel<<<grids,blocks>>>(d_non_overlap_flags, 1, d_mband_sparse_output.count);		
		move_overlap_of_coo_in_dia_kernel<<<grids, blocks>>>(d_non_overlap_flags, d_mband_sparse_output.rows, d_mband_sparse_output.data, d_mband_sparse_output.rowIndex, d_mband_sparse_output.colIndex, d_dia_dia_output_data, d_dia_dia_output_offset_indices, d_mband_sparse_output.count);

		// scan flags to find indices
		thrust::device_ptr<int> d_non_overlap_flags_thrust(d_non_overlap_flags);
		thrust::exclusive_scan(d_non_overlap_flags_thrust, d_non_overlap_flags_thrust + d_mband_sparse_output.count + 1, d_non_overlap_flags_thrust);

		// Get the compressed nnz
		transfer_int_array_device_to_host(&d_non_overlap_flags[d_mband_sparse_output.count], &non_cmprsd_nnz, 1);

		// If overlap is less than 50 percent don't do it
		msp_dia_overlap_perc = ((float)(original_nnz - non_cmprsd_nnz)*100)/(original_nnz+1);
		if(msp_dia_overlap_perc < 50){
			non_overlap_timer.Stop();
			msp_dia_overlap_time = non_overlap_timer.Elapsed();
			return;
		}
		
		// allocating space for compressed indices
		int* d_non_cmprsd_indices;
		allocate_and_memset_device_int_array(&d_non_cmprsd_indices, non_cmprsd_nnz);

		// Gather indices of overlap elements
		find_dimensions_of_blocks_and_threads(d_mband_sparse_output.count, grids, blocks);
		gather_marked_indices_kernel<<<grids,blocks>>>(d_non_overlap_flags,d_non_cmprsd_indices, d_mband_sparse_output.count);

		// Using overlap indices gather data
		coosc_matrix non_cmprsd_output;
		allocate_and_memset_device_coosc_matrix(d_mband_sparse_output.rows, d_mband_sparse_output.cols, non_cmprsd_nnz, non_cmprsd_output);

		// Filling Non overlapped data
		find_dimensions_of_blocks_and_threads(non_cmprsd_nnz, grids, blocks);
		fill_non_compressed_data_kernel<<<grids,blocks>>>(d_non_cmprsd_indices, d_mband_sparse_output.data, d_mband_sparse_output.rowIndex, d_mband_sparse_output.colIndex, non_cmprsd_output.data, non_cmprsd_output.rowIndex, non_cmprsd_output.colIndex, non_cmprsd_output.count);
		
		// Filling Compressed row Pointer
		find_dimensions_of_blocks_and_threads(d_mband_sparse_output.cols+1, grids, blocks);
		fill_non_compressed_rowPtr_kernel<<<grids,blocks>>>(d_non_overlap_flags, d_mband_sparse_output.colPtr, non_cmprsd_output.colPtr, d_mband_sparse_output.cols+1);

	non_overlap_timer.Stop();

	// Free the original data and replace the pointer
	cudaFree(d_non_overlap_flags);
	cudaFree(d_mband_sparse_output.data); cudaFree(d_mband_sparse_output.rowIndex); cudaFree(d_mband_sparse_output.colIndex); cudaFree(d_mband_sparse_output.colPtr);
	
	cudaDeviceSynchronize();

	d_mband_sparse_output = non_cmprsd_output;
	if(DEBUG) print_device_coosc_matrix(d_mband_sparse_output);

	msp_dia_overlap_time = non_overlap_timer.Elapsed();
	msp_dia_overlap_perc = ((float)(original_nnz - non_cmprsd_nnz)*100)/original_nnz;
}

__global__
void move_overlap_of_coo_in_csr_kernel(int* non_overlap_flags, float* coo_data, int* coo_rowIndex, int* coo_colIndex, float* csr_data, int* csr_colIndex, int* csr_rowPtr, int size){
	int tid = (blockIdx.z*(gridDim.x*gridDim.y) + blockIdx.y*(gridDim.x) + blockIdx.x) * blockDim.x + threadIdx.x;
	if(tid < size){
		int coo_row = coo_rowIndex[tid];
		int l_index = csr_rowPtr[coo_row];
		int r_index = csr_rowPtr[coo_row+1];

		if((r_index - l_index > 0) && (non_overlap_flags[tid] != 0)){
			// Allow only non overlap elements and unset the non_overlap flag if it overlaps
			// row has elements
			int m_index = (l_index+r_index)/2;
			int coo_col = coo_colIndex[tid];

			while(l_index < r_index){
				if(coo_col > csr_colIndex[m_index]){
					l_index = m_index+1;
				}else{
					r_index = m_index;
				}
				m_index = (l_index+r_index)/2;
			}

			
			// size can only be 1
			if(coo_col == csr_colIndex[l_index]){
				//printf("overlap %d %d %d\n",coo_row, coo_col, csr_colIndex[l_index]);
				non_overlap_flags[tid] = 0;
				atomicAdd(&csr_data[l_index] , coo_data[tid]);
			}
		}
	}
}

void move_overlap_of_csc_in_csr(coosc_matrix &coosc_mat, coosr_matrix &coosr_mat, float &time_taken, float &overlap_perc){
	int DEBUG = 0;

	time_taken = 0;
	overlap_perc = 0;

	GpuTimer non_overlap_timer;

	dim3 grids,blocks;
	int original_nnz = coosc_mat.count;
	int cmprsd_nnz, non_cmprsd_nnz;

	if(DEBUG) print_device_coosr_matrix(coosr_mat);

	non_overlap_timer.Start();
		// Allocating flags to model non overlap elements.
		int* d_non_overlap_flags;
		allocate_and_memset_device_int_array(&d_non_overlap_flags, coosc_mat.count+1);

		// Initially we assume no element overlaps
		find_dimensions_of_blocks_and_threads(coosc_mat.count, grids, blocks);		
		fill_array_with_value_kernel<<<grids,blocks>>>(d_non_overlap_flags, 1, coosc_mat.count);
		move_overlap_of_coo_in_csr_kernel<<<grids, blocks>>>(d_non_overlap_flags, coosc_mat.data, coosc_mat.rowIndex, coosc_mat.colIndex, 
					coosr_mat.data, coosr_mat.colIndex, coosr_mat.rowPtr, coosc_mat.count);

		// scan flags to find indices
		thrust::device_ptr<int> d_non_overlap_flags_thrust(d_non_overlap_flags);
		thrust::exclusive_scan(d_non_overlap_flags_thrust, d_non_overlap_flags_thrust + coosc_mat.count + 1, d_non_overlap_flags_thrust);


		// Get the non compressed nnz
		transfer_int_array_device_to_host(&d_non_overlap_flags[coosc_mat.count], &non_cmprsd_nnz, 1);

		// If overlap is less than 50 percent don't do it
		overlap_perc = ((float)(original_nnz - non_cmprsd_nnz)*100)/(original_nnz+1);
		if(overlap_perc < 50){
			non_overlap_timer.Stop();
			time_taken = non_overlap_timer.Elapsed();
			return;
		}
		
		// allocating space for compressed indices
		int* d_non_cmprsd_indices;
		allocate_and_memset_device_int_array(&d_non_cmprsd_indices, non_cmprsd_nnz);

		// Gather indices of overlap elements
		find_dimensions_of_blocks_and_threads(coosc_mat.count, grids, blocks);
		gather_marked_indices_kernel<<<grids,blocks>>>(d_non_overlap_flags,d_non_cmprsd_indices, coosc_mat.count);


		// Using overlap indices gather data
		coosc_matrix non_cmprsd_output;
		allocate_and_memset_device_coosc_matrix(coosc_mat.rows, coosc_mat.cols, non_cmprsd_nnz, non_cmprsd_output);

		// Filling Non overlapped data
		find_dimensions_of_blocks_and_threads(non_cmprsd_nnz, grids, blocks);
		fill_non_compressed_data_kernel<<<grids,blocks>>>(d_non_cmprsd_indices, coosc_mat.data, coosc_mat.rowIndex, coosc_mat.colIndex, non_cmprsd_output.data, non_cmprsd_output.rowIndex, non_cmprsd_output.colIndex, non_cmprsd_output.count);
		
		// Filling Compressed row Pointer
		find_dimensions_of_blocks_and_threads(coosc_mat.cols+1, grids, blocks);
		fill_non_compressed_rowPtr_kernel<<<grids,blocks>>>(d_non_overlap_flags, coosc_mat.colPtr, non_cmprsd_output.colPtr, coosc_mat.cols+1);


	non_overlap_timer.Stop();

	if(DEBUG) print_device_coosc_matrix(coosc_mat);
	if(DEBUG) print_device_coosr_matrix(coosr_mat);

	// Free the original data and replace the pointer
	cudaFree(d_non_overlap_flags);
	cudaFree(coosc_mat.data); cudaFree(coosc_mat.rowIndex); cudaFree(coosc_mat.colIndex); cudaFree(coosc_mat.colPtr);
	
	cudaDeviceSynchronize();

	coosc_mat = non_cmprsd_output;
	if(DEBUG) print_device_coosc_matrix(coosc_mat);

	time_taken = non_overlap_timer.Elapsed();
	overlap_perc = ((float)(original_nnz - non_cmprsd_nnz)*100)/original_nnz;
}


// CONSOLIDATION related
__global__
void compute_csr_merge_output_rowcsr_rowPtr(int* A_rowPtr, int* B_rowPtr, int* C_rowPtr, int rows){
	int pos = (blockIdx.z*(gridDim.x*gridDim.y) + blockIdx.y*(gridDim.x) + blockIdx.x) * blockDim.x + threadIdx.x;

	if(pos <= rows){
		C_rowPtr[pos] = A_rowPtr[pos] + B_rowPtr[pos];
	}

}

__global__
void place_first_csr_in_output_csr(float* A_data, int* A_rowIndex, int* A_colIndex, int A_count,
									int* B_colIndex,int* B_rowPtr, int B_count,
									float* C_data, int* C_rowIndex, int* C_colIndex, int* C_rowPtr){
	int pos = (blockIdx.z*(gridDim.x*gridDim.y) + blockIdx.y*(gridDim.x) + blockIdx.x) * blockDim.x + threadIdx.x;
	int DEBUG = 0;

	if(pos < A_count){
		int row = A_rowIndex[pos];
		int l_index = B_rowPtr[row];
		int r_index = B_rowPtr[row+1]-1;
		int m_index = (l_index+r_index)/2;

		int search_element = A_colIndex[pos];

		while(l_index < r_index){
			if(search_element >= B_colIndex[m_index]){
				l_index = m_index+1;
			}else{
				r_index = m_index;
			}
			m_index = (l_index+r_index)/2;
		}

		int output_place = pos + l_index ;

		// for handling case like (1,8) and (2,8)
		if(l_index == B_rowPtr[row+1]-1 && search_element >= B_colIndex[l_index])
			output_place++;

		C_data[output_place] = A_data[pos];
		C_rowIndex[output_place] = A_rowIndex[pos];
		C_colIndex[output_place] = search_element;

	}

}

__global__
void place_second_csr_in_output_csr(float* A_data, int* A_rowIndex, int* A_colIndex, int A_count,
									int* B_colIndex,int* B_rowPtr, int B_count,
									float* C_data, int* C_rowIndex, int* C_colIndex, int* C_rowPtr){
	int pos = (blockIdx.z*(gridDim.x*gridDim.y) + blockIdx.y*(gridDim.x) + blockIdx.x) * blockDim.x + threadIdx.x;
	int DEBUG = 0;

	if(pos < A_count){
		int row = A_rowIndex[pos];
		int l_index = B_rowPtr[row];
		int r_index = B_rowPtr[row+1]-1;
		int row_count = r_index - l_index + 1;
		int m_index = (l_index+r_index)/2 + (l_index+r_index)%2;

		int search_element = A_colIndex[pos];

		while(l_index < r_index){
		
			if(search_element <= B_colIndex[m_index]){
				r_index = m_index-1;
			}else{
				l_index = m_index;
			}

			m_index = (l_index+r_index)/2 + (l_index+r_index)%2;
		}

		int output_place = pos + l_index + 1 ;
		if((l_index == B_rowPtr[row] && search_element <= B_colIndex[l_index]) || (row_count == 0))
			output_place--;

		C_data[output_place] = A_data[pos];
		C_rowIndex[output_place] = A_rowIndex[pos];
		C_colIndex[output_place] = search_element;

	}

}

void merge_two_csr_reps( coosr_matrix d_A_rowcsr, coosr_matrix d_B_rowcsr, coosr_matrix &d_merged_rowcsr){

	dim3 grids, blocks;

	// allocating space for merged data.
	int consolidated_count = d_A_rowcsr.count + d_B_rowcsr.count;
	allocate_and_memset_device_coosr_matrix(d_A_rowcsr.rows, d_A_rowcsr.cols, consolidated_count, d_merged_rowcsr);

	// calculating merged row Pointer
	find_dimensions_of_blocks_and_threads(d_A_rowcsr.rows+1, grids, blocks);
	compute_csr_merge_output_rowcsr_rowPtr<<<grids,blocks>>>(d_A_rowcsr.rowPtr, d_B_rowcsr.rowPtr, d_merged_rowcsr.rowPtr, d_A_rowcsr.rows);

	// placing first csr on to output
	find_dimensions_of_blocks_and_threads(d_A_rowcsr.count, grids, blocks);
	place_first_csr_in_output_csr<<<grids,blocks>>>(d_A_rowcsr.data, d_A_rowcsr.rowIndex, d_A_rowcsr.colIndex, d_A_rowcsr.count, 
																		d_B_rowcsr.colIndex, d_B_rowcsr.rowPtr, d_B_rowcsr.count,
																		d_merged_rowcsr.data, d_merged_rowcsr.rowIndex, d_merged_rowcsr.colIndex, d_merged_rowcsr.rowPtr);


	find_dimensions_of_blocks_and_threads(d_B_rowcsr.count, grids, blocks);
	place_second_csr_in_output_csr<<<grids,blocks>>>(d_B_rowcsr.data, d_B_rowcsr.rowIndex, d_B_rowcsr.colIndex, d_B_rowcsr.count, 
																		d_A_rowcsr.colIndex, d_A_rowcsr.rowPtr, d_A_rowcsr.count,
																		d_merged_rowcsr.data, d_merged_rowcsr.rowIndex, d_merged_rowcsr.colIndex, d_merged_rowcsr.rowPtr);
	cudaDeviceSynchronize();
}

__global__
void mark_unique_occurence_in_sorted_list(int* rowIndex, int* colIndex, int* flag, int size){

	int pos = (blockIdx.z*(gridDim.x*gridDim.y) + blockIdx.y*(gridDim.x) + blockIdx.x) * blockDim.x + threadIdx.x;

	if(pos < size){
		if(pos == size-1)
			flag[pos] = 1;
		else{
			if((rowIndex[pos] != rowIndex[pos+1]) || (colIndex[pos] != colIndex[pos+1]))
				flag[pos] = 1;
		}
	}
}

__global__
void compress_consolidated_output(int* indices, float* read_data, int* read_rowIndex, int* read_colIndex, float* write_data, int* write_rowIndex, int* write_colIndex, int size){

	int pos = (blockIdx.z*(gridDim.x*gridDim.y) + blockIdx.y*(gridDim.x) + blockIdx.x) * blockDim.x + threadIdx.x;

	if(pos < size){
		float value = read_data[pos];
		int index = indices[pos];
		atomicAdd(&write_data[index],value);
		write_rowIndex[index] = read_rowIndex[pos];
		write_colIndex[index] = read_colIndex[pos];
	}
}

__global__
void compute_compressed_output_rowPtr(int* uncompressed_rowPtr, int* compressed_count, int* compressed_rowPtr, int A_rows){
	int pos = (blockIdx.z*(gridDim.x*gridDim.y) + blockIdx.y*(gridDim.x) + blockIdx.x) * blockDim.x + threadIdx.x;

	if(pos <= A_rows){
		compressed_rowPtr[pos] = compressed_count[uncompressed_rowPtr[pos]];
	}
}

void group_unique_matrix_elements(coosr_matrix d_merged_rowcsr, coosr_matrix &d_unique_rowcsr){

	int DEBUG = 0;

	dim3 grids, blocks;
	int* d_flags;
	allocate_and_memset_device_int_array(&d_flags, d_merged_rowcsr.count+1);
		
	if(DEBUG) printf("Merged block count %d\n", d_merged_rowcsr.count);

   	find_dimensions_of_blocks_and_threads(d_merged_rowcsr.count, grids, blocks);
	mark_unique_occurence_in_sorted_list<<<grids,blocks>>>(d_merged_rowcsr.rowIndex, d_merged_rowcsr.colIndex, d_flags, d_merged_rowcsr.count);

	if(DEBUG) print_device_int_array(d_flags, d_merged_rowcsr.count+1);

	thrust::device_ptr<int> d_flags_thrust(d_flags);
	thrust::exclusive_scan(d_flags_thrust, d_flags_thrust + d_merged_rowcsr.count + 1, d_flags_thrust);

	if(DEBUG) print_device_int_array(d_flags, d_merged_rowcsr.count+1);
	if(DEBUG) print_device_float_array(d_merged_rowcsr.data, d_merged_rowcsr.count);

	// getting the count of unique output elements
	int unique_nnz;
	cudaMemcpy(&unique_nnz, d_flags + d_merged_rowcsr.count, 1*sizeof(int), cudaMemcpyDeviceToHost );

	allocate_and_memset_device_coosr_matrix(d_merged_rowcsr.rows, d_merged_rowcsr.cols, unique_nnz, d_unique_rowcsr);

   	find_dimensions_of_blocks_and_threads(d_merged_rowcsr.count, grids, blocks);
	compress_consolidated_output<<<grids, blocks>>>(d_flags, d_merged_rowcsr.data, d_merged_rowcsr.rowIndex, d_merged_rowcsr.colIndex,
													d_unique_rowcsr.data, d_unique_rowcsr.rowIndex, d_unique_rowcsr.colIndex, 
													d_merged_rowcsr.count);

	if(DEBUG) print_device_float_array(d_unique_rowcsr.data, unique_nnz);

	find_dimensions_of_blocks_and_threads(d_merged_rowcsr.rows+1, grids, blocks);
	compute_compressed_output_rowPtr<<<grids,blocks>>>(d_merged_rowcsr.rowPtr, d_flags, d_unique_rowcsr.rowPtr, d_merged_rowcsr.rows);

	if(DEBUG){
		coosr_matrix test;
		allocate_and_transfer_coosr_device_to_host(d_unique_rowcsr, test);
		print_coosr_matrix(test);
	}

	cudaDeviceSynchronize();


}


// CONSOLIDATION USING MERGIN
__global__
void compress_row_and_col_positions(int* rowIndex, int* colIndex, int cols, int* indices, int size){
	int tid = (blockIdx.z*(gridDim.x*gridDim.y) + blockIdx.y*(gridDim.x) + blockIdx.x) * blockDim.x + threadIdx.x;

	if(tid < size){
		indices[tid] = rowIndex[tid]*cols + colIndex[tid];
	}
}

__global__
void expand_positions_from_row_and_col(int* indices, int cols, int* rowIndex, int* colIndex, int size){
	int tid = (blockIdx.z*(gridDim.x*gridDim.y) + blockIdx.y*(gridDim.x) + blockIdx.x) * blockDim.x + threadIdx.x;

	if(tid < size){
		rowIndex[tid] = indices[tid]/cols;
		colIndex[tid] = indices[tid]%cols;
	}
}

__global__
void mark_first_unique_elements_in_sorted_array(int* flags, int* keys, int size){
	int tid = (blockIdx.z*(gridDim.x*gridDim.y) + blockIdx.y*(gridDim.x) + blockIdx.x) * blockDim.x + threadIdx.x;

	if(tid < size-1){
		if(keys[tid] != keys[tid+1])
			flags[tid] = 1;
	}else{
		if(tid == size-1) 
			flags[tid] = 1;
	}
}

__global__
void compress_merged_coosr_output(int* cmprd_indices, float* read_data, int* read_indices, float* write_data, int* write_indices, int size){
	int tid = (blockIdx.z*(gridDim.x*gridDim.y) + blockIdx.y*(gridDim.x) + blockIdx.x) * blockDim.x + threadIdx.x;

	if(tid < size){
		int cmprsd_index = cmprd_indices[tid];
		atomicAdd(&write_data[cmprsd_index], read_data[tid]);
		write_indices[cmprsd_index] = read_indices[tid];
	}
}


void merge_coosr_matrices_optimized(cusparseHandle_t cusparse_handle, coosr_matrix* d_coosr_matrices, int num_mats, coosr_matrix &d_merged_coosr_output, float &time_taken){


	int DEBUG = 0;

	dim3 grids,blocks;
	int*  indices[num_mats];

	for (int i = 0; i < num_mats; ++i)
	{
		if(DEBUG) print_device_coosr_matrix(d_coosr_matrices[i]);

		allocate_and_memset_device_int_array(&indices[i], d_coosr_matrices[i].count);
		// Compressing the (row,col) -> (row*cols+col)
		find_dimensions_of_blocks_and_threads(d_coosr_matrices[i].count, grids, blocks);
		compress_row_and_col_positions<<<grids,blocks>>>(d_coosr_matrices[i].rowIndex, d_coosr_matrices[i].colIndex, d_coosr_matrices[i].cols, indices[i], d_coosr_matrices[i].count);

		if(DEBUG){
			print_device_int_array(indices[i], d_coosr_matrices[i].count);
			print_device_float_array(d_coosr_matrices[i].data, d_coosr_matrices[i].count);
		}
	}

	int* d_merged_indices[num_mats];
	float*  d_merged_data[num_mats];
	int d_cum_counts[num_mats];

	d_merged_indices[0] = indices[0];
	d_merged_data[0] = d_coosr_matrices[0].data;
	d_cum_counts[0] = d_coosr_matrices[0].count;

	// After each merge we maintain the size of the outptut appends
	for (int i = 1; i < num_mats; ++i)
	{
		d_cum_counts[i] = d_cum_counts[i-1] + d_coosr_matrices[i].count;
	}
	int total_size = d_cum_counts[num_mats-1];

	if(DEBUG){
		print_device_int_array(d_merged_indices[0], d_cum_counts[0]);
		print_device_float_array(d_merged_data[0], d_cum_counts[0]);
	}

	GpuTimer merge_timers[num_mats-1];

	// Performing the merge and removing the older values
	for (int i = 1; i < num_mats; ++i)
	{

		allocate_and_memset_device_int_array(&d_merged_indices[i], d_cum_counts[i]);
		allocate_and_memset_device_float_array(&d_merged_data[i], d_cum_counts[i]);

		thrust::device_ptr<int> d_A_keys(d_merged_indices[i-1]);
		thrust::device_ptr<float> d_A_values(d_merged_data[i-1]);

		thrust::device_ptr<int> d_B_keys(indices[i]);
		thrust::device_ptr<float> d_B_values(d_coosr_matrices[i].data);	

		thrust::device_ptr<int> d_output_keys(d_merged_indices[i]);
		thrust::device_ptr<float> d_output_values(d_merged_data[i]);

		merge_timers[i-1].Start();

			thrust::merge_by_key(thrust::device,
				d_A_keys, d_A_keys + d_cum_counts[i-1],
				d_B_keys, d_B_keys + d_coosr_matrices[i].count,
				d_A_values, d_B_values,
				d_output_keys, d_output_values);

		merge_timers[i-1].Stop();

		if(i > 1){
			cudaFree(d_merged_indices[i-1]); 
			cudaFree(d_merged_data[i-1]);
			cudaFree(indices[i-1]);
		}

		if(DEBUG){
			print_device_int_array(d_merged_indices[i], d_cum_counts[i]);
			print_device_float_array(d_merged_data[i], d_cum_counts[i]);
		}

	}
	GpuTimer compression_timer;

	compression_timer.Start();

		// Compress the data;
		int* d_flags;
		allocate_and_memset_device_int_array(&d_flags, total_size+1);
		find_dimensions_of_blocks_and_threads(total_size, grids, blocks);
		mark_first_unique_elements_in_sorted_array<<<grids,blocks>>>(d_flags, d_merged_indices[num_mats-1], d_cum_counts[num_mats-1]);	

		if(DEBUG) print_device_int_array(d_flags, total_size);

		// exclusive scan
		thrust::device_ptr<int> d_flags_thrust(d_flags);
		thrust::exclusive_scan(d_flags_thrust, d_flags_thrust+total_size+1, d_flags_thrust);

		if(DEBUG) print_device_int_array(d_flags, total_size);

		int unique_size;
		transfer_int_array_device_to_host(&d_flags[total_size], &unique_size, 1);

		if(DEBUG) printf("compressed count %d\n", unique_size );

		// allocating memory for the final merged coosr
		int* d_unique_indices;
		allocate_and_memset_device_int_array(&d_unique_indices, unique_size);
		allocate_and_memset_device_coosr_matrix(d_coosr_matrices[0].rows, d_coosr_matrices[0].cols, unique_size, d_merged_coosr_output);

		find_dimensions_of_blocks_and_threads(total_size, grids, blocks);
		compress_merged_coosr_output<<<grids,blocks>>>(d_flags, d_merged_data[num_mats-1], d_merged_indices[num_mats-1], d_merged_coosr_output.data,  d_unique_indices, total_size);

		find_dimensions_of_blocks_and_threads(unique_size, grids, blocks);
		expand_positions_from_row_and_col<<<grids,blocks>>>(d_unique_indices, d_merged_coosr_output.cols, d_merged_coosr_output.rowIndex, d_merged_coosr_output.colIndex, unique_size);

		// Generating row pointer
		cusparseXcoo2csr(cusparse_handle, d_merged_coosr_output.rowIndex,
		                d_merged_coosr_output.count, d_merged_coosr_output.rows, d_merged_coosr_output.rowPtr, CUSPARSE_INDEX_BASE_ZERO);
		cudaDeviceSynchronize();
	compression_timer.Stop();

	if(DEBUG) print_device_coosr_matrix(d_merged_coosr_output);


	float merge_time = 0;
	float compression_time = compression_timer.Elapsed();

	for (int i = 0; i < num_mats-1; ++i)
	{
		merge_time += merge_timers[i].Elapsed();
	}

	time_taken = 0;
	time_taken = merge_time + compression_time;

	//printf("consolidation time %f = (%f %f)\n", time_taken, merge_time, compression_time);

	return;

}

// CONSOLIDATION USING UNSORTED COOSC OUTPUT
void get_coosr_to_coosc_unsorted_conv_time(cusparseHandle_t cusparse_handle, coosr_matrix coosr_mat, float &time_taken){

	int DEBUG = 0;

	GpuTimer sort_coosr_by_col_timer;
	dim3 grids,blocks;

	if(DEBUG) print_device_coosr_matrix(coosr_mat);

	// SORT coosc_matrix by row
	int* d_indices;
	allocate_and_memset_device_int_array(&d_indices, coosr_mat.count);
	find_dimensions_of_blocks_and_threads(coosr_mat.count, grids, blocks);
	fill_array_with_tid_kernel<<<grids,blocks>>>(d_indices, coosr_mat.count);

	// Allocating memory for column sorted coosr matrix
	coosc_matrix d_csr_colSorted;
	allocate_and_memset_device_coosc_matrix(coosr_mat.rows, coosr_mat.cols, coosr_mat.count, d_csr_colSorted);

	// Making a copy of coosr mat column indices (so when sorted the original is intact)
	int* d_coosr_colIndices;
	allocate_and_transfer_int_array_device_to_device(coosr_mat.colIndex, &d_coosr_colIndices, coosr_mat.count);

	thrust::device_ptr<int> d_indices_thrust(d_indices);
	thrust::device_ptr<int> d_coosr_colIndices_thrust(d_coosr_colIndices);

	if(DEBUG) print_device_int_array(d_coosr_colIndices, coosr_mat.count);
	sort_coosr_by_col_timer.Start();
		thrust::sort_by_key(d_coosr_colIndices_thrust, d_coosr_colIndices_thrust + coosr_mat.count, d_indices_thrust);

		if(DEBUG) print_device_int_array(d_indices, coosr_mat.count);

		find_dimensions_of_blocks_and_threads(coosr_mat.count, grids, blocks);
		shuffle_matrix_data_using_rowIndices<<<grids,blocks>>>(d_indices, coosr_mat.data, coosr_mat.rowIndex, coosr_mat.colIndex,
											 d_csr_colSorted.data, d_csr_colSorted.rowIndex, d_csr_colSorted.colIndex, coosr_mat.count);

		// Generating row pointer
		cusparseXcoo2csr(cusparse_handle, d_csr_colSorted.colIndex,
		                d_csr_colSorted.count, d_csr_colSorted.cols, d_csr_colSorted.colPtr, CUSPARSE_INDEX_BASE_ZERO);
		cudaDeviceSynchronize();
	sort_coosr_by_col_timer.Stop();

	if(DEBUG) print_device_coosc_matrix(d_csr_colSorted);

	cudaFree(d_indices); cudaFree(d_csr_colSorted.rowIndex); cudaFree(d_csr_colSorted.colIndex); cudaFree(d_csr_colSorted.data);
	cudaFree(d_coosr_colIndices);
	time_taken = sort_coosr_by_col_timer.Elapsed();
	// printf("Conv B csr2csc(unsorted) %.3f\n", sort_coosr_by_col_timer.Elapsed());
	
}

__global__
void shuffle_matrix_data_using_rowIndices(int* shuffled_indices, float* read_data, int* read_rowIndex, int* read_colIndex, float* write_data, int* write_rowIndex, int* write_colIndex, int size ){
	int tid = (blockIdx.z*(gridDim.x*gridDim.y) + blockIdx.y*(gridDim.x) + blockIdx.x) * blockDim.x + threadIdx.x;
	if(tid < size){
		int read_index = shuffled_indices[tid];
		write_data[tid] = read_data[read_index];
		write_rowIndex[tid] = read_rowIndex[read_index];
		write_colIndex[tid] = read_colIndex[read_index];
	}
}

void move_overlap_of_anycsr_in_csr_and_compress(coosr_matrix mat1, coosr_matrix mat2, coosr_matrix &d_cmprsd_mat, float &move_time, float &compress_time){
	// any CSR means sorted/unsorted in column
	// move interstion of mat1 with mat2 to mat1 and compress. 

	GpuTimer move_timer, compress_timer;
	dim3 grids,blocks;

	//print_device_coosr_matrix(mat2);

	move_timer.Start();
		// MOVE overlap elements of bsp rowsorted on to merged(spb + spsp) and compress
		int* d_non_overlap_flags;
		allocate_device_int_array(&d_non_overlap_flags, mat1.count+1);

		// Setting initial status of all flags to non_overlap
		find_dimensions_of_blocks_and_threads(mat1.count, grids, blocks);
		fill_array_with_value_kernel<<<grids,blocks>>>(d_non_overlap_flags, 1, mat1.count);

		// move overlap elements and set status of flag to overlap
		find_dimensions_of_blocks_and_threads(mat1.count, grids, blocks);
		move_overlap_of_coo_in_csr_kernel<<<grids,blocks>>>(d_non_overlap_flags, mat1.data, mat1.rowIndex, mat1.colIndex,
										 mat2.data, mat2.colIndex,  mat2.rowPtr, mat1.count);
	move_timer.Stop();		

	//print_device_coosr_matrix(mat2);

	compress_timer.Start();
		// Compressing the output
		thrust::device_ptr<int> d_non_overlap_flags_thrust(d_non_overlap_flags);
		thrust::exclusive_scan(d_non_overlap_flags_thrust, d_non_overlap_flags_thrust + mat1.count + 1, d_non_overlap_flags_thrust);

		int cmprsd_nnz;
		transfer_int_array_device_to_host(d_non_overlap_flags+mat1.count , &cmprsd_nnz, 1);

		allocate_and_memset_device_coosr_matrix(mat1.rows, mat1.cols, cmprsd_nnz, d_cmprsd_mat);

		// Gathering non removed data
		find_dimensions_of_blocks_and_threads(mat1.count, grids, blocks);
		fill_coo_data_kernel<<<grids,blocks>>>(mat1.data, mat1.rowIndex, mat1.colIndex,
							d_cmprsd_mat.data, d_cmprsd_mat.rowIndex, d_cmprsd_mat.colIndex, d_non_overlap_flags, mat1.count);

		// Generating rowPtr for cmprsd_output
		find_dimensions_of_blocks_and_threads(mat1.rows+1, grids, blocks);
		fill_values_with_requested_indices<<<grids,blocks>>>(mat1.rowPtr, d_non_overlap_flags, d_cmprsd_mat.rowPtr, mat1.rows+1);

	compress_timer.Stop();

	move_time = move_timer.Elapsed();
	compress_time = compress_timer.Elapsed();

	float total_time = move_timer.Elapsed() + compress_timer.Elapsed();
	// printf("total time %.3f move time %.4f compress time %.4f \n",total_time, move_timer.Elapsed(), compress_timer.Elapsed());
}

void merge_with_out_conversion_to_2piece(cusparseHandle_t cusparse_handle, coosr_matrix d_sparse_mband_output, coosc_matrix d_mband_sparse_output_coosc, coosr_matrix d_sparse_sparse_output, coosr_matrix &d_merged_output, float &merge_time, float &sort_time){
	int DEBUG = 0;
	dim3 grids, blocks;

	GpuTimer sort_coosc_by_row_timer;
	float spb_spsp_merge_time = 0;
	float bsp_move_time = 0, bsp_compress_time = 0;


	if(DEBUG) print_device_coosc_matrix(d_mband_sparse_output_coosc);

	// SORT coosc_matrix by row
	int* d_indices;
	allocate_and_memset_device_int_array(&d_indices, d_mband_sparse_output_coosc.count);
	find_dimensions_of_blocks_and_threads(d_mband_sparse_output_coosc.count, grids, blocks);
	fill_array_with_tid_kernel<<<grids,blocks>>>(d_indices, d_mband_sparse_output_coosc.count);

	// Allocating memory for row sorted band sparse output
	coosr_matrix d_csc_rowSorted;
	allocate_and_memset_device_coosr_matrix(d_mband_sparse_output_coosc.rows, d_mband_sparse_output_coosc.cols, d_mband_sparse_output_coosc.count, d_csc_rowSorted);

	// Making a copy of band sparse output row indices (so when sorted the original is intact)
	int* d_coosc_row_indices;
	allocate_and_transfer_int_array_device_to_device(d_mband_sparse_output_coosc.rowIndex, &d_coosc_row_indices, d_mband_sparse_output_coosc.count);

	thrust::device_ptr<int> d_indices_thrust(d_indices);
	thrust::device_ptr<int> d_coosc_row_indices_thrust(d_coosc_row_indices);

	if(DEBUG) print_device_int_array(d_coosc_row_indices, d_mband_sparse_output_coosc.count);
	sort_coosc_by_row_timer.Start();
		thrust::sort_by_key(d_coosc_row_indices_thrust, d_coosc_row_indices_thrust + d_mband_sparse_output_coosc.count, d_indices_thrust);

		if(DEBUG) print_device_int_array(d_indices, d_mband_sparse_output_coosc.count);

		find_dimensions_of_blocks_and_threads(d_mband_sparse_output_coosc.count, grids, blocks);
		shuffle_matrix_data_using_rowIndices<<<grids,blocks>>>(d_indices, d_mband_sparse_output_coosc.data, d_mband_sparse_output_coosc.rowIndex, d_mband_sparse_output_coosc.colIndex,
											 d_csc_rowSorted.data, d_csc_rowSorted.rowIndex, d_csc_rowSorted.colIndex, d_mband_sparse_output_coosc.count);

		// Generating row pointer
		cusparseXcoo2csr(cusparse_handle, d_csc_rowSorted.rowIndex,
		                d_csc_rowSorted.count, d_csc_rowSorted.rows, d_csc_rowSorted.rowPtr, CUSPARSE_INDEX_BASE_ZERO);
		cudaDeviceSynchronize();
	sort_coosc_by_row_timer.Stop();

	if(DEBUG) print_device_coosr_matrix(d_csc_rowSorted);

	cudaFree(d_indices); cudaFree(d_coosc_row_indices);

	// Merge spb and spsp output using binaray search approach
	coosr_matrix d_outputs[2];
	coosr_matrix d_spb_spsp_merge_output;
	d_outputs[0] = d_sparse_mband_output;
	d_outputs[1] = d_sparse_sparse_output;
	merge_coosr_matrices(d_outputs, 2, d_spb_spsp_merge_output, spb_spsp_merge_time);

	coosr_matrix d_cmprsd_mat;
	move_overlap_of_anycsr_in_csr_and_compress(d_csc_rowSorted , d_spb_spsp_merge_output, d_cmprsd_mat, bsp_move_time, bsp_compress_time);

	float total_time = sort_coosc_by_row_timer.Elapsed() + spb_spsp_merge_time + bsp_move_time + bsp_compress_time;
	merge_time = spb_spsp_merge_time + bsp_move_time + bsp_compress_time;
	sort_time = sort_coosc_by_row_timer.Elapsed();

	/*
	printf("Timtkn 2 UNIQUE CSR %.2f = (%.2f) (%.2f=%.3f + %.3f) (%.2f)\n", total_time, sort_coosc_by_row_timer.Elapsed(), 
		bsp_move_time+bsp_compress_time, bsp_move_time ,bsp_compress_time, 
		spb_spsp_merge_time);
	*/

}

void merge_with_out_conversion_to_3piece(cusparseHandle_t cusparse_handle, coosr_matrix d_sparse_mband_output, coosc_matrix d_mband_sparse_output_coosc, coosr_matrix d_sparse_sparse_output, coosr_matrix &d_merged_output, float &merge_time, float &sort_time){
	int DEBUG = 0;
	int VERIFY = 0;

	dim3 grids, blocks;

	GpuTimer sort_coosc_by_row_timer;
	GpuTimer move_band_sparse_output_timer, compress_band_sparse_output_timer;
	float spb_spsp_move_time = 0, spb_spsp_compress_time = 0;

	// temp 
	coosr_matrix h_spb_output_coosr, h_spsp_output_coosr;
	coosc_matrix h_bsp_output_coosc;
	if(VERIFY){
		// Moving reference outputs from device to host
		allocate_and_transfer_coosr_device_to_host(d_sparse_mband_output, h_spb_output_coosr);
		allocate_and_transfer_coosr_device_to_host(d_sparse_sparse_output, h_spsp_output_coosr);
		allocate_and_transfer_coosc_device_to_host(d_mband_sparse_output_coosc, h_bsp_output_coosc);

	}


	// SORT coosc_matrix by row
	int* d_indices;
	allocate_and_memset_device_int_array(&d_indices, d_mband_sparse_output_coosc.count);
	find_dimensions_of_blocks_and_threads(d_mband_sparse_output_coosc.count, grids, blocks);
	fill_array_with_tid_kernel<<<grids,blocks>>>(d_indices, d_mband_sparse_output_coosc.count);

	// Allocating memory for row sorted band sparse output
	coosr_matrix d_csc_rowSorted;
	allocate_and_memset_device_coosr_matrix(d_mband_sparse_output_coosc.rows, d_mband_sparse_output_coosc.cols, d_mband_sparse_output_coosc.count, d_csc_rowSorted);

	// Making a copy of band sparse output row indices (so when sorted the original is intact)
	int* d_coosc_row_indices;
	allocate_and_transfer_int_array_device_to_device(d_mband_sparse_output_coosc.rowIndex, &d_coosc_row_indices, d_mband_sparse_output_coosc.count);

	thrust::device_ptr<int> d_indices_thrust(d_indices);
	thrust::device_ptr<int> d_coosc_row_indices_thrust(d_coosc_row_indices);

	if(DEBUG) print_device_int_array(d_coosc_row_indices, d_mband_sparse_output_coosc.count);
	sort_coosc_by_row_timer.Start();
		thrust::sort_by_key(d_coosc_row_indices_thrust, d_coosc_row_indices_thrust + d_mband_sparse_output_coosc.count, d_indices_thrust);

		if(DEBUG) print_device_int_array(d_indices, d_mband_sparse_output_coosc.count);

		find_dimensions_of_blocks_and_threads(d_mband_sparse_output_coosc.count, grids, blocks);
		shuffle_matrix_data_using_rowIndices<<<grids,blocks>>>(d_indices, d_mband_sparse_output_coosc.data, d_mband_sparse_output_coosc.rowIndex, d_mband_sparse_output_coosc.colIndex,
											 d_csc_rowSorted.data, d_csc_rowSorted.rowIndex, d_csc_rowSorted.colIndex, d_mband_sparse_output_coosc.count);

		// Generating row pointer
		cusparseXcoo2csr(cusparse_handle, d_csc_rowSorted.rowIndex,
		                d_csc_rowSorted.count, d_csc_rowSorted.rows, d_csc_rowSorted.rowPtr, CUSPARSE_INDEX_BASE_ZERO);
		cudaDeviceSynchronize();
	sort_coosc_by_row_timer.Stop();

	cudaFree(d_indices); cudaFree(d_coosc_row_indices);

	move_band_sparse_output_timer.Start();

		// MOVE overlap elements of bsp rowsorted on to spb and spsp
		int* d_non_overlap_flags;
		allocate_device_int_array(&d_non_overlap_flags, d_csc_rowSorted.count+1);

		// Setting initial status of all flags to non_overlap
		find_dimensions_of_blocks_and_threads(d_csc_rowSorted.count, grids, blocks);
		fill_array_with_value_kernel<<<grids,blocks>>>(d_non_overlap_flags, 1, d_csc_rowSorted.count);

		// move overlap elements and set status of flag to overlap
		find_dimensions_of_blocks_and_threads(d_csc_rowSorted.count, grids, blocks);
		move_overlap_of_coo_in_csr_kernel<<<grids,blocks>>>(d_non_overlap_flags, d_csc_rowSorted.data, d_csc_rowSorted.rowIndex, d_csc_rowSorted.colIndex,
										 d_sparse_mband_output.data, d_sparse_mband_output.colIndex,  d_sparse_mband_output.rowPtr, d_csc_rowSorted.count);
		
		// move overlap elements and set status of flag to overlap
		find_dimensions_of_blocks_and_threads(d_csc_rowSorted.count, grids, blocks);
		move_overlap_of_coo_in_csr_kernel<<<grids,blocks>>>(d_non_overlap_flags, d_csc_rowSorted.data, d_csc_rowSorted.rowIndex, d_csc_rowSorted.colIndex,
										 d_sparse_sparse_output.data, d_sparse_sparse_output.colIndex,  d_sparse_sparse_output.rowPtr, d_csc_rowSorted.count);
			
	move_band_sparse_output_timer.Stop();		

	compress_band_sparse_output_timer.Start();
		// Compressing the output
		thrust::device_ptr<int> d_non_overlap_flags_thrust(d_non_overlap_flags);
		thrust::exclusive_scan(d_non_overlap_flags_thrust, d_non_overlap_flags_thrust + d_csc_rowSorted.count + 1, d_non_overlap_flags_thrust);

		int cmprsd_nnz;
		transfer_int_array_device_to_host(d_non_overlap_flags+d_csc_rowSorted.count , &cmprsd_nnz, 1);

		coosr_matrix d_cmprsd_csc_rowSorted;
		allocate_and_memset_device_coosr_matrix(d_csc_rowSorted.rows, d_csc_rowSorted.cols, cmprsd_nnz, d_cmprsd_csc_rowSorted);

		// Gathering non removed data
		find_dimensions_of_blocks_and_threads(d_csc_rowSorted.count, grids, blocks);
		fill_coo_data_kernel<<<grids,blocks>>>(d_csc_rowSorted.data, d_csc_rowSorted.rowIndex, d_csc_rowSorted.colIndex,
							d_cmprsd_csc_rowSorted.data, d_cmprsd_csc_rowSorted.rowIndex, d_cmprsd_csc_rowSorted.colIndex, d_non_overlap_flags, d_csc_rowSorted.count);

		// Generating rowPtr for cmprsd_output
		find_dimensions_of_blocks_and_threads(d_csc_rowSorted.rows+1, grids, blocks);
		fill_values_with_requested_indices<<<grids,blocks>>>(d_csc_rowSorted.rowPtr, d_non_overlap_flags, d_cmprsd_csc_rowSorted.rowPtr, d_csc_rowSorted.rows+1);
	compress_band_sparse_output_timer.Stop();

	coosr_matrix d_cmprsd_mat;

	if(d_sparse_mband_output.count < d_sparse_sparse_output.count)	
		move_overlap_of_anycsr_in_csr_and_compress(d_sparse_mband_output, d_sparse_sparse_output, d_cmprsd_mat, spb_spsp_move_time, spb_spsp_compress_time);
	else
		move_overlap_of_anycsr_in_csr_and_compress(d_sparse_sparse_output, d_sparse_mband_output, d_cmprsd_mat, spb_spsp_move_time, spb_spsp_compress_time);


	// Timings
	float bsp_spbSpsp_merge_time =	move_band_sparse_output_timer.Elapsed() + compress_band_sparse_output_timer.Elapsed();
	float spb_spsp_merge_time = spb_spsp_move_time + spb_spsp_compress_time;

	float total_time = sort_coosc_by_row_timer.Elapsed() + 	bsp_spbSpsp_merge_time + spb_spsp_merge_time;

	merge_time = spb_spsp_merge_time + bsp_spbSpsp_merge_time;
	sort_time = sort_coosc_by_row_timer.Elapsed();
	/*
	printf("Timtkn 3 UNIQUE CSR %.2f = (%.2f) (%.2f=%.3f + %.3f) (%.2f=%.3f+%.3f))\n", total_time, sort_coosc_by_row_timer.Elapsed(), 
					bsp_spbSpsp_merge_time, move_band_sparse_output_timer.Elapsed() , compress_band_sparse_output_timer.Elapsed(),
					spb_spsp_merge_time, spb_spsp_move_time, spb_spsp_compress_time
					);
	*/
	// VERIFY THE OUTPUT spb, bsp, spsp with spb_spp_merge + cmprsd_
	if(VERIFY){
		
		coosr_matrix d_fin_spb_output, d_fin_bsp_output, d_fin_spsp_output;

		d_fin_bsp_output = d_cmprsd_csc_rowSorted;
		if(d_sparse_mband_output.count < d_sparse_sparse_output.count){
			d_fin_spsp_output = d_sparse_sparse_output;
			d_fin_spb_output = d_cmprsd_mat;
		}else{
			d_fin_spb_output = d_sparse_mband_output;
			d_fin_spsp_output = d_cmprsd_mat;
		}

		coosr_matrix h_fin_spb_output, h_fin_bsp_output, h_fin_spsp_output;

		allocate_and_transfer_coosr_device_to_host(d_fin_spb_output, h_fin_spb_output);
		allocate_and_transfer_coosr_device_to_host(d_fin_bsp_output, h_fin_bsp_output);
		allocate_and_transfer_coosr_device_to_host(d_fin_spsp_output, h_fin_spsp_output);
		
		block_matrix h_spb_output_block, h_spsp_output_block;
		block_matrix h_bsp_output_block;

		block_matrix h_fin_spb_output_block, h_fin_spsp_output_block;
		block_matrix h_fin_bsp_output_block;

		coosr_to_block(h_spb_output_coosr, h_spb_output_block);
		coosr_to_block(h_spsp_output_coosr, h_spsp_output_block);
		coosc_to_block(h_bsp_output_coosc, h_bsp_output_block);

		coosr_to_block(h_fin_spb_output, h_fin_spb_output_block);
		coosr_to_block(h_fin_bsp_output, h_fin_bsp_output_block);
		coosr_to_block(h_fin_spsp_output, h_fin_spsp_output_block);

		/*
		print_block_matrix(h_spb_output_block);
		print_block_matrix(h_bsp_output_block);
		print_block_matrix(h_spsp_output_block);

		print_block_matrix(h_fin_spb_output_block);
		print_block_matrix(h_fin_bsp_output_block);
		print_block_matrix(h_fin_spsp_output_block);
		*/

		block_matrix reference_block;
		block_matrix output_block;

		block_matrix temp_block;
		traditional_matrix_addition(h_spb_output_block, h_spsp_output_block, temp_block);
		traditional_matrix_addition(temp_block, h_bsp_output_block, reference_block);

		block_matrix temp_block_one;
		traditional_matrix_addition(h_fin_spb_output_block, h_fin_spsp_output_block, temp_block_one);
		traditional_matrix_addition(temp_block_one, h_fin_bsp_output_block, output_block);

		//print_block_matrix(reference_block);
		//print_block_matrix(output_block);
		

		are_matrices_equal(reference_block, output_block);
	}


	// concatenate the above two outputs. (output is partially column separated)


}


// CPU Kernels

// CPU dia dia multiplication
void multiply_dia_dia_matrices(float* h_A_data, int* h_A_offsets, int A_num_dias,
							  float* h_B_data, int* h_B_offsets, int B_num_dias,
							  int A_rows, int A_cols, int B_cols, float* output){

	int i,j,k;
	int offset_A, offset_B;
	int B_rows = A_cols;

	for(i=0;i<A_num_dias;i++){
		for(j=0;j<B_num_dias;j++){
			// multiplication of one dia from A and one dia from B
			offset_A = h_A_offsets[i];
			offset_B = h_B_offsets[j];

			for(k=0;k<A_rows;k++){
				if(k+offset_A >= 0 && (k+offset_A+offset_B) >=0 && (k+offset_A+offset_B) < B_cols){
					output[k*B_cols + (k+offset_A+offset_B)] += h_A_data[i*A_rows + k] * h_B_data[j*B_rows + k + offset_A];
				}
			}

		}
	}
}

// CPU rowcsr dia multiplication
void multiply_coosr_band_matrices(coosr_matrix A, band_matrix B, float* output){
	int i,j,k;
	int rowCount,rowPtr;
	int B_offset;

	// looping each row
	for(i=0;i<A.rows;i++){
		rowCount = A.rowPtr[i+1] - A.rowPtr[i];
		rowPtr = A.rowPtr[i];

		if(rowCount > 0){
			// looping each dia
			for(j=0;j<B.count;j++){	
				B_offset = B.offsets[j];

				// multiplying a row with a dia
				int index;
				for(k=0;k<rowCount;k++){
					index = A.colIndex[rowPtr + k];
					if((index + B_offset) >= 0 && (index + B_offset) < B.cols){
						output[i*B.cols + (index+B_offset)] += A.data[rowPtr + k]*B.data[j*B.rows + index];
					}
				}
			}
		}
	}
}

// cpu dia colcsr multiplication
void multiply_band_coosc_matrices(band_matrix A, coosc_matrix B, float* output){
	int i,j,k;
	int colCount,colPtr;
	int A_offset;

	// looping each col
	for(i=0;i<B.cols;i++){
		colCount = B.colPtr[i+1] - B.colPtr[i];
		colPtr = B.colPtr[i];

		if(colCount > 0){
			// looping each dia
			for(j=0;j<A.count;j++){
				A_offset = A.offsets[j];

				// multiplying a col with a dia
				int index;
				for(k=0;k<colCount;k++){
					index = B.rowIndex[colPtr + k];
					if((index-A_offset) >= 0 && (index-A_offset) < A.rows){
						output[(index-A_offset)*B.cols + i] += B.data[colPtr + k] * A.data[j*A.rows + (index-A_offset)];
					}

				}
			}
		}
	}
}

// cpu rowcsr colcsr multiplication
void multiply_rowcsr_colcsr_matrices(coosr_matrix A,coosc_matrix B,float* output){

	for (int i = 0; i < A.rows; ++i)
	{
		int rowPtr = A.rowPtr[i];
		int rowCount = A.rowPtr[i+1] - rowPtr;

		if(rowCount > 0){
			for (int j = 0; j < B.cols; ++j)
			{
				int colPtr = B.colPtr[j];
				int colCount = B.colPtr[j+1]-colPtr;
				
				if(colCount > 0){
					float sum = 0;
					int k,l;
					
					k = rowPtr;
					l = colPtr;

					int A_fix = 1; // flag to denote whether we  are fixing A and traversing B or otherwise
					while(k < rowPtr+rowCount && l < colPtr+colCount){
						if(A_fix){
							if(B.rowIndex[l] < A.colIndex[k])
								l++;
							else{
								if(B.rowIndex[l] == A.colIndex[k]){
									sum += A.data[k] * B.data[l];
									k++; l++;
								}
								A_fix = (A_fix + 1)%2;
							}
						}else{
							if(A.colIndex[k] < B.rowIndex[l])
								k++;
							else{
								if(A.colIndex[k] == B.rowIndex[l]){
									sum += A.data[k] * B.data[l];
									k++; l++;
								}
								A_fix = (A_fix + 1)%2;
							}
						}
					}

					output[i*B.cols + j] = sum;
				}
			}
		
		}
		
	}

	/*
	for(i=0;i<A.count;i++)
		for(j=0;j<B.count;j++)
			if(A.colIndex[i] == B.rowIndex[j])
				output[A.rowIndex[i] * B.cols + B.colIndex[j]] += A.data[i]*B.data[j];
	*/
			
}

// MATCHING OUTPUT related
void match_dia_dia_output(int A_rows, int A_cols, int B_cols,
						   float* d_A_dia_data, int* d_A_dia_offsets, int A_num_dias,
						   float* d_B_dia_data, int* d_B_dia_offsets, int B_num_dias,
						   float* d_dia_dia_output_data, int* d_dia_dia_output_offsets, int C_num_dias){
	float* h_A_dia_data, *h_B_dia_data;
	int* h_A_dia_offsets, *h_B_dia_offsets;

	float* h_dia_dia_output_data;
	int* h_dia_dia_output_offsets;

	block_matrix output, reference;
	output.rows = A_rows; output.cols = B_cols;
	reference.rows = A_rows; reference.cols = B_cols;

	allocate_and_transfer_float_array_device_to_host(d_A_dia_data, &h_A_dia_data, A_num_dias*A_rows);
	allocate_and_transfer_int_array_device_to_host(d_A_dia_offsets, &h_A_dia_offsets, A_num_dias);

	allocate_and_transfer_float_array_device_to_host(d_B_dia_data, &h_B_dia_data, B_num_dias*A_cols);
	allocate_and_transfer_int_array_device_to_host(d_B_dia_offsets, &h_B_dia_offsets, B_num_dias);

	allocate_and_transfer_float_array_device_to_host(d_dia_dia_output_data, &h_dia_dia_output_data, C_num_dias*A_rows);
	allocate_and_transfer_int_array_device_to_host(d_dia_dia_output_offsets, &h_dia_dia_output_offsets, C_num_dias);

	// Allocating memory for CPU output reference
	allocate_host_float_array(&reference.data, A_rows*B_cols);
	allocate_host_float_array(&output.data, A_rows*B_cols);

	multiply_dia_dia_matrices(h_A_dia_data, h_A_dia_offsets, A_num_dias,
							  h_B_dia_data, h_B_dia_offsets, B_num_dias,
							  A_rows, A_cols, B_cols, reference.data);

	// converting Device dia data into block
	int row,col,offset;
	for (int i = 0; i < C_num_dias; ++i)
	{
		offset = h_dia_dia_output_offsets[i];

		for (int j = 0; j < A_rows; ++j)
		{	
			row = j;
			col = j + offset;

			if(col >=0 && col <= B_cols-1){
				output.data[row*A_rows + col] = h_dia_dia_output_data[i*A_rows + j];
			}
			
		}

	}



	are_matrices_equal(output, reference);

}

void match_coosr_dia_output(coosr_matrix d_A_sparse, band_matrix* d_B_bands, int B_num_bands, coosr_matrix d_output){
	coosr_matrix h_output;
	allocate_and_transfer_coosr_device_to_host(d_output, h_output);

	coosr_matrix h_A_sparse;
	band_matrix* h_B_bands = (band_matrix*) calloc(B_num_bands, sizeof(band_matrix));

	allocate_and_transfer_coosr_device_to_host(d_A_sparse, h_A_sparse);
	for (int i = 0; i < B_num_bands; ++i)
	{
		allocate_and_transfer_band_device_to_host(d_B_bands[i], h_B_bands[i]);
	}

	block_matrix output_block, reference_block;

	coosr_to_block(h_output, output_block);

	reference_block.rows = d_A_sparse.rows ; reference_block.cols = d_B_bands[0].cols;
	reference_block.data = (float*) calloc(d_A_sparse.rows*d_B_bands[0].cols, sizeof(float));

	for (int i = 0; i < B_num_bands; ++i)
	{
		multiply_coosr_band_matrices(h_A_sparse, h_B_bands[i], reference_block.data);
	}

	//print_block_matrix(output_block);
	//print_block_matrix(reference_block);

	are_matrices_equal(output_block, reference_block);
}


void match_dia_coosc_output(band_matrix* d_A_bands, coosc_matrix d_B_sparse, int A_num_bands, coosr_matrix d_output){
	coosr_matrix h_output;
	allocate_and_transfer_coosr_device_to_host(d_output, h_output);

	coosc_matrix h_B_sparse;
	band_matrix* h_A_bands = (band_matrix*) calloc(A_num_bands, sizeof(band_matrix));

	allocate_and_transfer_coosc_device_to_host(d_B_sparse, h_B_sparse);
	for (int i = 0; i < A_num_bands; ++i)
	{
		allocate_and_transfer_band_device_to_host(d_A_bands[i], h_A_bands[i]);
	}

	block_matrix output_block, reference_block;

	coosr_to_block(h_output, output_block);

	reference_block.rows = d_B_sparse.rows ; reference_block.cols = d_A_bands[0].cols;
	reference_block.data = (float*) calloc(d_B_sparse.rows*d_A_bands[0].cols, sizeof(float));

	for (int i = 0; i < A_num_bands; ++i)
	{
		multiply_band_coosc_matrices(h_A_bands[i], h_B_sparse, reference_block.data);
	}

	// print_block_matrix(output_block);
	// print_block_matrix(reference_block);

	are_matrices_equal(output_block, reference_block);
}

void match_sparse_sparse_output(coosr_matrix d_A_sparse, coosc_matrix d_B_sparse, coosr_matrix d_output){
	coosr_matrix h_A_sparse;
	allocate_and_transfer_coosr_device_to_host(d_A_sparse, h_A_sparse);
	coosc_matrix h_B_sparse;
	allocate_and_transfer_coosc_device_to_host(d_B_sparse, h_B_sparse);

	block_matrix reference_block, output_block;
	coosr_matrix h_output;
	allocate_and_transfer_coosr_device_to_host(d_output, h_output);

	reference_block.rows = d_A_sparse.rows; reference_block.cols = d_B_sparse.cols;
	allocate_host_float_array(&reference_block.data, d_A_sparse.rows * d_B_sparse.cols);
	multiply_rowcsr_colcsr_matrices(h_A_sparse, h_B_sparse, reference_block.data);

	coosr_to_block(h_output, output_block);

	//print_block_matrix(output_block);
	//print_block_matrix(reference_block);

	are_matrices_equal(output_block, reference_block);

}

void match_consolidated_output(float* d_dia_dia_output_data, int* d_dia_dia_output_offsets, int num_dias, 
	coosr_matrix d_sparse_band, coosc_matrix d_band_sparse, coosr_matrix d_sparse_sparse, coosr_matrix d_coosr_reference){

	int A_rows = d_sparse_band.rows;
	int B_cols = d_sparse_band.cols;

	block_matrix output_block;
	output_block.rows = d_sparse_band.rows;
	output_block.cols = d_sparse_band.cols;
	output_block.data = (float*) calloc(d_sparse_band.rows * d_sparse_band.cols, sizeof(float));

	// Transfering dia dia output data to host
	float* h_dia_dia_output_data;
	int* h_dia_dia_output_offsets;

	allocate_and_transfer_float_array_device_to_host(d_dia_dia_output_data, &h_dia_dia_output_data, num_dias*A_rows);
	allocate_and_transfer_int_array_device_to_host(d_dia_dia_output_offsets, &h_dia_dia_output_offsets, num_dias);

	// converting Device dia data into block
	int row,col,offset;
	for (int i = 0; i < num_dias; ++i)
	{
		offset = h_dia_dia_output_offsets[i];

		for (int j = 0; j < A_rows; ++j)
		{	
			row = j;
			col = j + offset;

			if(col >=0 && col <= B_cols-1){
				output_block.data[row*A_rows + col] += h_dia_dia_output_data[i*A_rows + j];
			}
			
		}

	}

	coosr_matrix h_sparse_band, h_sparse_sparse;
	coosc_matrix h_band_sparse;
	allocate_and_transfer_coosr_device_to_host(d_sparse_band, h_sparse_band);
	allocate_and_transfer_coosc_device_to_host(d_band_sparse, h_band_sparse);
	allocate_and_transfer_coosr_device_to_host(d_sparse_sparse, h_sparse_sparse);

	for (int i = 0; i < h_sparse_band.count; ++i)
	{
		row = h_sparse_band.rowIndex[i];
		col = h_sparse_band.colIndex[i];
		output_block.data[row*B_cols + col] += h_sparse_band.data[i];
	}

	for (int i = 0; i < h_band_sparse.count; ++i)
	{
		row = h_band_sparse.rowIndex[i];
		col = h_band_sparse.colIndex[i];
		output_block.data[row*B_cols + col] += h_band_sparse.data[i];
	}

	for (int i = 0; i < h_sparse_sparse.count; ++i)
	{
		row = h_sparse_sparse.rowIndex[i];
		col = h_sparse_sparse.colIndex[i];
		output_block.data[row*B_cols + col] += h_sparse_sparse.data[i];
	}

	block_matrix reference_block;
	coosr_matrix h_coosr_reference;
	allocate_and_transfer_coosr_device_to_host(d_coosr_reference, h_coosr_reference);
	coosr_to_block(h_coosr_reference, reference_block);

	are_matrices_equal(output_block, reference_block);

}

int fill_synthetic_matrix(cusp::coo_matrix<int, float, cusp::host_memory> &synth, int rows, int cols, 
								float nnz_perc, float band_perc, float band_occupancy, int experiment, int seed){

	int DEBUG = 0;
	float sparse_perc = 100 - band_perc;

	int FILL_FLOAT = 0;
	int FILL_ROW_BLOCK = 1;
	int FILL_COLUMN_BLOCK = 2;

	if(DEBUG){
		if(experiment == 0)
			printf("SPARSE DISTRIBUTION: RANDOM\n");
		if(experiment == FILL_ROW_BLOCK)
			printf("SPARSE DISTRIBUTION: ROW-BLOCK\n");
		if(experiment == FILL_COLUMN_BLOCK)
			printf("SPARSE DISTRIBUTION: COLUMN-BLOCK\n");
	}

	srand(time(0) + seed);

	// Caluculating nnz of band and sparse
	int nnz_in_matrix = (int)(nnz_perc*(rows*cols))/100;	
	int nnz_in_band = (int) ((band_perc * (nnz_in_matrix))/100);
	int nnz_in_sparse = nnz_in_matrix-nnz_in_band;

	// printf("nnz_in_matrix %d nnz_in_band %d nnz_in_sparse %d\n", nnz_in_matrix, nnz_in_band, nnz_in_sparse);
	
	// nnz for a diagonal in band
	int dia_occupancy_count = (int) ((band_occupancy * rows)/100);
	int dia_count = ceil((float)nnz_in_band/dia_occupancy_count); // calculating required diagonals

	// Calculating left and right offset of band
	int left_offset = -1 * dia_count/2;
	int right_offset = left_offset + dia_count - 1;
	int base_offset = rows-1;

	// Setting up band nnz and sparse nnz
	int synth_band_nnz = dia_count*rows - ((left_offset*(left_offset-1))/2) - ((right_offset*(right_offset+1))/2); // includes zero elements in band also
	int synth_sparse_nnz = nnz_in_sparse;
	int synth_nnz = synth_band_nnz + synth_sparse_nnz;

	// printf("synth_nnz %d synth_band_nnz %d synth_sparse_nnz %d\n", synth_nnz, synth_band_nnz, synth_sparse_nnz);

	int block_size = ceil((float)synth_sparse_nnz/rows);
	if(experiment){
		// If experiment sparse nnz has to be computed
		synth_sparse_nnz = block_size*rows; // Max space required
		synth_nnz = synth_band_nnz + synth_sparse_nnz;
	}

	if(synth_sparse_nnz + synth_band_nnz > rows*cols){
		printf("Impossible to create synth matrix with %.2f band_perc and %.2f sparse_perc\n", band_perc, sparse_perc);
		return 0;
	}

	//printf("left_offset %d right_offset %d dia_count %d synth_band_nnz %d synth_sparse_nnz %d\n", left_offset,right_offset, dia_count, synth_band_nnz, synth_sparse_nnz);

	// initalizing matrix
	synth.resize(rows, cols, synth_nnz);

	// fill band data (square matrices)
	int index = 0;
	int row_start, row_end, row_start_index;
	
	for (int offset = left_offset; offset <= right_offset; ++offset)
	{
		row_start = -1*min(0,offset);
		row_end = min(rows-1,rows-1-offset);
		row_start_index = index;

		// Filling that diagonal
		for (int row = row_start; row <= row_end; ++row)
		{
			synth.row_indices[index] = row;
			synth.column_indices[index] = row+offset;
			if(FILL_FLOAT)
				synth.values[index] = rand()%10 + (float)(rand()%100)/100;
			else
				synth.values[index] = rand()%10 + 1;
			index++;
		}

		// Making some elements to zero to acheive band occupancy
		int num_zero_to_fill = -1*min(0, -1*(rows - (int)ceil(((band_occupancy*rows)/100)) - abs(offset)) );

		int zeros_filled = 0;
		int row;

		while(zeros_filled != num_zero_to_fill){
			row = rand()%rows;
			if(row >= row_start && row <= row_end){
				synth.values[row_start_index + (row - row_start)] = 0;
				zeros_filled++;
			}
		}

		// compressing the index
		int ptr = row_start_index;
		for (int i = row_start_index; i < index ; ++i)
		{
			if(synth.values[i] != 0){
				synth.row_indices[ptr] = synth.row_indices[i];
				synth.column_indices[ptr] = synth.column_indices[i];
				synth.values[ptr] = synth.values[i];

				ptr++;
			}
		}

		// setting index after compressing
		index = ptr;

	}

	//printf("synth_band_nnz %d index %d\n",nnz_in_band, index );
	
	int row, col, dia_offset;
	if(experiment){
		for (int j = 0; j < block_size; ++j)
		{
			for (int i = 0; i < cols; ++i)
			{
				// fill last row block
				row = (rows-1) - j;
				col = i;
				dia_offset = col-row;

				if( !(dia_offset >= left_offset && dia_offset <= right_offset) 
					or !(-1*dia_offset >= left_offset && -1*dia_offset <= right_offset)){

					if(experiment == FILL_COLUMN_BLOCK){
						synth.row_indices[index] = col;
						synth.column_indices[index] = row;
					}else{
						synth.row_indices[index] = row;
						synth.column_indices[index] = col;
					}
					
					if(FILL_FLOAT)
						synth.values[index] = rand()%10 + (float)(rand()%100)/(float)100;
					else
						synth.values[index] = rand()%10 + 1;
					index++;
				}

			}
		}
	}else{
		std::map<int,int> flags;
		while(flags.size() < synth_sparse_nnz){
			row = rand() % rows;
			col = rand() % cols;
			dia_offset = col-row;

			// check whether genrtd row and col is out of band
			if(dia_offset < left_offset || dia_offset > right_offset){
				// check whether random element can be inserted
				if(flags.insert( std::pair<int,int>(row*cols+col, 1)).second){

					// append to cusp coo
					synth.row_indices[index] = row;
					synth.column_indices[index] = col;
					if(FILL_FLOAT)
						synth.values[index] = rand()%10 + 1 + (float)(rand()%100)/(float)100;
					else
						synth.values[index] = rand()%10 + 1;
					index++;
				}
			}
		}
	}

	// Because synth nnz has more elements
	synth.resize(rows,cols,index);

	//printf("synth_sparse_nnz %d index %d\n", synth_sparse_nnz, index- nnz_in_band);
	
	if(!experiment && index < nnz_in_matrix && (rows+left_offset) > (dia_occupancy_count)){
		printf("FAILURE IN GENERATING SYNTH MATRIX %d is nnz we filled %d have to be filled\n", index, nnz_in_matrix );
		return 0;		
	}
	// as we are not generating randomly we need to do this.
	synth.sort_by_row_and_column();

	return 1;

}

void traditional_matrix_multiplication(block_matrix A, block_matrix B, block_matrix &C){
	C.rows = A.rows;
	C.cols = B.cols;
	C.data = (float*) calloc(A.rows*B.cols, sizeof(float));

	int sum;
	for (int i = 0; i < A.rows; ++i)
	{
		for (int j = 0; j < B.cols; ++j)
		{
			sum = 0;
			for (int k = 0; k < A.cols; ++k)
			{
				sum += A.data[i*A.cols+k] * B.data[k*B.cols+j];
			}
			C.data[i*A.rows + j] = sum;
		}
	}
}

void traditional_matrix_addition(block_matrix A, block_matrix B, block_matrix &C){
	if(A.rows != B.rows || A.cols != B.cols){
		printf("Cannot add matrices of different dimensions\n");
		return;
	}

	C.rows = A.rows;
	C.cols = B.cols;
	C.data = (float*) calloc(A.rows*B.cols, sizeof(float));

	
	for (int i = 0; i < A.rows; ++i)
	{
		for (int j = 0; j < B.cols; ++j)
		{
			C.data[i*A.rows + j] = A.data[i*A.rows + j] + B.data[i*A.rows + j];
		}
	}
}


void are_matrices_equal(block_matrix mat1, block_matrix mat2){
	bool equal = 1;
	float tol = 5.0e-1f;
	int i,j;
	int bi,bj;
	int mat1_nnz_count = 0;
	int mat2_nnz_count = 0;

	int differences = 0;

	for(i=0;i<mat1.rows;i++){
		for(j=0;j<mat2.cols;j++){
			if(mat1.data[i*mat2.cols+j] !=0 )
				mat1_nnz_count++;
			if(mat2.data[i*mat2.cols+j] != 0)
				mat2_nnz_count++;
			if(fabs(mat1.data[i*mat2.cols + j] - mat2.data[i*mat2.cols + j]) > tol){
				differences++;
				bi = i;
				bj = j;
			}
		}
	}

	if(differences == 0)
		printf("matrices are same :) mat1_nnz_count %d mat2_nnz_count %d\n",mat1_nnz_count, mat2_nnz_count);
	else
		printf("matrices differed at %d locations (%d,%d) with values (%7.2f,%7.2f)\n",differences, bi,bj,mat1.data[bi*mat2.cols + bj],mat2.data[bi*mat2.cols + bj]);
}



double timer() {
    struct timeval tp;
    gettimeofday(&tp, NULL);
    return ((double) (tp.tv_sec) + 1e-6 * tp.tv_usec);
}


void perform_cusparse_coosr_coosr(cusparseHandle_t cusparse_handle, coosr_matrix A, coosr_matrix B, coosr_matrix &C, float &preprocessing_time, float &processing_time){

	int DEBUG = 0;

	GpuTimer cusparse_preprocessing_timer;
	GpuTimer cusparse_processing_timer;

	cusparseMatDescr_t cusparse_descrA = 0;
	cusparseMatDescr_t cusparse_descrB = 0;
	cusparseMatDescr_t cusparse_descrC = 0;

	// create and setup matrix descriptors A, B & C
	cusparseCreateMatDescr(&cusparse_descrA);        
	cusparseSetMatType(cusparse_descrA, CUSPARSE_MATRIX_TYPE_GENERAL);
	cusparseSetMatIndexBase(cusparse_descrA, CUSPARSE_INDEX_BASE_ZERO);  

	cusparseCreateMatDescr(&cusparse_descrB);        
	cusparseSetMatType(cusparse_descrB, CUSPARSE_MATRIX_TYPE_GENERAL);
	cusparseSetMatIndexBase(cusparse_descrB, CUSPARSE_INDEX_BASE_ZERO);  

	cusparseCreateMatDescr(&cusparse_descrC);        
	cusparseSetMatType(cusparse_descrC, CUSPARSE_MATRIX_TYPE_GENERAL);
	cusparseSetMatIndexBase(cusparse_descrC, CUSPARSE_INDEX_BASE_ZERO);  

	int cusparse_baseC;
	int h_cusparse_nnz;
	int *cusparse_nnzTotalDevHostPtr = &h_cusparse_nnz;
	cusparseSetPointerMode(cusparse_handle, CUSPARSE_POINTER_MODE_HOST);

	cudaMalloc((void**)&C.rowPtr, sizeof(int) * (A.rows+1));

	cusparse_preprocessing_timer.Start();
	// preliminary operation to know nnz in CSR(rXc) output 
	cusparseStatus_t c_first = cusparseXcsrgemmNnz(cusparse_handle, CUSPARSE_OPERATION_NON_TRANSPOSE, CUSPARSE_OPERATION_NON_TRANSPOSE, A.rows, A.cols, B.cols, 
	                    cusparse_descrA, A.count, A.rowPtr, A.colIndex,
	                    cusparse_descrB, B.count, B.rowPtr, B.colIndex,
	                    cusparse_descrC, C.rowPtr, cusparse_nnzTotalDevHostPtr );
	cusparse_preprocessing_timer.Stop();

	if(DEBUG) std::cout << "pre processing status" << c_first << std::endl;


	if (NULL != cusparse_nnzTotalDevHostPtr){
	    h_cusparse_nnz = *cusparse_nnzTotalDevHostPtr;
	}else{
	    cudaMemcpy(&h_cusparse_nnz, (C.rowPtr)+A.rows, sizeof(int), cudaMemcpyDeviceToHost);
	    cudaMemcpy(&cusparse_baseC, (C.rowPtr), sizeof(int), cudaMemcpyDeviceToHost);
	    h_cusparse_nnz -= cusparse_baseC;
	}

	// Allocate data and colIndex based on obtained nnz
	cudaMalloc((void**)&C.data, sizeof(float) * h_cusparse_nnz);
	cudaMalloc((void**)&C.colIndex, sizeof(int) * h_cusparse_nnz);
	cudaMalloc((void**)&C.rowIndex, sizeof(int) * (h_cusparse_nnz));

	cusparse_processing_timer.Start();
		// calling spmm kernel.
		cusparseStatus_t c_second = cusparseScsrgemm(cusparse_handle, CUSPARSE_OPERATION_NON_TRANSPOSE, CUSPARSE_OPERATION_NON_TRANSPOSE, A.rows, B.rows, B.cols,
	                 cusparse_descrA, A.count, A.data, A.rowPtr, A.colIndex,
	                 cusparse_descrB, B.count, B.data, B.rowPtr, B.colIndex,
	                 cusparse_descrC, C.data, C.rowPtr, C.colIndex);

		
	// Generating coo data
	cusparseXcsr2coo(cusparse_handle, C.rowPtr,
                 h_cusparse_nnz, A.rows, C.rowIndex,
                 CUSPARSE_INDEX_BASE_ZERO);
	
	cusparse_processing_timer.Stop();

		
	
	//cudaDeviceSynchronize();
	if(DEBUG) std::cout << "processing status" << c_second << std::endl;
	cusparseDestroyMatDescr(cusparse_descrA);
	cusparseDestroyMatDescr(cusparse_descrB);
	cusparseDestroyMatDescr(cusparse_descrC);

	
	C.rows = A.rows;
	C.cols = A.cols;
	C.count = h_cusparse_nnz;
	preprocessing_time = cusparse_preprocessing_timer.Elapsed();
	processing_time = cusparse_processing_timer.Elapsed();
}

void perform_cusparse_coosr_coosc(cusparseHandle_t cusparse_handle, coosr_matrix A, coosc_matrix B, coosr_matrix &C, float &preprocessing_time, float &processing_time){
	int DEBUG = 0;

	GpuTimer cusparse_preprocessing_timer;
	GpuTimer cusparse_processing_timer;

	cusparseMatDescr_t cusparse_descrA = 0;
	cusparseMatDescr_t cusparse_descrB = 0;
	cusparseMatDescr_t cusparse_descrC = 0;

	// create and setup matrix descriptors A, B & C
	cusparseCreateMatDescr(&cusparse_descrA);        
	cusparseSetMatType(cusparse_descrA, CUSPARSE_MATRIX_TYPE_GENERAL);
	cusparseSetMatIndexBase(cusparse_descrA, CUSPARSE_INDEX_BASE_ZERO);  

	cusparseCreateMatDescr(&cusparse_descrB);        
	cusparseSetMatType(cusparse_descrB, CUSPARSE_MATRIX_TYPE_GENERAL);
	cusparseSetMatIndexBase(cusparse_descrB, CUSPARSE_INDEX_BASE_ZERO);  

	cusparseCreateMatDescr(&cusparse_descrC);        
	cusparseSetMatType(cusparse_descrC, CUSPARSE_MATRIX_TYPE_GENERAL);
	cusparseSetMatIndexBase(cusparse_descrC, CUSPARSE_INDEX_BASE_ZERO);  

	int cusparse_baseC;
	int h_cusparse_nnz;
	int *cusparse_nnzTotalDevHostPtr = &h_cusparse_nnz;
	cusparseSetPointerMode(cusparse_handle, CUSPARSE_POINTER_MODE_HOST);

	cudaMalloc((void**)&C.rowPtr, sizeof(int) * (A.rows+1));

	cusparse_preprocessing_timer.Start();
	// preliminary operation to know nnz in CSR(rXc) output 
	cusparseStatus_t c_first = cusparseXcsrgemmNnz(cusparse_handle, CUSPARSE_OPERATION_NON_TRANSPOSE, CUSPARSE_OPERATION_TRANSPOSE, A.rows, A.cols, B.cols, 
	                    cusparse_descrA, A.count, A.rowPtr, A.colIndex,
	                    cusparse_descrB, B.count, B.colPtr, B.rowIndex,
	                    cusparse_descrC, C.rowPtr, cusparse_nnzTotalDevHostPtr );
	cusparse_preprocessing_timer.Stop();

	if(DEBUG) std::cout << "pre processing status" << c_first << std::endl;


	if (NULL != cusparse_nnzTotalDevHostPtr){
	    h_cusparse_nnz = *cusparse_nnzTotalDevHostPtr;
	}else{
	    cudaMemcpy(&h_cusparse_nnz, (C.rowPtr)+A.rows, sizeof(int), cudaMemcpyDeviceToHost);
	    cudaMemcpy(&cusparse_baseC, (C.rowPtr), sizeof(int), cudaMemcpyDeviceToHost);
	    h_cusparse_nnz -= cusparse_baseC;
	}

	// Allocate data and colIndex based on obtained nnz
	cudaMalloc((void**)&C.data, sizeof(float) * h_cusparse_nnz);
	cudaMalloc((void**)&C.colIndex, sizeof(int) * h_cusparse_nnz);

	cusparse_processing_timer.Start();
	// calling spmm kernel.
	cusparseStatus_t c_second = cusparseScsrgemm(cusparse_handle, CUSPARSE_OPERATION_NON_TRANSPOSE, CUSPARSE_OPERATION_TRANSPOSE, A.rows, B.rows, B.cols,
	                 cusparse_descrA, A.count, A.data, A.rowPtr, A.colIndex,
	                 cusparse_descrB, B.count, B.data, B.colPtr, B.rowIndex,
	                 cusparse_descrC, C.data, C.rowPtr, C.colIndex);
	cusparse_processing_timer.Stop();

	//cudaDeviceSynchronize();
	if(DEBUG) std::cout << "processing status" << c_second << std::endl;
	cusparseDestroyMatDescr(cusparse_descrA);
	cusparseDestroyMatDescr(cusparse_descrB);
	cusparseDestroyMatDescr(cusparse_descrC);

	
	C.rows = A.rows;
	C.cols = A.cols;
	C.count = h_cusparse_nnz;
	preprocessing_time = cusparse_preprocessing_timer.Elapsed();
	processing_time = cusparse_processing_timer.Elapsed();
}

// For a given number of threads this function generates the kernel configuration 
// parameters with respect to the GPU used.
void find_dimensions_of_blocks_and_threads(int total_threads, dim3 &grids, dim3 &blocks){
	// If max threads per block is 1024 and max threads per sm is 2048 and alloted=max_threads_per_block (only 1 block is scheduled per sm)
	// If it is 512 atleast 2 will be scheduled

	int DEBUG = 0;

	int num_threads_per_block = deviceProp.maxThreadsPerBlock;
	int num_cores_per_sm = 192;

	if(deviceProp.major == 3){
		num_cores_per_sm = 192;
	}else{
		num_cores_per_sm = 32;
	}
	int num_sms = deviceProp.multiProcessorCount;

	if(total_threads < num_cores_per_sm){
		grids.x = 1; grids.y = 1; grids.z = 1;
		blocks.x = total_threads+1; blocks.y = 1; blocks.z = 1;
		if(DEBUG) printf("total_threads %d block dim (%d %d %d) thread dim (%d %d %d)\n",total_threads, grids.x,grids.y,grids.z, blocks.x,blocks.y,blocks.z  ); 
		return;
	}

	grids.x = num_sms;
	blocks.x = ceil(((float)total_threads)/grids.x);

	if(blocks.x > num_threads_per_block){
		blocks.x = num_threads_per_block;
		int block_size = blocks.x * blocks.y * blocks.z;

		grids.x = ceil(((float)total_threads)/block_size);

		if(grids.x > deviceProp.maxGridSize[0]){
			grids.y = ceil(((float)grids.x)/deviceProp.maxGridSize[0]);
			grids.x = deviceProp.maxGridSize[0];

			if(grids.y > deviceProp.maxGridSize[1]){
				grids.z = ceil(((float)grids.y)/deviceProp.maxGridSize[1]);
				grids.y = deviceProp.maxGridSize[1];

				if(grids.z > deviceProp.maxGridSize[2]){
					printf("Can't divide threads in to grids and blocks\n");
				}
			}
		}
	}

	if(DEBUG) printf("total_threads %d block dim (%d %d %d) thread dim (%d %d %d)\n",total_threads, grids.x,grids.y,grids.z, blocks.x,blocks.y,blocks.z  ); 

}

// For a given number of threads this function generates the kernel configuration 
// parameters with respect to the GPU used.
void find_dimensions_of_blocks_and_threads_custom(int total_threads, dim3 &grids, dim3 &blocks, int threads_per_block){
	// If max threads per block is 1024 and max threads per sm is 2048 and alloted=max_threads_per_block (only 1 block is scheduled per sm)
	// If it is 512 atleast 2 will be scheduled

	int DEBUG = 0;

	int num_threads_per_block = threads_per_block;
	int num_cores_per_sm = 192;

	if(deviceProp.major == 3){
		num_cores_per_sm = 192;
	}else{
		num_cores_per_sm = 32;
	}
	int num_sms = deviceProp.multiProcessorCount;

	if(total_threads < num_cores_per_sm){
		grids.x = 1; grids.y = 1; grids.z = 1;
		blocks.x = total_threads; blocks.y = 1; blocks.z = 1;
		if(DEBUG) printf("total_threads %d block dim (%d %d %d) thread dim (%d %d %d)\n",total_threads, grids.x,grids.y,grids.z, blocks.x,blocks.y,blocks.z  ); 
		return;
	}

	grids.x = num_sms;
	blocks.x = ceil(((float)total_threads)/grids.x);

	if(blocks.x > num_threads_per_block){
		blocks.x = num_threads_per_block;
		int block_size = blocks.x * blocks.y * blocks.z;

		grids.x = ceil(((float)total_threads)/block_size);

		if(grids.x > deviceProp.maxGridSize[0]){
			grids.y = ceil(((float)grids.x)/deviceProp.maxGridSize[0]);
			grids.x = deviceProp.maxGridSize[0];

			if(grids.y > deviceProp.maxGridSize[1]){
				grids.z = ceil(((float)grids.y)/deviceProp.maxGridSize[1]);
				grids.y = deviceProp.maxGridSize[1];

				if(grids.z > deviceProp.maxGridSize[2]){
					printf("Can't divide threads in to grids and blocks\n");
				}
			}
		}
	}

	if(DEBUG) printf("total_threads %d block dim (%d %d %d) thread dim (%d %d %d)\n",total_threads, grids.x,grids.y,grids.z, blocks.x,blocks.y,blocks.z  ); 

}


void allocate_host_float_array(float** h_array, int size){
	*h_array = (float*) calloc(size, sizeof(float));
}

void allocate_host_int_array(int** h_array, int size){
	*h_array = (int*) calloc(size, sizeof(int));
}

void allocate_device_float_array(float** d_array, int size){
	cudaMalloc((void**)d_array, sizeof(float) * size);
}

void allocate_device_int_array(int** d_array, int size){
	cudaMalloc((void**)d_array, sizeof(int) * size);
}

void memset_device_float_array(float* d_array, float size){
	cudaMemset(d_array,0, sizeof(float) * size);	
}

void memset_device_int_array(int* d_array, int size){
	cudaMemset(d_array,0, sizeof(int) * size);	
}


void allocate_and_memset_device_float_array(float** d_array, int size){
	cudaMalloc((void**)d_array, sizeof(float) * size);
	cudaMemset(*d_array,0, sizeof(float) * size);
}

void allocate_and_memset_device_int_array(int** d_array, int size){
	cudaMalloc((void**)d_array, sizeof(int) * size);
	cudaMemset(*d_array,0, sizeof(int) * size);
}

void allocate_and_transfer_float_array_host_to_device(float* h_array, float** d_array, int size){
	cudaMalloc((void**)d_array, sizeof(float) * size);
	cudaMemcpy(d_array, h_array, sizeof(float) * size, cudaMemcpyHostToDevice);
}

void allocate_and_transfer_int_array_host_to_device(int* h_array, int** d_array, int size){
	cudaMalloc((void**)d_array, sizeof(int) * size);
	cudaMemcpy(d_array, h_array, sizeof(int) * size, cudaMemcpyHostToDevice);
}

void allocate_and_transfer_float_array_device_to_host(float* d_array, float** h_array, int size){
	*h_array = (float*) calloc(size, sizeof(float));
	cudaMemcpy(*h_array, d_array, sizeof(float) * size, cudaMemcpyDeviceToHost);
}

void allocate_and_transfer_int_array_device_to_host(int* d_array, int** h_array, int size){
	*h_array = (int*) calloc(size, sizeof(int));
	cudaMemcpy(*h_array, d_array, sizeof(int) * size, cudaMemcpyDeviceToHost);
}


void allocate_and_transfer_float_array_device_to_device(float* d_from_array, float** d_to_array, int size){
	float* h_array = (float*) calloc(size, sizeof(float));
	cudaMalloc((void**)d_to_array, sizeof(float) * size);
	cudaMemcpy(h_array, d_from_array, sizeof(float) * size, cudaMemcpyDeviceToHost);
	cudaMemcpy(*d_to_array, h_array, sizeof(float) * size, cudaMemcpyHostToDevice);
	free(h_array);
}

void allocate_and_transfer_int_array_device_to_device(int* d_from_array, int** d_to_array, int size){
	int* h_array = (int*) calloc(size, sizeof(int));
	cudaMalloc((void**)d_to_array, sizeof(int) * size);
	cudaMemcpy(h_array, d_from_array, sizeof(int) * size, cudaMemcpyDeviceToHost);
	cudaMemcpy(*d_to_array, h_array, sizeof(int) * size, cudaMemcpyHostToDevice);
	free(h_array);
}

void transfer_float_array_host_to_device(float* h_array, float* d_array, int size){
	cudaMemcpy(d_array, h_array, sizeof(float)*size, cudaMemcpyHostToDevice);
}
void transfer_int_array_host_to_device(int* h_array, int* d_array, int size){
	cudaMemcpy(d_array, h_array, sizeof(int)*size, cudaMemcpyHostToDevice);
}

void transfer_float_array_device_to_host(float* d_array, float* h_array, int size){
	cudaMemcpy(h_array, d_array, sizeof(float)*size, cudaMemcpyDeviceToHost);
}
void transfer_int_array_device_to_host(int* d_array, int* h_array, int size){
	cudaMemcpy(h_array, d_array, sizeof(int)*size, cudaMemcpyDeviceToHost);
}

void transfer_float_array_device_to_device(float* d_from_array, float* d_to_array, int size){
	float* h_array = (float*) calloc(size, sizeof(float));
	cudaMemcpy(h_array, d_from_array, sizeof(float)*size, cudaMemcpyDeviceToHost);
	cudaMemcpy(d_to_array, h_array, sizeof(float)*size, cudaMemcpyHostToDevice);
}
void transfer_int_array_device_to_device(int* d_from_array, int* d_to_array, int size){
	int* h_array = (int*) calloc(size, sizeof(int));
	cudaMemcpy(h_array, d_from_array, sizeof(int)*size, cudaMemcpyDeviceToHost);
	cudaMemcpy(d_to_array, h_array, sizeof(int)*size, cudaMemcpyHostToDevice);
}

void print_grids_and_blocks(dim3 grids, dim3 blocks){
	printf("grids : %d %d %d\n", grids.x, grids.y, grids.z);
	printf("blocks : %d %d %d\n", blocks.x, blocks.y, blocks.z);
}


void print_device_float_array(float* array, int size){

	float* test = (float*) calloc(size, sizeof(float));
	cudaMemcpy(test, array, sizeof(float)*size, cudaMemcpyDeviceToHost);

	for (int i = 0; i < size; ++i)
		printf("%4.2f ",test[i] );
		printf("\n");

}

void print_device_int_array(int* array, int size){

	int* test = (int*) calloc(size, sizeof(int));
	cudaMemcpy(test, array, sizeof(int)*size, cudaMemcpyDeviceToHost);

	for (int i = 0; i < size; ++i)
		printf("%5d",test[i] );
		printf("\n");

}

void print_host_float_array(float* array, int size){

	for (int i = 0; i < size; ++i)
		printf("%4.2f ",array[i] );
		printf("\n");

}

void print_host_int_array(int* array, int size){
	for (int i = 0; i < size; ++i)
		printf("%6d",array[i] );
		printf("\n");
}

void print_device_coosr_matrix(coosr_matrix d_A_rowcsr){
	coosr_matrix coosr_temp;
	allocate_and_transfer_coosr_device_to_host(d_A_rowcsr, coosr_temp);
	block_matrix block_temp;
	coosr_to_block(coosr_temp, block_temp);
	print_block_matrix(block_temp);
}

void print_device_coosc_matrix(coosc_matrix d_A_coosc){
	coosc_matrix coosc_temp;
	allocate_and_transfer_coosc_device_to_host(d_A_coosc, coosc_temp);
	block_matrix block_temp;
	coosc_to_block(coosc_temp, block_temp);
	print_block_matrix(block_temp);
}

void print_device_band_matrix(band_matrix d_A_band){
	band_matrix band_temp;
	allocate_and_transfer_band_device_to_host(d_A_band, band_temp);
	block_matrix block_temp;
	band_to_block(band_temp, block_temp);
	print_block_matrix(block_temp);

}

void print_split_matrices(coosr_matrix d_A_full, coosr_matrix d_A_rowcsr, band_matrix* d_B_bands, int B_num_bands){
	
	printf("ORIGINAL MATRIX\n");
	print_device_coosr_matrix(d_A_full);

	printf("SPARSE MATRIX\n");
	print_device_coosr_matrix(d_A_rowcsr);

	for (int i = 0; i < B_num_bands; ++i)
	{	
		printf("BAND MATRIX %d\n", i+1);
		print_device_band_matrix(d_B_bands[i]);
	}

}

// Cusp matrix in COO format is converted in to COOSR format.
void generate_coosr_from_cusp_coo(cusp::coo_matrix<int, float, cusp::host_memory> cusp_mat, coosr_matrix &coosr_mat){
	coosr_mat.rows = cusp_mat.num_rows;
	coosr_mat.cols = cusp_mat.num_cols;
	coosr_mat.count = cusp_mat.num_entries;

	coosr_mat.data = (float*) calloc(coosr_mat.count, sizeof(float));
	coosr_mat.rowIndex = (int*) calloc(coosr_mat.count, sizeof(int));
	coosr_mat.colIndex = (int*) calloc(coosr_mat.count, sizeof(int));
	coosr_mat.rowPtr = (int*) calloc(coosr_mat.rows+1, sizeof(int));

	for (int i = 0; i < coosr_mat.count; ++i)
	{
		coosr_mat.data[i] = cusp_mat.values[i];
		coosr_mat.rowIndex[i] = cusp_mat.row_indices[i];
		coosr_mat.colIndex[i] = cusp_mat.column_indices[i];
		coosr_mat.rowPtr[cusp_mat.row_indices[i]+1] += 1;
	}

	// calculating rowPtr
	for (int i = 1; i < coosr_mat.rows+1; ++i){
		coosr_mat.rowPtr[i] += coosr_mat.rowPtr[i-1];
	}
}


// converts matrix in dia format to block format
void band_to_block(band_matrix band_mat, block_matrix &block_mat){
	int i,j;
	int offset;

	block_mat.rows = band_mat.rows;
	block_mat.cols = band_mat.cols;
	block_mat.data = (float*) calloc(band_mat.rows * band_mat.cols, sizeof(float));

	for(i=0;i<band_mat.count;i++){
		for(j=0;j<band_mat.rows;j++){
			offset = band_mat.offsets[i];
			if(band_mat.data[i*band_mat.rows + j] != 0){
				block_mat.data[j*band_mat.cols + (j+offset)] = band_mat.data[i*band_mat.rows + j];
			}
		}
	}
}

// converts matrix in row_csr format to block format
void coosr_to_block(coosr_matrix coosr_mat, block_matrix &block_mat){
	int i,j;
	int rowCount,rowPtr;
	int colIndex;

	block_mat.rows = coosr_mat.rows;
	block_mat.cols = coosr_mat.cols;
	block_mat.data = (float*) calloc(coosr_mat.rows * coosr_mat.cols, sizeof(float));


	// looping each row
	for(i=0;i<coosr_mat.rows;i++){
		rowCount = coosr_mat.rowPtr[i+1] - coosr_mat.rowPtr[i];
		rowPtr = coosr_mat.rowPtr[i];
		for(j=0;j<rowCount;j++){
			colIndex = coosr_mat.colIndex[rowPtr + j];
			block_mat.data[i*coosr_mat.cols + colIndex] = coosr_mat.data[rowPtr + j];
		}
	}
}

// converts matrix in col_csr format to block format
void coosc_to_block(coosc_matrix coosc_mat, block_matrix &block_mat){
	int i,j;
	int colCount,colPtr;
	int rowIndex;

	block_mat.rows = coosc_mat.rows;
	block_mat.cols = coosc_mat.cols;
	block_mat.data = (float*) calloc(coosc_mat.rows * coosc_mat.cols, sizeof(float));

	// looping each column
	for(i=0;i<coosc_mat.cols;i++){
		colCount = coosc_mat.colPtr[i+1] - coosc_mat.colPtr[i];
		colPtr = coosc_mat.colPtr[i];
		for(j=0;j<colCount;j++){
			rowIndex = coosc_mat.rowIndex[colPtr + j];
			block_mat.data[rowIndex*coosc_mat.cols + i] = coosc_mat.data[colPtr + j];
		}
	}
}

void coosr_to_coosc_in_device(cusparseHandle_t cusparse_handle, coosr_matrix d_coosr_mat, coosc_matrix &d_coosc_mat, float &time_taken){

	GpuTimer splitting_csr_to_csc_timer;

	// setting basic properties for B colcsr
	d_coosc_mat.rows = d_coosr_mat.rows;
	d_coosc_mat.cols = d_coosr_mat.cols;
	d_coosc_mat.count = d_coosr_mat.count;

	// initializing cpu data
	allocate_and_memset_device_float_array(&d_coosc_mat.data, d_coosr_mat.count);
	allocate_and_memset_device_int_array(&d_coosc_mat.rowIndex, d_coosr_mat.count);
	allocate_and_memset_device_int_array(&d_coosc_mat.colIndex, d_coosr_mat.count);
	allocate_and_memset_device_int_array(&d_coosc_mat.colPtr, d_coosr_mat.cols+1);

	splitting_csr_to_csc_timer.Start();
		// converting CSR rep of B_sparse to CSC representation
		cusparseScsr2csc(cusparse_handle, d_coosr_mat.rows, d_coosr_mat.cols, d_coosr_mat.count,
	    				 d_coosr_mat.data, d_coosr_mat.rowPtr, d_coosr_mat.colIndex,
	    				 d_coosc_mat.data, d_coosc_mat.rowIndex, d_coosc_mat.colPtr, 
	    				 CUSPARSE_ACTION_NUMERIC, CUSPARSE_INDEX_BASE_ZERO);
		cudaDeviceSynchronize();

		// computing colIndex using colPtr
		cusparseXcsr2coo(cusparse_handle, d_coosc_mat.colPtr,
	                 d_coosr_mat.count, d_coosr_mat.cols, d_coosc_mat.colIndex,
	                 CUSPARSE_INDEX_BASE_ZERO);	
		cudaDeviceSynchronize();
	splitting_csr_to_csc_timer.Stop();

	time_taken = splitting_csr_to_csc_timer.Elapsed();


}

void print_block_matrix(block_matrix mat){
	
	for (int i = 0; i < mat.rows; ++i)
	{
		for (int j = 0; j < mat.cols; ++j)
		{
			printf("%4d",(int)mat.data[i*mat.cols + j] );
		}
		printf("\n");
	}
	printf("\n");
}


// prints band matrix in diagonal format
void print_band_matrix(band_matrix mat){
	printf("MATRIX IN BAND FORMAT\n");

	// printing first r diagonals
	for(int i=0;i<mat.rows;i++){
		for(int j=0;j<mat.count;j++){
			printf("%7.2f",mat.data[j* mat.rows + i] );
		}
		printf("\n");
	}
	printf("\n");

	for(int i=0;i<mat.count;i++)
		printf("%6d ",mat.offsets[i] );
		printf("\n");

	printf("matrix band left offset is %d\n", mat.left_offset);
	printf("matrix band right offset is %d\n", mat.right_offset);

}

// prints matrix in rowcsr format
void print_coosr_matrix(coosr_matrix mat){
	printf("MATRIX IN COOSR FORMAT\n");
	
	for(int i=0;i<mat.rows;i++){
		int count = mat.rowPtr[i+1] - mat.rowPtr[i];
		
		if(count != 0)
			printf("row %4d -->",i );

		for(int j=mat.rowPtr[i];j<mat.rowPtr[i]+count;j++)
			printf("%7.2f",mat.data[j]);

		if(count != 0)
			printf("\n");
	}
	printf("rowPtr\n");
	for (int k = 0; k < mat.rows+1; ++k)
	{
		printf("%4d",mat.rowPtr[k] );
	}
	printf("\n\n");
}


// prints matrix in colcsr format
void print_coosc_matrix(coosc_matrix mat){
	printf("MATRIX IN COOSC FORMAT\n");

	for(int i=0;i<mat.cols;i++){
		int count = mat.colPtr[i+1] - mat.colPtr[i];

		if(count != 0)
			printf("col %4d -->",i );

		for(int j=mat.colPtr[i];j<mat.colPtr[i]+count;j++)
			printf("%7.2f",mat.data[j]);
		
		if(count != 0)
			printf("\n");
		
	}
	printf("colPtr\n");
	for (int k = 0; k < mat.cols+1; ++k)
	{
		printf("%4d",mat.colPtr[k] );
	}
	printf("\n\n");
}


void allocate_device_coosr_matrix(int rows, int cols, int count, coosr_matrix &coosr_mat){
	coosr_mat.rows = rows;
	coosr_mat.cols = cols;
	coosr_mat.count = count;

	allocate_device_float_array(&coosr_mat.data, count);
	allocate_device_int_array(&coosr_mat.rowIndex, count);
	allocate_device_int_array(&coosr_mat.colIndex, count);
	allocate_device_int_array(&coosr_mat.rowPtr, rows+1);

}

void allocate_device_coosc_matrix(int rows, int cols, int count, coosc_matrix &coosc_mat){
	coosc_mat.rows = rows;
	coosc_mat.cols = cols;
	coosc_mat.count = count;

	allocate_device_float_array(&coosc_mat.data, count);
	allocate_device_int_array(&coosc_mat.rowIndex, count);
	allocate_device_int_array(&coosc_mat.colIndex, count);
	allocate_device_int_array(&coosc_mat.colPtr, cols+1);

}

void allocate_and_memset_device_coosr_matrix(int rows, int cols, int count, coosr_matrix &coosr_mat){
	coosr_mat.rows = rows;
	coosr_mat.cols = cols;
	coosr_mat.count = count;

	allocate_and_memset_device_float_array(&coosr_mat.data, count);
	allocate_and_memset_device_int_array(&coosr_mat.rowIndex, count);
	allocate_and_memset_device_int_array(&coosr_mat.colIndex, count);
	allocate_and_memset_device_int_array(&coosr_mat.rowPtr, rows+1);

}

void allocate_and_memset_device_coosc_matrix(int rows, int cols, int count, coosc_matrix &coosc_mat){
	coosc_mat.rows = rows;
	coosc_mat.cols = cols;
	coosc_mat.count = count;

	allocate_and_memset_device_float_array(&coosc_mat.data, count);
	allocate_and_memset_device_int_array(&coosc_mat.rowIndex, count);
	allocate_and_memset_device_int_array(&coosc_mat.colIndex, count);
	allocate_and_memset_device_int_array(&coosc_mat.colPtr, cols+1);

}


// band in host is duplicated on to the device
void allocate_and_transfer_band_host_to_device(band_matrix h_mat, band_matrix &d_mat){

	d_mat.rows = h_mat.rows;
	d_mat.cols = h_mat.cols;
	d_mat.count = h_mat.count;
	d_mat.left_offset = h_mat.left_offset;
	d_mat.right_offset = h_mat.right_offset;

	cudaMalloc((void**)&d_mat.data, sizeof(float) * h_mat.rows * h_mat.count);
	cudaMalloc((void**)&d_mat.offsets, sizeof(int) * h_mat.count);
	
	cudaMemcpy(d_mat.data, h_mat.data, sizeof(float)* h_mat.rows * h_mat.count, cudaMemcpyHostToDevice);
	cudaMemcpy(d_mat.offsets, h_mat.offsets, sizeof(int)* h_mat.count, cudaMemcpyHostToDevice);
				
}

// coosr in host is duplicated on to the device
void allocate_and_transfer_coosr_host_to_device(coosr_matrix h_mat, coosr_matrix &d_mat){

	d_mat.rows = h_mat.rows;
	d_mat.cols = h_mat.cols;
	d_mat.count = h_mat.count;


	// rowcsr related
	cudaMalloc((void**)&d_mat.data, sizeof(float) * h_mat.count);
	cudaMalloc((void**)&d_mat.rowIndex, sizeof(int) * h_mat.count);
	cudaMalloc((void**)&d_mat.colIndex, sizeof(int) * h_mat.count);
	cudaMalloc((void**)&d_mat.rowPtr, sizeof(int) * (h_mat.rows + 1)); // we store one extra index for calculating length forumula

	cudaMemcpy(d_mat.data, h_mat.data, sizeof(float) * h_mat.count, cudaMemcpyHostToDevice);
	cudaMemcpy(d_mat.colIndex, h_mat.colIndex, sizeof(int) * h_mat.count, cudaMemcpyHostToDevice);
	cudaMemcpy(d_mat.rowIndex, h_mat.rowIndex, sizeof(int) * h_mat.count, cudaMemcpyHostToDevice);
	cudaMemcpy(d_mat.rowPtr, h_mat.rowPtr, sizeof(int) * (h_mat.rows + 1), cudaMemcpyHostToDevice);

}

// coosc in host is duplicated on to the device
void allocate_and_transfer_coosc_host_to_device(coosc_matrix h_mat, coosc_matrix &d_mat){

	d_mat.rows = h_mat.rows;
	d_mat.cols = h_mat.cols;
	d_mat.count = h_mat.count;


	// rowcsr related
	cudaMalloc((void**)&d_mat.data, sizeof(float) * h_mat.count);
	cudaMalloc((void**)&d_mat.rowIndex, sizeof(int) * h_mat.count);
	cudaMalloc((void**)&d_mat.colIndex, sizeof(int) * h_mat.count);
	cudaMalloc((void**)&d_mat.colPtr, sizeof(int) * (h_mat.cols + 1)); // we store one extra index for calculating length forumula

	cudaMemcpy(d_mat.data, h_mat.data, sizeof(float) * h_mat.count, cudaMemcpyHostToDevice);
	cudaMemcpy(d_mat.colIndex, h_mat.colIndex, sizeof(int) * h_mat.count, cudaMemcpyHostToDevice);
	cudaMemcpy(d_mat.rowIndex, h_mat.rowIndex, sizeof(int) * h_mat.count, cudaMemcpyHostToDevice);
	cudaMemcpy(d_mat.colPtr, h_mat.colPtr, sizeof(int) * (h_mat.cols + 1), cudaMemcpyHostToDevice);

}

// band on device is duplicated on to the host
void allocate_and_transfer_band_device_to_host(band_matrix d_mat, band_matrix &h_mat){
	h_mat.rows = d_mat.rows;
	h_mat.cols = d_mat.cols;
	h_mat.count = d_mat.count;
	h_mat.left_offset = d_mat.left_offset;
	h_mat.right_offset = d_mat.right_offset;

	h_mat.data = (float*) calloc(d_mat.rows*d_mat.count, sizeof(float));
	h_mat.offsets = (int*) calloc(d_mat.count, sizeof(int));

	
	cudaMemcpy(h_mat.data, d_mat.data, sizeof(float)* d_mat.rows * d_mat.count, cudaMemcpyDeviceToHost);
	cudaMemcpy(h_mat.offsets, d_mat.offsets, sizeof(int)* d_mat.count, cudaMemcpyDeviceToHost);
}

// coosr on device is duplicated on to the host
void allocate_and_transfer_coosr_device_to_host(coosr_matrix d_mat, coosr_matrix &h_mat){
	h_mat.rows = d_mat.rows;
	h_mat.cols = d_mat.cols;
	h_mat.count = d_mat.count;

	h_mat.data = (float*) calloc(d_mat.count, sizeof(float));
	h_mat.rowIndex = (int*) calloc(d_mat.count, sizeof(int));
	h_mat.colIndex = (int*) calloc(d_mat.count, sizeof(int));
	h_mat.rowPtr = (int*) calloc(d_mat.rows+1, sizeof(int));

	cudaMemcpy(h_mat.data, d_mat.data, sizeof(float) * d_mat.count, cudaMemcpyDeviceToHost);
	cudaMemcpy(h_mat.colIndex, d_mat.colIndex, sizeof(int) * d_mat.count, cudaMemcpyDeviceToHost);
	cudaMemcpy(h_mat.rowIndex, d_mat.rowIndex, sizeof(int) * d_mat.count, cudaMemcpyDeviceToHost);
	cudaMemcpy(h_mat.rowPtr, d_mat.rowPtr, sizeof(int) * (d_mat.rows + 1), cudaMemcpyDeviceToHost);

}

// coosc on device is duplicated on to the host
void allocate_and_transfer_coosc_device_to_host(coosc_matrix d_mat, coosc_matrix &h_mat){
	h_mat.rows = d_mat.rows;
	h_mat.cols = d_mat.cols;
	h_mat.count = d_mat.count;

	h_mat.data = (float*) calloc(d_mat.count, sizeof(float));
	h_mat.rowIndex = (int*) calloc(d_mat.count, sizeof(int));
	h_mat.colIndex = (int*) calloc(d_mat.count, sizeof(int));
	h_mat.colPtr = (int*) calloc(d_mat.rows+1, sizeof(int));

	cudaMemcpy(h_mat.data, d_mat.data, sizeof(float) * d_mat.count, cudaMemcpyDeviceToHost);
	cudaMemcpy(h_mat.colIndex, d_mat.colIndex, sizeof(int) * d_mat.count, cudaMemcpyDeviceToHost);
	cudaMemcpy(h_mat.rowIndex, d_mat.rowIndex, sizeof(int) * d_mat.count, cudaMemcpyDeviceToHost);
	cudaMemcpy(h_mat.colPtr, d_mat.colPtr, sizeof(int) * (d_mat.cols + 1), cudaMemcpyDeviceToHost);
}


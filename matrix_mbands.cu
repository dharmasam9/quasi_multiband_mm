// piston [UNI BAND]
num_bands = 1

band_starts[0] = -44;
band_counts[0] = 89

// sherman2 [MULTI BAND]

num_bands = 5;

band_starts[0] = -221; band_starts[1] = -41; band_starts[2] = -11; band_starts[3] = 31; band_starts[4] = 211;
band_counts[0] = 11;  band_counts[1] = 11; band_counts[2] = 23; band_counts[3] = 11; band_counts[4] = 11;

// fv1 [MULTI BAND]
num_bands = 3;
band_starts[0] = -99; band_starts[1] = -1; band_starts[2] = 97;
band_counts[0] = 3;  band_counts[1] = 3; band_counts[2] = 3; 

// fv2 [MULTI BAND]
num_bands = 3;
band_starts[0] = -100; band_starts[1] = -1; band_starts[2] = 98;
band_counts[0] = 3;  band_counts[1] = 3; band_counts[2] = 3; 

// pcrystk03.mtx [MULTI BAND]

num_bands = 9
band_starts[0] = -572; band_starts[1] = -509; band_starts[2] = -446; band_starts[3] = -68; 
band_starts[4] = -5; 
band_starts[8] = 562;   band_starts[7] = 499; band_starts[6] = 436; band_starts[5] = 58;

band_counts[0] = 11; band_counts[1] = 11; band_counts[2] = 11; band_counts[3] = 11; 
band_counts[4] = 11; 
band_counts[8] = 11;   band_counts[7] = 11; band_counts[6] = 11; band_counts[5] = 11;

// denormal [MULTI BAND]
num_bands = 5
band_starts[0] = -596; band_starts[1] = -299; band_starts[2] = -2; band_starts[3] = 297; band_starts[4] = 596;
band_counts[0] = 1; band_counts[1] = 3; band_counts[2] = 5; band_counts[3] = 3; band_counts[4] = 1;

// Lin [MULTI BAND]
num_bands = 5;
band_starts[0] = -6400; band_starts[1] = -80; band_starts[2] = -1; band_starts[3] = 80; band_starts[4] = 6400;
band_counts[0] = 1; band_counts[1] = 1; band_counts[2] = 3; band_counts[3] = 1; band_counts[4] = 1;

// matrix_9 [QUASI MULTI BAND]
num_bands = 5;

band_starts[0] = -3980; band_starts[1] = -155; band_starts[2] = -5; band_starts[3] = 151; band_starts[4] = 3976;
band_counts[0] = 5; band_counts[1] = 5; band_counts[2] = 11; band_counts[3] = 5; band_counts[4] = 5;


// raefsky.mtx [QUASI MULTI BAND]
num_bands = 3

band_starts[0] = -727; band_starts[1] = -15; band_starts[2] = 697;
band_counts[0] = 31;  band_counts[1] = 31; band_counts[2] = 31; 

// PR02R [QUASI MULTI BAND]
// 237ms 468ms
num_bands = 5;

band_starts[0] = -2482; band_starts[1] = -1252; band_starts[2] = -18; band_starts[3] = 1226; band_starts[4] = 2474;
band_counts[0] = 9;  band_counts[1] = 26; band_counts[2] = 37; band_counts[3] = 26; band_counts[4] = 9;


// Kuu [QUASI MULTI BAND] [NO SPEED UP]
num_bands = 3;
band_starts[0] = -139; band_starts[1] = -5; band_starts[2] = 130;
band_counts[0] = 10;  band_counts[1] = 11; band_counts[2] = 10;
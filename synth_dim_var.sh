
# Common Properties
DEVICE=0
USE_OVERLAP=0
FOR_ANALYSIS=1
VERIFY=0
DEBUG=0


# Synthetic matrix properites
nnz=2048000
rows=3000
band_perc=90
band_occupancy=90
# 0=random 1=row_block 2=col_block
SPARSE_FILL_METHOD=0

num_data_points=12
strip_size=1000


result_dir="./results/dim_var-"
hyphen="-"
result=$result_dir$nnz$hyphen$band_perc$hyphen$band_occupancy


if [ -f "$result" ]
then
	rm $result
else
	touch $result
fi


for ((  j = 0 ;  j < num_data_points;  j++  ))
	do
		
		den=`expr $rows \* $rows`
		nnz_perc=$(echo "scale=2; ($nnz*100)/$den" | bc)

		./quasi "0" "$rows" "$nnz_perc" "$band_perc" "$band_occupancy" "$SPARSE_FILL_METHOD" "$DEVICE" "$FOR_ANALYSIS"  "$USE_OVERLAP" "$VERIFY" "$DEBUG" >> "$result"
		rows=`expr $rows + $strip_size`
		
	done
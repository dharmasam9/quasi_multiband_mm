#!/bin/bash
dirr="../testmatrices"
result="./results/probable_matrices"

nvcc -w -I ./ -arch="sm_35" -lcusparse find_prop.cu -o findprop


# getting file values if f
files=""
matrix_count=0
sample_count=${#BAND_OCCUPANCY[@]}


if [ -d $dirr ]; then
    for f in "$dirr"/*; 
    	do
	    	if [ -f $f ]; then
		        files="$files $f"
		        matrix_count=$(($matrix_count + 1))
			fi
		done
else
    echo $dirr " is Not a directory"
fi

matrices=($files)
# running the alog with different dia fill percentages
for ((  i = 0 ;  i < matrix_count;  i++  ))
do
	
	./findprop "${matrices[i]}" >> "$result"
	
done

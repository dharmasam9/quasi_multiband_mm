#include "common.cu"
#include <thrust/device_vector.h>

using namespace cusp;

int main(int argc, char *argv[])
{

	float split_A_matrix_time;

	char* fn1 = argv[1];
	
	//float BAND_OCCUPANCY = atof(argv[2]);
	//int USE_MAIN_ONLY = atoi(argv[3]);

	float BAND_OCCUPANCY = 60;
	int USE_MAIN_ONLY = 0;

	cusp::coo_matrix<int, float, cusp::host_memory> cusp_A;

	// reading matrix in cusp format
	cusp::io::read_matrix_market_file(cusp_A, fn1);

	// cusparse handle
    cusparseHandle_t cusparse_handle = 0;
    cusparseCreate(&cusparse_handle);

    // Setting up device and initializing device properties.
    int dev = 0;
    cudaSetDevice(dev);
    cudaGetDeviceProperties(&deviceProp, dev);

    // matrices
    coosr_matrix h_A_full, d_A_full, d_A_dia_coosr;
	
	generate_coosr_from_cusp_coo(cusp_A , h_A_full );
	allocate_and_transfer_coosr_host_to_device(h_A_full , d_A_full);

	cudaDeviceSynchronize();

	float* d_A_dia_data;
	int* d_A_dia_offsets, *d_A_dia_offset_indices;
	
	int A_num_dias, A_num_bands;
	int A_nz_dias;
	band_matrix *h_A_bands, *d_A_bands;
	coosr_matrix d_A_rowcsr;

	split_matrix_in_gpu(cusparse_handle, d_A_full, d_A_dia_data, d_A_dia_offsets, d_A_dia_offset_indices, A_num_dias,
					d_A_bands, A_num_bands, d_A_rowcsr, d_A_dia_coosr, A_nz_dias, split_A_matrix_time, BAND_OCCUPANCY, USE_MAIN_ONLY);
 					
	

	cudaDeviceSynchronize();

	// Calculating matrix properties
	int sparse_nnz = d_A_rowcsr.count;
	int band_nnz = d_A_full.count - d_A_rowcsr.count;
	float nnz_perc = ((float)d_A_full.count/(d_A_full.rows*d_A_full.cols))*100;
	float band_perc = (float)band_nnz*100/d_A_full.count;
	float sparse_perc = (float)sparse_nnz*100/d_A_full.count;

	// Finding percentage of diagonals, band_occupancy.		
	int num_diagonals = 0;
	for (int i = 0; i < A_num_bands; ++i)
	{
		int left = d_A_bands[i].left_offset;
		int count = d_A_bands[i].count;
		num_diagonals += count;
	}

	
	float dia_perc = (float)num_diagonals*100/ (d_A_full.rows + d_A_full.cols - 1);
	float calc_band_occupancy = (float)band_nnz*100/(d_A_full.rows * num_diagonals);


	char* mat_name;
	get_name(fn1, mat_name);
	if(band_perc > 50){
		printf("%s,%d,%d,%d,%.2f,%d,%.2f,%d,%.2f,%.2f,%.2f,%d,%d,%.2f,%.2f\n", 
			mat_name, d_A_rowcsr.rows, d_A_rowcsr.cols, d_A_full.count, nnz_perc, band_nnz, band_perc, sparse_nnz, sparse_perc, BAND_OCCUPANCY, calc_band_occupancy , A_num_bands, num_diagonals, dia_perc, split_A_matrix_time);
	}
	
	

}
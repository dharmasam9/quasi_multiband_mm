#include "common.cu"
#include  <thrust/sort.h>
#include <thrust/merge.h>
#include <thrust/functional.h>
#include <thrust/execution_policy.h>

#include <iostream>
using namespace std;

int main(int argc, char const *argv[])
{
	// set device
	int dev = 0;
	cudaSetDevice(dev);
    cudaGetDeviceProperties(&deviceProp, dev);

	dim3 grids,blocks;
	GpuTimer sorting_timer;

	srand(time(0) + 1);

	int size = 10;
	int a[size];

	int band_occupancy = 5;

	for (int i = 0; i < size; ++i)
	{
		a[i] = rand()%10;
	}

	std::vector<pair<int,int> > v;



	for (int i = 0; i < size; ++i)
	{
		if(a[i] > band_occupancy){
			pair<int,int> temp(a[i],i);
			v.push_back(temp);
		}
	}

	for (int i = 0; i < v.size(); ++i)
	{
		cout << v[i].first << " " << v[i].second << endl;
	}

	sort(v.rbegin(),v.end());

	for (int i = 0; i < v.size(); ++i)
	{
		cout << v[i].first << " " << v[i].second << endl;
	}


	return 0;
}
#ifndef CPU_TIMER_H__
#define CPU_TIMER_H__

#include <sys/time.h>
#include <cuda_runtime.h>

struct CpuTimer
{
  double start;
  double stop;

  CpuTimer()
  {
    start = 0;
    stop = 0;
  }

  ~CpuTimer()
  {
   
  }

  double timer() {
      struct timeval tp;
      gettimeofday(&tp, NULL);
      return ((double) (tp.tv_sec) + 1e-6 * tp.tv_usec);
  }


  void Start()
  {
    start = timer();
  }

  void Stop()
  {
    cudaDeviceSynchronize();
    stop = timer();
  }

  float Elapsed()
  {
    double elapsed;
    elapsed = (stop-start)*1000;
    return (float)elapsed;
  }
};

#endif  /* CPU_TIMER_H__ */
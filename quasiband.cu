#include "common.cu"
#include <thrust/device_vector.h>

using namespace cusp;

int main(int argc, char *argv[])
{
	float BAND_OCCUPANCY = 50;

	// To verify gpu output with cpu output
	int FROM_FILE = 1;
	int VERIFY = 0;
	int DEBUG = 0;
	int FOR_ANALYSIS = 0;
	int USE_OVERLAP_OPTI = 0;
	int USE_MAIN_ONLY = 0;

	
	dim3 grids,blocks;

	// cusparse handle
    cusparseHandle_t cusparse_handle = 0;
    cusparseCreate(&cusparse_handle);

    cusp::coo_matrix<int, float, cusp::host_memory> cusp_A, cusp_B;

    // Bench mark matrices
    coosr_matrix d_cusparse_dia_dia_output;
	coosr_matrix d_cusparse_sparse_dia_output;
	coosr_matrix d_cusparse_dia_sparse_output;
	coosr_matrix d_cusparse_sparse_sparse_output; 
	coosr_matrix d_cusparse_full_sparse_output;

	int sparse_band_output_count = 0;
	int band_sparse_output_count = 0;

	// timings
	float cusparse_preprocessing_time = 0, cusparse_processing_time = 0;
	float cusparse_dia_dia_preprocessing_time = 0, cusparse_dia_dia_processing_time = 0;
	float cusparse_sparse_dia_preprocessing_time = 0, cusparse_sparse_dia_processing_time = 0;
	float cusparse_dia_sparse_preprocessing_time = 0, cusparse_dia_sparse_processing_time = 0;
	float cusparse_full_sparse_preprocessing_time = 0, cusparse_full_sparse_processing_time = 0;

	float split_A_matrix_time = 0, split_B_matrix_time = 0;
	float split_B_coosr2coosc_unsorted_time = 0;
	GpuTimer split_B_coosr2coosc_timer; float split_B_coosr2coosc_time;


	GpuTimer dia_dia_preprocessing_timer;
	GpuTimer dia_dia_processing_timer;
	float dia_dia_preprocessing_time = 0, dia_dia_processing_time = 0;


	GpuTimer sparse_band_preprocessing_timer, sparse_band_processing_timer;	
	float sparse_band_preprocessing_time = 0, sparse_band_processing_time = 0;
	float sparse_band_merge_time = 0; float sparse_band_diadia_overlap_time = 0;


	GpuTimer band_sparse_preprocessing_timer, band_sparse_processing_timer;
	float band_sparse_preprocessing_time = 0, band_sparse_processing_time = 0;
	float band_sparse_merge_time = 0; float band_sparse_diadia_overlap_time = 0;
	float band_sparse_output_coosc2coosr_unsorted_time = 0;

	float sparse_sparse_preprocessing_time = 0, sparse_sparse_processing_time = 0;
	float sparse_sparse_diadia_overlap_time = 0;


	float sparse_band_diadia_overlap_perc = 0;
	float band_sparse_diadia_overlap_perc = 0;
	float sparse_sparse_diadia_overlap_perc = 0;

	float output_3piece_consolidation_time = 0;
	float ouptut_2piece_consolidation_time = 0;

	FROM_FILE = atoi(argv[1]);

	if(FROM_FILE){
		char* fn1 = argv[2];
		char* fn2 = argv[2];

		BAND_OCCUPANCY = atof(argv[3]);

		USE_MAIN_ONLY = atoi(argv[4]);

		cusp::io::read_matrix_market_file(cusp_A, fn1);
		cusp::io::read_matrix_market_file(cusp_B, fn2);
		
		int dev = 0; // default first device
		if(argc > 5)
			dev = atoi(argv[5]);
		cudaSetDevice(dev);
        cudaGetDeviceProperties(&deviceProp, dev);
        
		if(argc > 6)
			FOR_ANALYSIS = atoi(argv[6]);

		if(!FOR_ANALYSIS)
			printf("\nDevice %d: \"%s\"\n", dev, deviceProp.name);

		if(argc > 7)
			USE_OVERLAP_OPTI = atoi(argv[7]);

		if(argc > 8)
			VERIFY = atoi(argv[8]);

		if(argc > 9)
			DEBUG = atoi(argv[9]);

	}else{
		int experiment = 1;

		int size = atoi(argv[2]);
		float nnz_perc = atof(argv[3]);
		float band_perc = atof(argv[4]);
		BAND_OCCUPANCY = atof(argv[5]);

		if(argc > 6)
			experiment = atof(argv[6]);

		int dev = 0; // default first device
		if(argc > 7)
			dev = atoi(argv[7]);
		cudaSetDevice(dev);
        cudaGetDeviceProperties(&deviceProp, dev);

        if(argc > 8)
			FOR_ANALYSIS = atoi(argv[8]);

		if(!FOR_ANALYSIS)
			printf("\nDevice %d: \"%s\"\n", dev, deviceProp.name);

        if(argc > 9)
			USE_OVERLAP_OPTI = atoi(argv[9]);
		
		if(argc > 10)
			VERIFY = atoi(argv[10]);

		if(argc > 11)
			DEBUG = atoi(argv[11]);

		fill_synthetic_matrix(cusp_A, size, size, nnz_perc, band_perc, BAND_OCCUPANCY, experiment, 1);
		fill_synthetic_matrix(cusp_B, size, size, nnz_perc, band_perc, BAND_OCCUPANCY, experiment, 2);
	}
	

	coosr_matrix h_A_full, h_B_full, h_C_full;
	coosr_matrix d_A_full, d_B_full, d_C_full;
	coosr_matrix d_A_dia_coosr, d_B_dia_coosr;

	generate_coosr_from_cusp_coo(cusp_A , h_A_full );
	generate_coosr_from_cusp_coo(cusp_B , h_B_full );

	allocate_and_transfer_coosr_host_to_device(h_A_full , d_A_full);
	allocate_and_transfer_coosr_host_to_device(h_B_full , d_B_full);

	cudaDeviceSynchronize();

	
	// CUSPARSE COMPUTATION FOR REFERENCE AND BENCH MARKING	
	perform_cusparse_coosr_coosr(cusparse_handle , d_A_full, d_B_full, d_C_full, cusparse_preprocessing_time, cusparse_processing_time);

	if(!FOR_ANALYSIS)
		printf("pre %f proc %f total %f\n", cusparse_preprocessing_time, cusparse_processing_time, cusparse_preprocessing_time+cusparse_processing_time);;

	//return;
	if(DEBUG) printf("DONE BENCH-MARK COMPUTATION\n");

	float* d_A_dia_data;
	int* d_A_dia_offsets, *d_A_dia_offset_indices;
	float* d_B_dia_data;
	int* d_B_dia_offsets, *d_B_dia_offset_indices;

	int A_num_dias , B_num_dias;
	int A_num_bands  , B_num_bands;
	int A_nz_dias, B_nz_dias; // nnz diagonal counts


	band_matrix *h_A_bands, *d_A_bands;
	band_matrix *h_B_bands, *d_B_bands;

	coosr_matrix d_A_rowcsr;
	coosr_matrix d_B_rowcsr;
	coosc_matrix d_B_colcsr;	

	split_matrix_in_gpu(cusparse_handle, d_A_full, d_A_dia_data, d_A_dia_offsets, d_A_dia_offset_indices, A_num_dias,
					d_A_bands, A_num_bands, d_A_rowcsr, d_A_dia_coosr, A_nz_dias, split_A_matrix_time, BAND_OCCUPANCY, USE_MAIN_ONLY);
	//return;
 					
	split_matrix_in_gpu(cusparse_handle, d_B_full, d_B_dia_data, d_B_dia_offsets, d_B_dia_offset_indices, B_num_dias,
					d_B_bands, B_num_bands, d_B_rowcsr, d_B_dia_coosr, B_nz_dias, split_B_matrix_time, BAND_OCCUPANCY, USE_MAIN_ONLY);

	if(DEBUG) printf("DONE SPLITTING\n");

	get_coosr_to_coosc_unsorted_conv_time(cusparse_handle, d_B_rowcsr, split_B_coosr2coosc_unsorted_time);

	/*
	// Generates a sparse matrix
	int* rowPtr = (int*) calloc(d_A_rowcsr.rows+1, sizeof(int));
	cudaMemcpy(rowPtr, d_A_rowcsr.rowPtr, (d_A_rowcsr.rows+1)* sizeof(int), cudaMemcpyDeviceToHost);

	int* colIndex = (int*) calloc(rowPtr[d_A_rowcsr.rows], sizeof(int));
	cudaMemcpy(colIndex, d_A_rowcsr.colIndex, (rowPtr[d_A_rowcsr.rows])* sizeof(int), cudaMemcpyDeviceToHost);
	
	char* name;
	get_name(argv[2], name);

	FILE* fp = fopen(name,"w+");
	int count = 0;
	fprintf(fp,"%%%%MatrixMarket matrix coordinate real general\n");
	fprintf(fp, "%d %d %d\n",d_A_rowcsr.rows,d_A_rowcsr.rows, rowPtr[d_A_rowcsr.rows]);
	for (int i = 0; i < d_A_rowcsr.rows; ++i)
	{
		int ptr = rowPtr[i];
		int length = (rowPtr[i+1] - rowPtr[i]);

		if(length > 0){
			count++;
		}

		for (int j = 0; j < length; ++j)
		{
			fprintf(fp,"%d %d 1\n", i+1,colIndex[ptr+j]+1);
		}
	}
	fprintf(fp,"rows %d", count );

	fclose(fp);
	
	return;
	*/

	//coosr_to_coosc_in_device(cusparse_handle, d_B_rowcsr , d_B_colcsr, split_B_coosr_to_coosc_conv_time);
	split_B_coosr2coosc_timer.Start();
		allocate_and_memset_device_coosc_matrix(d_B_rowcsr.rows, d_B_rowcsr.cols, d_B_rowcsr.count, d_B_colcsr);

		// converting CSR rep of B_sparse to CSC representation
		cusparseScsr2csc(cusparse_handle, d_B_rowcsr.rows, d_B_rowcsr.cols, d_B_rowcsr.count,
	    				 d_B_rowcsr.data, d_B_rowcsr.rowPtr, d_B_rowcsr.colIndex,
	    				 d_B_colcsr.data, d_B_colcsr.rowIndex, d_B_colcsr.colPtr, 
	    				 CUSPARSE_ACTION_NUMERIC, CUSPARSE_INDEX_BASE_ZERO);

		// computing colIndex using colPtr
		cusparseXcsr2coo(cusparse_handle, d_B_colcsr.colPtr,
	                 d_B_colcsr.count, d_B_colcsr.cols, d_B_colcsr.colIndex,
	                 CUSPARSE_INDEX_BASE_ZERO);	
		cudaDeviceSynchronize();
	split_B_coosr2coosc_timer.Stop();

	if(DEBUG) printf("DONE B CSR2CSC\n");

	//print_split_matrices(d_A_full, d_A_rowcsr, d_B_bands, B_num_bands);
	cudaDeviceSynchronize();

	// Calculating matrix properties
	int sparse_nnz = d_A_rowcsr.count;
	int band_nnz = d_A_full.count - d_A_rowcsr.count;
	float nnz_perc = ((float)d_A_full.count/(d_A_full.rows*d_A_full.cols))*100;
	float band_perc = (float)band_nnz*100/d_A_full.count;
	float sparse_perc = (float)sparse_nnz*100/d_A_full.count;
	
	
	float dia_perc = (float)A_num_dias*100/ (d_A_full.rows + d_A_full.cols - 1);
	float calc_band_occupancy = (float)band_nnz*100/(d_A_full.rows * A_num_dias);

	float band_in_nzdia = ((float)A_num_dias*100)/A_nz_dias;

	if(!FOR_ANALYSIS){

		printf("---- MATRIX PROPERTIES ----\n");

		printf("rows %d cols %d nnz %d nnz_perc %.3f band_occupancy %.2f band_nnz %d sparse_nnz %d band_perc %.2f sparse_perc %.2f num_bands %d A_num_dias %d dia_perc %.2f num_nz_dias %d %%of_band_in_nnzdia %.2f \n",
			d_A_full.rows, d_A_full.cols, d_A_full.count, nnz_perc, calc_band_occupancy, band_nnz, sparse_nnz, band_perc, sparse_perc, A_num_bands, A_num_dias, dia_perc , A_nz_dias, band_in_nzdia);

		for (int i = 0; i < A_num_bands; ++i)
		{
			printf("BAND %d: offset %d count %d\n", i+1, d_A_bands[i].left_offset, d_A_bands[i].count);
		}
		
	}else{
		char* mat_name;
		get_name(argv[2], mat_name);
		
		printf("# %s,",mat_name);
		printf("%d,%d,%d, %.2f,%.2f,%.2f,",
			d_A_full.rows, d_A_full.cols, d_A_full.count , ((float)d_A_full.count/(d_A_full.rows*d_A_full.cols))*100,band_perc, sparse_perc);
		printf("%.2f,%.2f,%d,%d,%.2f,%d,%d,%d,%.2f,", 
			BAND_OCCUPANCY, calc_band_occupancy, band_nnz, sparse_nnz, dia_perc, A_num_bands, A_num_dias, A_nz_dias, band_in_nzdia);

		for (int i = 0; i < A_num_bands; ++i)
			printf("(%d|%d)", d_A_bands[i].left_offset, d_A_bands[i].count);		
		printf("\n");
		
	}


	// *******************
	// DIA DIA MODULE
	// *******************

	float* d_dia_dia_output_data;
	int* d_dia_dia_output_offsets, *d_dia_dia_output_offset_indices;
	int h_dia_dia_output_offset_count;
	
	//GpuTimer dia_dia_preprocessing_timer;
	dia_dia_preprocessing_timer.Start();
		dia_dia_preprocessing(d_A_full.rows, d_B_full.cols, d_A_dia_offsets, A_num_dias, d_B_dia_offsets,  B_num_dias,  h_dia_dia_output_offset_count, d_dia_dia_output_offsets, d_dia_dia_output_data, d_dia_dia_output_offset_indices);
		cudaDeviceSynchronize();
	dia_dia_preprocessing_timer.Stop();

	if(DEBUG) printf("DONE DIA-DIA PRE-PROCESSING\n");


	int num_row_blocks = 1;
	int num_threads_per_row_block = d_A_full.rows;
	if(d_A_full.rows > 1024){
		num_threads_per_row_block = 512;
		num_row_blocks = ceil((float)d_A_full.rows/512);
	}
	
	dia_dia_processing_timer.Start();
		dia_dia_multiplication_kernel<<<dim3(A_num_dias , B_num_dias, num_row_blocks),num_threads_per_row_block>>>(
										d_A_dia_data, d_A_dia_offsets, d_A_full.rows, d_A_full.cols,
										d_B_dia_data, d_B_dia_offsets, d_B_full.rows, d_B_full.cols,
										d_dia_dia_output_offset_indices, d_dia_dia_output_data);
		cudaDeviceSynchronize();
	dia_dia_processing_timer.Stop();


	if(DEBUG) printf("DONE DIA-DIA PROCESSING\n");

	if(VERIFY)
	match_dia_dia_output(d_A_full.rows, d_A_full.cols, d_B_full.cols,
						   d_A_dia_data, d_A_dia_offsets, A_num_dias,
						   d_B_dia_data, d_B_dia_offsets, B_num_dias,
						   d_dia_dia_output_data, d_dia_dia_output_offsets, h_dia_dia_output_offset_count);

	if(d_A_rowcsr.count != 0){

	// *******************
	// SPARSE BAND MODULE
	// *******************
	// sparse band pre-processing related pointers [DEVICE]
	int** d_A_blockCount, **d_A_base_rowIndex, **d_A_base_colIndex;
	int** d_A_block_start_positions_rowPtr; // needed for optimized sparse band processing kernel

	coosr_matrix* d_sparse_band_outputs;
	coosr_matrix d_sparse_mband_output;


	// Double pointer array to hold single pointers of every band.
	d_A_blockCount = (int**) malloc(B_num_bands * sizeof(int*));
	d_A_base_rowIndex = (int**) malloc(B_num_bands * sizeof(int*));
	d_A_base_colIndex = (int**) malloc(B_num_bands * sizeof(int*));
	d_A_block_start_positions_rowPtr = (int**) malloc(B_num_bands * sizeof(int*));
	d_sparse_band_outputs = (coosr_matrix*) malloc(B_num_bands * sizeof(coosr_matrix));

	sparse_band_preprocessing_timer.Start();

		for (int i = 0; i < B_num_bands; ++i)
		{
			d_sparse_band_outputs[i].rows = d_A_rowcsr.rows;
			d_sparse_band_outputs[i].cols = d_B_bands[i].cols;

			sparse_band_preprocessing(d_A_rowcsr, d_B_bands[i],
									d_A_full.rows, d_A_full.cols, d_B_full.cols,
									d_sparse_band_outputs[i].rowPtr, d_sparse_band_outputs[i].rowIndex, d_sparse_band_outputs[i].colIndex, d_sparse_band_outputs[i].count,
									d_A_blockCount[i], d_A_base_rowIndex[i], d_A_base_colIndex[i], d_A_block_start_positions_rowPtr[i]);

			
			// allocating data for sparse band output
			allocate_and_memset_device_float_array(&d_sparse_band_outputs[i].data, d_sparse_band_outputs[i].count);
		}
		cudaDeviceSynchronize();

	sparse_band_preprocessing_timer.Stop();


	if(DEBUG) printf("DONE SPARSE-DIA PRE-PROCESSING\n");
	
	sparse_band_processing_timer.Start();
		for (int i = 0; i < B_num_bands; ++i)
		{
			//find_dimensions_of_blocks_and_threads(d_B_bands[i].count * d_A_rowcsr.count, grids, blocks);
			find_dimensions_of_blocks_and_threads_custom(d_B_bands[i].count * d_A_rowcsr.count, grids, blocks, 192);
			rowcsr_band_matrix_multiplication_optimized_kernel<<<grids, blocks>>>(d_A_rowcsr.data, d_A_rowcsr.colIndex, d_A_rowcsr.rowIndex, d_A_rowcsr.rowPtr,
													d_A_blockCount[i], d_A_block_start_positions_rowPtr[i], d_A_base_colIndex[i],
													d_A_rowcsr.rows, d_A_rowcsr.cols,
													d_B_bands[i].data, d_B_bands[i].offsets,
													d_B_bands[i].rows, d_B_bands[i].cols,
													d_A_rowcsr.count, d_B_bands[i].count,
													d_sparse_band_outputs[i].data, d_sparse_band_outputs[i].colIndex, d_sparse_band_outputs[i].rowPtr);
		}
		cudaDeviceSynchronize();

	sparse_band_processing_timer.Stop();

	if(DEBUG) printf("DONE SPARSE-DIA PROCESSING\n");

	// Compress all sparse band output blocks to one block
	merge_coosr_matrices(d_sparse_band_outputs, B_num_bands, d_sparse_mband_output, sparse_band_merge_time);
	sparse_band_output_count = d_sparse_mband_output.count;
	cudaDeviceSynchronize();


	if(DEBUG) printf("DONE SPARSE-DIA MERGING\n");

	if(VERIFY)
		match_coosr_dia_output(d_A_rowcsr, d_B_bands, B_num_bands, d_sparse_mband_output);
	cudaDeviceSynchronize();

	// Removing the overlap part of band sparse output with dia dia output
	if(USE_OVERLAP_OPTI){
		move_overlap_of_csr_in_dia(d_sparse_mband_output, d_dia_dia_output_data, d_dia_dia_output_offset_indices, sparse_band_diadia_overlap_time, sparse_band_diadia_overlap_perc);
		//printf("OVERLAP SPM DIA PERC %.2f TIME %.2f\n", sparse_band_diadia_overlap_perc, sparse_band_diadia_overlap_time);
	}

	// *********************
	// SPARSE SPARSE MODULE
	// *********************

	// processing SPARSE X SPARSE
	perform_cusparse_coosr_coosr(cusparse_handle , d_A_rowcsr, d_B_rowcsr, d_cusparse_sparse_sparse_output, sparse_sparse_preprocessing_time, sparse_sparse_processing_time);
	cudaDeviceSynchronize();

	if(DEBUG) printf("DONE SPARSE-SPARSE COMPUTATION\n");
    
	if(VERIFY)
		match_sparse_sparse_output(d_A_rowcsr, d_B_colcsr, d_cusparse_sparse_sparse_output);

	if(USE_OVERLAP_OPTI){
		move_overlap_of_csr_in_dia(d_cusparse_sparse_sparse_output, d_dia_dia_output_data, d_dia_dia_output_offset_indices, sparse_sparse_diadia_overlap_time, sparse_sparse_diadia_overlap_perc);
		//printf("OVERLAP SPSP DIA PERC %.2f TIME %.2f\n", sparse_sparse_diadia_overlap_perc, sparse_sparse_diadia_overlap_time);
	}


	// *******************
	// BAND SPARSE MODULE
	// *******************

	// Transpose the bands
	int temp;
	for (int i = 0; i < A_num_bands; ++i)
	{	
		transpose_band_matrix(d_A_bands[i]);
	}
	

	// Use coosc (d_B_colcsr) as coosr so that you 
	coosr_matrix d_B_trans_coosr;
	d_B_trans_coosr.rows = d_B_colcsr.cols;
	d_B_trans_coosr.cols = d_B_colcsr.rows;
	d_B_trans_coosr.count = d_B_colcsr.count;
	d_B_trans_coosr.data = d_B_colcsr.data;
	d_B_trans_coosr.rowPtr = d_B_colcsr.colPtr;
	d_B_trans_coosr.colIndex = d_B_colcsr.rowIndex;
	d_B_trans_coosr.rowIndex = d_B_colcsr.colIndex;


	// band sparse pre-processing related pointers [DEVICE]
	int** d_B_blockCount, **d_B_base_rowIndex, **d_B_base_colIndex;
	int** d_B_block_start_positions_rowPtr; // needed for optimized sparse band processing kernel

	coosr_matrix* d_band_sparse_outputs;
	coosr_matrix d_mband_sparse_output_trans;

	d_B_blockCount = (int**) malloc(A_num_bands * sizeof(int*));
	d_B_base_rowIndex = (int**) malloc(A_num_bands * sizeof(int*));
	d_B_base_colIndex = (int**) malloc(A_num_bands * sizeof(int*));
	d_B_block_start_positions_rowPtr = (int**) malloc(A_num_bands * sizeof(int*));

	d_band_sparse_outputs = (coosr_matrix*) malloc(A_num_bands * sizeof(coosr_matrix));

	band_sparse_preprocessing_timer.Start();

		for (int i = 0; i < A_num_bands; ++i)
		{
			d_band_sparse_outputs[i].rows = d_B_trans_coosr.rows;
			d_band_sparse_outputs[i].cols = d_A_bands[i].cols;

			sparse_band_preprocessing(d_B_trans_coosr, d_A_bands[i],
									d_A_full.rows, d_A_full.cols, d_B_full.cols,
									d_band_sparse_outputs[i].rowPtr, d_band_sparse_outputs[i].rowIndex, d_band_sparse_outputs[i].colIndex, d_band_sparse_outputs[i].count,
									d_B_blockCount[i], d_B_base_rowIndex[i], d_B_base_colIndex[i], d_B_block_start_positions_rowPtr[i]);

			
			// allocating data for sparse band output
			allocate_and_memset_device_float_array(&d_band_sparse_outputs[i].data, d_band_sparse_outputs[i].count);
		}
		cudaDeviceSynchronize();

	band_sparse_preprocessing_timer.Stop();
	if(DEBUG) printf("DONE DIA-SPARSE PRE-PROCESSING\n");

	band_sparse_processing_timer.Start();
		for (int i = 0; i < A_num_bands; ++i)
		{
			//find_dimensions_of_blocks_and_threads(d_A_bands[i].count * d_B_trans_coosr.count, grids, blocks);
			find_dimensions_of_blocks_and_threads_custom(d_B_bands[i].count * d_A_rowcsr.count, grids, blocks, 192);
			rowcsr_band_matrix_multiplication_optimized_kernel<<<grids, blocks>>>(d_B_trans_coosr.data, d_B_trans_coosr.colIndex, d_B_trans_coosr.rowIndex, d_B_trans_coosr.rowPtr,
													d_B_blockCount[i], d_B_block_start_positions_rowPtr[i], d_B_base_colIndex[i],
													d_B_trans_coosr.rows, d_B_trans_coosr.cols,
													d_A_bands[i].data, d_A_bands[i].offsets,
													d_A_bands[i].rows, d_A_bands[i].cols,
													d_B_trans_coosr.count, d_A_bands[i].count,
													d_band_sparse_outputs[i].data, d_band_sparse_outputs[i].colIndex, d_band_sparse_outputs[i].rowPtr);
			check_cuda_errors(__FILE__, __LINE__);
		}
		cudaDeviceSynchronize();

	band_sparse_processing_timer.Stop();
	if(DEBUG) printf("DONE DIA-SPARSE PROCESSING\n");
	

	// Compress all sparse band output blocks to one block
	merge_coosr_matrices(d_band_sparse_outputs, A_num_bands, d_mband_sparse_output_trans, band_sparse_merge_time);
	band_sparse_output_count = d_mband_sparse_output_trans.count;
	cudaDeviceSynchronize();

	if(DEBUG) printf("DONE DIA-SPARSE MERGING\n");

	if(VERIFY)
		match_coosr_dia_output(d_B_trans_coosr, d_A_bands, A_num_bands, d_mband_sparse_output_trans);

	// Convert output coosr to coosc
	coosc_matrix d_mband_sparse_output_coosc;

	d_mband_sparse_output_coosc.rows = d_mband_sparse_output_trans.cols;
	d_mband_sparse_output_coosc.cols = d_mband_sparse_output_trans.rows;
	d_mband_sparse_output_coosc.count = d_mband_sparse_output_trans.count;
	d_mband_sparse_output_coosc.data = d_mband_sparse_output_trans.data;
	d_mband_sparse_output_coosc.rowIndex = d_mband_sparse_output_trans.colIndex;
	d_mband_sparse_output_coosc.colIndex = d_mband_sparse_output_trans.rowIndex;
	d_mband_sparse_output_coosc.colPtr = d_mband_sparse_output_trans.rowPtr;
	
	if(USE_OVERLAP_OPTI){
		// Removing the overlap part of band sparse output with dia dia output
		move_overlap_of_csc_in_dia(d_mband_sparse_output_coosc, d_dia_dia_output_data, d_dia_dia_output_offset_indices, band_sparse_diadia_overlap_time, band_sparse_diadia_overlap_perc);
		//printf("OVERLAP MSP DIA PERC %.2f TIME %.2f\n", band_sparse_diadia_overlap_perc, band_sparse_diadia_overlap_time);
		cudaDeviceSynchronize();
	}

	// Verifying the final output
	cudaDeviceSynchronize();
	if(VERIFY)
		match_consolidated_output(d_dia_dia_output_data, d_dia_dia_output_offsets, h_dia_dia_output_offset_count, 
								d_sparse_mband_output, d_mband_sparse_output_coosc, d_cusparse_sparse_sparse_output, d_C_full);
	cudaDeviceSynchronize();


	// *********************************
	// Merge with out conversion
	// *********************************
	coosr_matrix d_merged_output;
	merge_with_out_conversion_to_3piece(cusparse_handle, d_sparse_mband_output, d_mband_sparse_output_coosc, d_cusparse_sparse_sparse_output, d_merged_output, 
		output_3piece_consolidation_time, band_sparse_output_coosc2coosr_unsorted_time);
	if(DEBUG) printf("DONE QUASI SORTEDOUTPUT CONSOLIDATION\n");
	
	/*
	// *********************
	// CUSPARSE DIA DIA MODULE
	// *********************
	perform_cusparse_coosr_coosr(cusparse_handle , d_A_dia_coosr, d_B_dia_coosr, d_cusparse_dia_dia_output, cusparse_dia_dia_preprocessing_time , cusparse_dia_dia_processing_time);


	// *********************
	// CUSPARSE SPARSE DIA MODULE
	// *********************
	perform_cusparse_coosr_coosr(cusparse_handle , d_A_rowcsr, d_B_dia_coosr, d_cusparse_sparse_dia_output, cusparse_sparse_dia_preprocessing_time , cusparse_sparse_dia_processing_time);


	// *********************
	// CUSPARSE DIA SPARSE MODULE
	// *********************
	perform_cusparse_coosr_coosr(cusparse_handle , d_A_dia_coosr, d_B_rowcsr, d_cusparse_dia_sparse_output, cusparse_dia_sparse_preprocessing_time , cusparse_dia_sparse_processing_time);


	// *********************
	// CUSPARSE (SPARSE+DIA) SPARSE MODULE
	// *********************
	perform_cusparse_coosr_coosr(cusparse_handle , d_A_full, d_B_rowcsr, d_cusparse_full_sparse_output, cusparse_full_sparse_preprocessing_time , cusparse_full_sparse_processing_time);
	cudaDeviceSynchronize();


	// *********************
	// THREE DIVISION CONSOLIDATION
	// *********************

	coosr_matrix d_temp_outputs[2];
	coosr_matrix d_temp_final_coosr_output;
	d_temp_outputs[0] = d_sparse_mband_output;
	d_temp_outputs[1] = d_cusparse_full_sparse_output;
	float temp_move_time=0, temp_compress_time=0;
	
	// Merging outputs generate by Dia*Dia, SparseXDia, DiaXSparse
	move_overlap_of_anycsr_in_csr_and_compress(d_sparse_mband_output,  d_cusparse_full_sparse_output, d_temp_final_coosr_output, temp_move_time, temp_compress_time);
	ouptut_2piece_consolidation_time = temp_move_time + temp_compress_time;
	*/
	cusparseDestroy(cusparse_handle);

	}

	// timings
	// -----------
	float cusparse_time = cusparse_preprocessing_time + cusparse_processing_time;
	
	split_A_matrix_time = min(split_A_matrix_time, split_B_matrix_time);
	split_B_matrix_time = split_A_matrix_time;

	split_B_coosr2coosc_time = split_B_coosr2coosc_timer.Elapsed();
	float split_time = split_A_matrix_time + split_B_matrix_time + split_B_coosr2coosc_time;
	
	dia_dia_preprocessing_time = dia_dia_preprocessing_timer.Elapsed();
	dia_dia_processing_time = dia_dia_processing_timer.Elapsed();
   	float dia_dia_time = dia_dia_preprocessing_time + dia_dia_processing_time;

   	sparse_band_preprocessing_time = sparse_band_preprocessing_timer.Elapsed();
   	sparse_band_processing_time = sparse_band_processing_timer.Elapsed();
	float sparse_band_time = sparse_band_preprocessing_time + sparse_band_processing_time + sparse_band_merge_time + sparse_band_diadia_overlap_time;

	band_sparse_preprocessing_time = band_sparse_preprocessing_timer.Elapsed();
	band_sparse_processing_time = band_sparse_processing_timer.Elapsed();	

	float band_sparse_coosc_time = band_sparse_preprocessing_time + band_sparse_processing_time + band_sparse_merge_time + band_sparse_diadia_overlap_time ;
	float band_sparse_unsorted_time =  band_sparse_coosc_time + band_sparse_output_coosc2coosr_unsorted_time;

	float sparse_sparse_time = 	sparse_sparse_preprocessing_time + sparse_sparse_processing_time  + sparse_sparse_diadia_overlap_time;

	float quasi_unsorted_split_time = 2*split_A_matrix_time + split_B_coosr2coosc_unsorted_time;
	float quasi_unsorted_time = quasi_unsorted_split_time + dia_dia_time  + sparse_band_time + band_sparse_unsorted_time +
								  sparse_sparse_time + output_3piece_consolidation_time;


	float cusparse_dia_dia_time = cusparse_dia_dia_preprocessing_time + cusparse_dia_dia_processing_time;
	float cusparse_sparse_dia_time = cusparse_sparse_dia_preprocessing_time + cusparse_sparse_dia_processing_time;
	float cusparse_dia_sparse_time = cusparse_dia_sparse_preprocessing_time + cusparse_dia_sparse_processing_time + sparse_band_diadia_overlap_time;
	float cusparse_full_sparse_time = cusparse_full_sparse_preprocessing_time + cusparse_full_sparse_processing_time;

	float no_conv_4div_time = 2*split_A_matrix_time + dia_dia_time  + sparse_band_time + cusparse_dia_sparse_time + sparse_sparse_time 
								+ output_3piece_consolidation_time;

	float no_conv_3div_time = 2*split_A_matrix_time + dia_dia_time  + sparse_band_time + cusparse_full_sparse_time 
								+ ouptut_2piece_consolidation_time;


	// If sparse nnz is zero it means this is a band.
	if(d_A_rowcsr.count == 0){
		quasi_unsorted_time = 2*split_A_matrix_time + dia_dia_time;
		no_conv_3div_time = quasi_unsorted_time;
		no_conv_4div_time = quasi_unsorted_time;
	}
	
	float speed_up = ((float)cusparse_time)/quasi_unsorted_time;	
	
	// other properties {merged_block_count}
	if(!FOR_ANALYSIS){
		printf("----------------------------------------------\n");
		printf("SPEED UP             %.3f\n",speed_up);
		printf("SPLITTING TIMING     %.3f = (%.3f + %.3f + %.3f)\n",split_time, split_A_matrix_time, split_B_matrix_time, split_B_coosr2coosc_time );
		/*
		printf("DIA DIA TIMINGSS     %.3f = (%.3f + %.3f )\n", dia_dia_time, dia_dia_preprocessing_timer.Elapsed(), dia_dia_processing_timer.Elapsed() );
		printf("SPARSE BAND TIME     %.3f = (%.3f + %.3f + %.3f )\n",sparse_band_time , sparse_band_preprocessing_timer.Elapsed(), sparse_band_processing_timer.Elapsed(), sparse_band_merge_time);
		printf("BAND SPARSE TIME     %.3f = (%.3f + %.3f + %.3f + %.3f)\n",band_sparse_time , band_sparse_preprocessing_timer.Elapsed(), band_sparse_processing_timer.Elapsed(), band_sparse_merge_time, band_sparse_output_coosc2coosr_timer.Elapsed());
		printf("SPARSE SPARSE TI     %.3f = (%.3f + %.3f + %.3f)\n", sparse_sparse_time, sparse_sparse_preprocessing_time, sparse_sparse_processing_time, sparse_sparse_csr2coo_conv_timer.Elapsed() );
		printf("CONSOLIDATION TI     %.3f\n", output_consolidation_time);
		*/
		printf("CUSPARSE TIMINGS    |%.3f| = (%.3f + %.3f)\n", cusparse_time, cusparse_preprocessing_time, cusparse_processing_time);
		printf("QUASI UNSORTED TMNG |%.3f| = (%.3f + %.3f + %.3f + %.3f + %.3f + %.3f + %.3f)\n",
			quasi_unsorted_time, quasi_unsorted_split_time - split_B_coosr2coosc_unsorted_time, split_B_coosr2coosc_unsorted_time , dia_dia_time  , sparse_band_time , band_sparse_unsorted_time, sparse_sparse_time , output_3piece_consolidation_time);

		printf("DIAGONAL TIMINGSSSS |%.3f %.3f, %.3f|\n", dia_dia_time, dia_dia_preprocessing_time, dia_dia_processing_time);
		printf("SPARSE BAND TIME     %.3f = (%.3f + %.3f + %.3f )\n",sparse_band_time , sparse_band_preprocessing_time, sparse_band_processing_time, sparse_band_merge_time);
		printf("BAND SPARSE TIME     %.3f = (%.3f + %.3f + %.3f + %.3f)\n",band_sparse_unsorted_time , band_sparse_preprocessing_time, band_sparse_processing_time, band_sparse_output_coosc2coosr_unsorted_time, band_sparse_merge_time);
		printf("CON(USRT3 USRT2)    |%.3f, %.3f|\n", output_3piece_consolidation_time, ouptut_2piece_consolidation_time);
		printf("NO CNVRSN 4div TIMG |%.3f| cusparse dia_sp  |%.3f| \n", no_conv_4div_time, cusparse_dia_sparse_time);
		printf("NO CNVRSN 3div TIMG |%.3f| cusparse full_sp |%.3f|\n", no_conv_3div_time, cusparse_full_sparse_time);
		printf("DIA DIA (QS ,CU)    |%.3f , %.3f| = (%.3f + %.3f)|\n",dia_dia_time, cusparse_dia_dia_time, cusparse_dia_dia_preprocessing_time, cusparse_dia_dia_processing_time);
		printf("SP DIA (QS CU)      |%.3f , %.3f| = (%.3f + %.3f)|\n",sparse_band_time, cusparse_sparse_dia_time, cusparse_sparse_dia_preprocessing_time, cusparse_sparse_dia_processing_time);
		printf("DIA_SP(UNSRTD CU)   |, %.3f , %.3f| = (%.3f + %.3f)|\n",band_sparse_unsorted_time , cusparse_dia_sparse_time, cusparse_dia_sparse_preprocessing_time, cusparse_dia_sparse_processing_time);

		printf("----------------------------------------------\n");

	}else{

		char* mat_name;
		get_name(argv[2], mat_name);
		if(FROM_FILE)
			printf("$ %s,", mat_name);
		else
			printf("$%d-%.2f-%.2f-%.2f,", d_A_rowcsr.rows, nnz_perc, band_perc, BAND_OCCUPANCY);

		printf("%.5f,%.2f,%.2f,%.2f,%.2f,%.2f,%.2f,%.2f,%.2f,%.2f,%.2f,%.2f,%.2f,%.2f,%.2f,%.2f,%.2f,", 
			speed_up,
			cusparse_time, quasi_unsorted_time, no_conv_4div_time, no_conv_3div_time, 
			sparse_band_time, cusparse_sparse_dia_time, band_sparse_unsorted_time, cusparse_dia_sparse_time,
			sparse_sparse_time, split_A_matrix_time, split_B_coosr2coosc_time, split_B_coosr2coosc_unsorted_time, 
			band_sparse_output_coosc2coosr_unsorted_time,
			output_3piece_consolidation_time, dia_dia_time, cusparse_dia_dia_time);

		
		printf("%d,%d,%d,%d,%d,", d_C_full.count, d_cusparse_dia_dia_output.count, d_cusparse_sparse_dia_output.count, d_cusparse_dia_sparse_output.count, d_cusparse_sparse_sparse_output.count);

		
		printf("%.2f,%.2f,",
			dia_dia_preprocessing_time, dia_dia_processing_time);

		printf("%.2f,%.2f,%.2f,",
			sparse_band_preprocessing_time, sparse_band_processing_time, sparse_band_merge_time);
		printf("%.2f,%.2f,%.2f,",
			band_sparse_preprocessing_time, band_sparse_processing_time, band_sparse_merge_time);
		printf("%.2f,%.2f\n",
			sparse_sparse_preprocessing_time, sparse_sparse_processing_time);

		printf("!%.3f,%.3f,%.3f,%d\n\n",speed_up, cusparse_time, quasi_unsorted_time, sparse_band_output_count+band_sparse_output_count );

	}


	return 0;
}

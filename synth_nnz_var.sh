
# Common Properties
DEVICE=0
USE_OVERLAP=0
FOR_ANALYSIS=1
VERIFY=0
DEBUG=0


# Synthetic matrix properites
rows=16000
nnz_perc=0.5
band_occupancy=90
BAND_PERC=('70' '75' '80' '85' '90' '95' )

# 0=random 1=row_block 2=col_block
SPARSE_FILL_METHOD=1

result_dir="./results/synth_nnzvar-"
hyphen="-"
result=$result_dir$rows$hyphen$nnz_perc$hyphen$band_occupancy$hyphen$SPARSE_FILL_METHOD

if [ -f "$result" ]
then
	rm $result
else
	touch $result
fi

sample_count=${#BAND_PERC[@]}

for ((  j = 0 ;  j < sample_count;  j++  ))
	do
		./quasi "0" "$rows" "$nnz_perc" "${BAND_PERC[j]}" "$band_occupancy"  "$SPARSE_FILL_METHOD" "$DEVICE" "$FOR_ANALYSIS"  "$USE_OVERLAP" "$VERIFY" "$DEBUG" >> "$result"
	done
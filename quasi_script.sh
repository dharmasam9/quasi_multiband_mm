#!/bin/bash
dirr="./matrices/quasi_multiband/"
result="./results/fin_quasi_multiband"


# configuration 
DEVICE=0
USE_OVERLAP=0
FOR_ANALYSIS=1
VERIFY=0
DEBUG=0
BAND_OCCUPANCY=('60')
ONLY_MAIN_BAND=0


# getting file values if f
files=""
matrix_count=0
sample_count=${#BAND_OCCUPANCY[@]}

if [ -d $dirr ]; then
    for f in "$dirr"/*; 
    	do
	    	if [ -f $f ]; then
		        files="$files $f"
		        matrix_count=$(($matrix_count + 1))
			fi
		done
else
    echo $dirr " is Not a directory"
fi

matrices=($files)
# running the alog with different dia fill percentages
for ((  i = 0 ;  i < matrix_count;  i++  ))
do
	for ((  j = 0 ;  j < sample_count;  j++  ))
		do
			./quasi "1" "${matrices[i]}" "${BAND_OCCUPANCY[j]}" "$ONLY_MAIN_BAND" "$DEVICE" "$FOR_ANALYSIS"  "$USE_OVERLAP" "$VERIFY" "$DEBUG">> "$result"
	    #echo "1" "${matrices[i]}" "${FILL_PERC[j]}" "$DEVICE" "$FOR_ANALYSIS" "$VERIFY" "$DEBUG"
	    
		done
	#echo $'\r' >> "$result"
done

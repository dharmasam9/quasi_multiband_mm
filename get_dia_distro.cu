#include "common.cu"
/*
	Program to find the non zero diagonal distribution
*/

int main(int argc, char  *argv[])
{	

	cudaSetDevice(0);
    cudaGetDeviceProperties(&deviceProp, 0);

	dim3 grids,blocks;

	GpuTimer find_nnz_timer;


	cusp::coo_matrix<int, float, cusp::host_memory> cusp_A;
	char* fn1 = argv[1];
	cusp::io::read_matrix_market_file(cusp_A, fn1);

	coosr_matrix d_A_full, d_A_coosr;
	generate_coosr_from_cusp_coo(cusp_A , d_A_full );
	allocate_and_transfer_coosr_host_to_device(d_A_full , d_A_coosr);


	// Finding nnz of all diagonals
	int all_dia_count = d_A_coosr.rows+d_A_coosr.cols-1;
	int* d_nnz_in_dia, *h_nnz_in_dia;

	allocate_host_int_array(&h_nnz_in_dia, all_dia_count);
	allocate_and_memset_device_int_array(&d_nnz_in_dia, all_dia_count);

	// finding nnz of diagonals
	find_dimensions_of_blocks_and_threads(d_A_coosr.count, grids, blocks);
	find_nnz_timer.Start();
		//print_grids_and_blocks(grids, blocks);
		find_nnz_in_dia_kernel<<<grids,blocks>>>(d_A_coosr.rowIndex, d_A_coosr.colIndex, d_A_coosr.rows, d_nnz_in_dia, d_A_coosr.count);
		cudaDeviceSynchronize();
		transfer_int_array_device_to_host(d_nnz_in_dia, h_nnz_in_dia, all_dia_count);
	find_nnz_timer.Stop();

	printf("%s\n",fn1 );
	int size = strlen(fn1);
	int pivot = 0;
	for (int i = size-1-4; i >0; i--)
	{
		if(fn1[i] == '/'){
			pivot = i+1;
			break;
		}
	}

	int name_size = size - 4 - pivot;
	char name[name_size+1];
	name[name_size] = '\0';

	for (int i = pivot; i < pivot+name_size; ++i)
	{
		name[i-pivot] = fn1[i];
	}

	char* base_dir = "./dia_info/";
	int base_dir_len = strlen(base_dir);

	char path[base_dir_len+name_size+1];
	path[base_dir_len+name_size] = '\0';

	for (int i = 0; i < base_dir_len; ++i)
	{
		path[i] = base_dir[i];
	}

	for (int i = 0; i < name_size; ++i)
	{
		path[base_dir_len+i] = name[i];
	}
	printf("%s\n", path);

	
	FILE *fp = fopen(path, "w");
	int offset;
	float perc_fill;

	fprintf(fp,"dia_count, offset\n");
	for (int i = 0; i < all_dia_count; ++i)
	{
		if(h_nnz_in_dia[i] != 0){
			offset = i-(d_A_coosr.rows-1);
			perc_fill = (float) (h_nnz_in_dia[i]*100)/ d_A_coosr.rows;
			fprintf(fp, "%d,%.3f,%d\n", offset, perc_fill, h_nnz_in_dia[i]);
		}
	}
	
	/*
	for (int i = 0; i < all_dia_count; ++i)
	{
		if(h_nnz_in_dia[i] == 0){
			offset = i-(d_A_coosr.rows-1);
			perc_fill = (float) (h_nnz_in_dia[i]*100)/ d_A_coosr.rows;
			fprintf(fp, "%d,%.3f,%d\n", offset, perc_fill, h_nnz_in_dia[i]);
		}
	}

	
	int row,col;
	for (int i = 0; i < cusp_A.num_entries; ++i)
	{
		row = cusp_A.row_indices[i];
		col = cusp_A.column_indices[i];
		offset = col - row;

		if(offset >= -1 && offset <= 1){

		}else{
			fprintf(fp, "row %5d col %5d\n", row,col);
		}

	}
	*/


	fclose(fp);

	return 0;
}
dirr="../testmatrices/"

if [ -d $dirr ]; then
    for f in "$dirr"/*; 
    	do
	    	if [ -f $f ]; then
		        files="$files $f"
		        matrix_count=$(($matrix_count + 1))
			fi
		done
else
    echo $dirr " is Not a directory"
fi


matrices=($files)
# running the alog with different dia fill percentages
for ((  i = 0 ;  i < matrix_count;  i++  ))
do

	./a.out "${matrices[i]}"
done
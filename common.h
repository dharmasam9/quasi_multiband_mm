#ifndef _COMMON_H
#define _COMMON_H

// C/C++ related
#include <stdio.h>
#include <stdlib.h>
#include <sys/time.h>
#include <math.h>

// For timing GPU
#include "timer.h"

// CUDA related
#include <cuda_runtime.h>

// CUSPARSE
#include <cusparse_v2.h>

// THRUST
#include <thrust/scan.h>
#include <thrust/reduce.h>
#include <thrust/device_ptr.h>
#include  <thrust/sort.h>
#include <thrust/merge.h>
#include <thrust/functional.h>
#include <thrust/execution_policy.h>


// CUSP RELATED
#include <cusp/coo_matrix.h>
#include <cusp/io/matrix_market.h>


// normal block matrix
typedef struct block_matrix{
	int rows;
	int cols;
	float* data;
}block_matrix;


// Structure for storing diagonal band in matrix
typedef struct band_matrix{
	int rows;
	int cols;
	int count;
	float* data;
	int* offsets;
	int left_offset;
	int right_offset;
}band_matrix;

// Structure for storing matrix as COO+CSR
typedef struct coosr_matrix{
	int rows;
	int cols;
	int count;
	float* data;
	int* rowIndex;
	int* colIndex;
	int* rowPtr;
}coosr_matrix;

// Structure for storing matrix as COO+CSC
typedef struct coosc_matrix{
	int rows;
	int cols;
	int count;
	float* data;
	int* rowIndex;
	int* colIndex;
	int* colPtr;
}coosc_matrix;

// Independent kernels
__global__
void fill_array_with_value_kernel(int* array , int value, int size);
__global__
void fill_array_with_tid_kernel(int* indices, int size);
__global__
void fill_array_with_tid_plus_base_offset(int* offsets, int base_offset, int size);
__global__
void set_zero_for_unset_flag_in_psum(int* psum_array, int* index_array, int size);
__global__
void multiply_array_by_minus_one(int* array,int size);
__global__
void mark_flags(int* orig_array, int* bool_array, int size);

__global__
void gather_marked_indices(int* scanned_flags, int* marked_indices, int size);

// Independent functions
void print_flags_in_csr_format(int* d_flags, int size, int rows, int* d_rowPtr);
void merge_coosr_matrices(coosr_matrix* d_coosr_matrices, int num_mats, coosr_matrix &d_merged_coosr_output, float &time_taken);


// SPLITTING related
__global__
void find_nnz_in_dia_kernel(int* row_indices, int* column_indices, int rows, int* nnz_in_dia, int size);
void find_total_in_dia(int* total_in_dia, int rows, int cols);
void pluck_band_using_cumulative_method(int* nnz_in_dia, int* total_in_dia, int A_rows, int A_cols, int &l_index, int &r_index, float FILL_PERC);
void pluck_band_using_stencil_average_method(int* nnz_in_dia, int* total_in_dia, int A_rows, int A_cols, int &l_index, int &r_index, float FILL_PERC);

__global__
void fill_band_data_with_flags_kernel(int rows, float* csr_data, int* csr_rowIndex, int* csr_colIndex,
									float* band_data, int* flags, int left_index, int right_index, int size);
__global__
void fill_coo_data_kernel(float* data, int* rowIndex, int* colIndex,
						float* sparse_data, int* sparse_rowIndex, int* sparse_colIndex,int* band_flags, int size);
void split_matrix_in_gpu(cusparseHandle_t cusparse_handle, coosr_matrix d_A_coosr, float* &d_band_data, int* &d_band_offsets, int* &d_band_offset_indices, 
		int &num_dias, band_matrix* &d_band_mats, int &num_bands, coosr_matrix &d_sparse_mat, coosr_matrix &d_dia_in_coosr_mat, int &nz_diagonals, float &split_time, float BAND_OCCUPANCY, int USE_MAIN_ONLY);
// DIA DIA related
void dia_dia_preprocessing(int rows, int cols, int* d_A_dia_offsets, int A_num_dias, int* d_B_dia_offsets, int B_num_dias, int &dia_count, int* &d_output_offsets, float* &d_output_data, int* &d_output_offset_indices);
__global__
void dia_dia_multiplication_kernel(float* d_A_data, int* d_A_offsets,int A_rows,int A_cols,float* d_B_data,int* d_B_offsets,int B_rows,int B_cols,int* d_output_offset_indices, float* d_output_data);

// SPARSE BAND related
__global__
void set_out_of_bound_element_flags_to_zero(int* colIndex, int cols, int* block_start_flags, int left_offset, int right_offset, int size);
__global__
void find_block_start_positions(int* rowIndex, int* colIndex, int left_offset, int right_offset, int* block_start_flags, int size);
__global__
void gather_block_start_positions(int* d_A_block_start_pos_flags, int* d_block_start_positions, int size);
__global__
void compute_uniband_blockCount_and_base_indices(int* d_A_block_start_positions,int* d_A_rowcsr_colIndex, int* d_A_rowcsr_rowIndex, int A_rowcsr_count, int l_index, int r_index, int* d_A_blockCount, int* d_A_base_rowIndex, int* d_A_base_colIndex, int B_cols, int size);
__global__
void compute_blockCount_and_base_indices(int* d_A_block_start_positions,int* d_A_rowcsr_colIndex, int* d_A_rowcsr_rowIndex, int* d_A_rowcsr_rowPtr, int A_rowcsr_count, int l_index, int r_index, int* d_A_blockCount, int* d_A_base_rowIndex, int* d_A_base_colIndex, int B_cols, int size);
__global__
void construct_sparse_band_output_rowPtr(int* d_A_blockCount, int* d_A_block_start_positions_rowPtr, int* d_sparse_band_output_rowPtr, int A_rows);
__global__
void mark_last_element_in_each_row(int* d_A_blockCount, int* d_output_colIndex, int  merged_block_count);
__global__
void fill_output_column_and_row_indices(int* d_A_blockCount, int* d_block_mappings, int* d_A_base_rowIndex, int* d_A_base_colIndex, int* d_output_rowIndex, int* d_output_colIndex, int h_sparse_band_output_nnz);

void sparse_band_preprocessing(coosr_matrix d_A_rowcsr, band_matrix d_A_band_mat,
						int A_rows, int A_cols, int B_cols,
						int* &d_output_rowPtr, int* &d_output_rowIndex, int* &d_output_colIndex, int &h_output_nnz,
						int* &d_A_blockCount, int* &d_A_base_rowIndex, int* &d_A_base_colIndex, int* &d_A_block_start_positions_rowPtr);

__global__
void rowcsr_band_matrix_multiplication_optimized_kernel(float* d_A_rowcsr_data,int* d_A_rowcsr_colIndex, int* d_A_rowcsr_rowIndex, int* d_A_rowcsr_rowPtr,
											int* d_A_blockCount, int* d_A_blockCount_rowPtr, int* d_A_baseColIndex,
											int A_rows, int A_cols,
											float* d_B_band_data,int* d_B_band_indices,
											int B_rows, int B_cols,
											int A_nnz, int B_band_count,
											float* d_output_rowcsr_values,int* d_output_rowcsr_colIndex,int* d_output_rowcsr_rowPtr);



// experimental
__global__
void compute_index_of_last_outofbound_element_in_row(int* colIndex, int* rowPtr, int last_allwd_colIndex, int* last_proper_csr_index, int size);

// BAND SPARSE related
__global__
void trasnspose_band_data_inplace(float* d_orig_data, float* d_new_data, int* d_orig_offsets, int rows, int size);
void transpose_band_matrix(band_matrix &band_mat);

// OVERLAP Optimization related
__global__
void move_overlap_of_coo_in_dia_kernel(int* non_overlap_flags, int rows, float* sparse_data, int* rowIndex, int* colIndex, float* dia_data, int* dia_offset_indices, int size);
__global__
void gather_marked_indices_kernel(int* scanned_flags, int* marked_indices, int size);
__global__
void fill_non_compressed_data_kernel(int* indices, float* orig_data, int* orig_rowIndex, int* orig_colIndex, float* cmprsd_data, int* cmprsd_rowIndex, int* cmprsd_colIndex, int size);
__global__
void fill_non_compressed_rowPtr_kernel(int* scanned_flags, int* orig_rowPtr, int* cmprsd_rowPtr, int size);

void move_overlap_of_csc_in_dia(coosc_matrix &d_mband_sparse_output, float* d_dia_dia_output_data, int* d_dia_dia_output_offset_indices, float &msp_dia_overlap_time, float &msp_dia_overlap_perc);
void move_overlap_of_csc_in_csr(coosc_matrix &d_mband_sparse_output, coosr_matrix &d_sparse_mband_output);

__global__
void move_overlap_of_coo_in_csr_kernel(int* non_overlap_flags, float* coo_data, int* coo_rowIndex, int* coo_colIndex, float* csr_data, int* csr_colIndex, int* csr_rowPtr, int size);
void move_overlap_of_csc_in_csr(coosc_matrix &coosc_mat, coosr_matrix &coosr_mat, float &time_taken, float &overlap_perc);

// CONSOLIDATION related
__global__
void compute_csr_merge_output_rowcsr_rowPtr(int* A_rowPtr, int* B_rowPtr, int* C_rowPtr, int rows);
__global__
void place_first_csr_in_output_csr(float* A_data, int* A_rowIndex, int* A_colIndex, int A_count,
									int* B_colIndex,int* B_rowPtr, int B_count,
									float* C_data, int* C_rowIndex, int* C_colIndex, int* C_rowPtr);
__global__
void place_second_csr_in_output_csr(float* A_data, int* A_rowIndex, int* A_colIndex, int A_count,
									int* B_colIndex,int* B_rowPtr, int B_count,
									float* C_data, int* C_rowIndex, int* C_colIndex, int* C_rowPtr);

void merge_two_csr_reps( coosr_matrix d_A_rowcsr, coosr_matrix d_B_rowcsr, coosr_matrix &d_merged_rowcsr);

__global__
void mark_unique_occurence_in_sorted_list(int* rowIndex, int* colIndex, int* flag, int size);

__global__
void compress_consolidated_output(int* indices, float* read_data, int* read_rowIndex, int* read_colIndex, float* write_data, int* write_rowIndex, int* write_colIndex, int size);

__global__
void compute_compressed_output_rowPtr(int* uncompressed_rowPtr, int* compressed_count, int* compressed_rowPtr, int A_rows);

void group_unique_matrix_elements(coosr_matrix d_merged_rowcsr, coosr_matrix &d_unique_rowcsr);

// CONSOLIDATION USING MERGING
__global__
void compress_row_and_col_positions(int* rowIndex, int* colIndex, int cols, int* indices, int size);
__global__
void expand_positions_from_row_and_col(int* indices, int cols, int* rowIndex, int* colIndex, int size);
__global__
void mark_first_unique_elements_in_sorted_array(int* flags, int* keys, int size);
__global__
void compress_merged_coosr_output(int* cmprd_indices, float* read_data, int* read_indices, float* write_data, int* write_indices, int size);
void merge_coosr_matrices_optimized(cusparseHandle_t cusparse_handle, coosr_matrix* d_coosr_matrices, int num_mats, coosr_matrix &d_merged_coosr_output, float &time_taken);

// CONSOLIDATION USING SEGMENTED SORT
void get_coosr_to_coosc_unsorted_conv_time(cusparseHandle_t cusparse_handle, coosr_matrix coosr_mat, float &time_taken);
__global__
void shuffle_matrix_data_using_rowIndices(int* shuffled_indices, float* read_data, int* read_rowIndex, int* read_colIndex, float* write_data, int* write_rowIndex, int* write_colIndex, int size );
void merge_with_out_conversion_to_2piece(cusparseHandle_t cusparse_handle, coosr_matrix d_sparse_mband_output, coosc_matrix d_mband_sparse_output_coosc, coosr_matrix d_sparse_sparse_output, coosr_matrix &d_merged_output, float &merge_time, float &sort_time);
void merge_with_out_conversion_to_3piece(cusparseHandle_t cusparse_handle, coosr_matrix d_sparse_mband_output, coosc_matrix d_mband_sparse_output_coosc, coosr_matrix d_sparse_sparse_output, coosr_matrix &d_merged_output, float &merge_time, float &sort_time);

// Corresponding CPU codes
void multiply_dia_dia_matrices(float* h_A_data, int* h_A_offsets, int A_num_dias,
							  float* h_B_data, int* h_B_offsets, int B_num_dias,
							  int A_rows, int A_cols, int B_cols, float* output);

void multiply_coosr_band_matrices(coosr_matrix A, band_matrix B, float* output);

void multiply_band_coosc_matrices(band_matrix A, coosc_matrix B, float* output);

void multiply_rowcsr_colcsr_matrices(coosr_matrix A,coosc_matrix B,float* output);

// MATCHING OUTPUT
void match_dia_dia_output(int A_rows, int A_cols, int B_cols,
						   float* d_A_dia_data, int* d_A_dia_offsets, int A_num_dias,
						   float* d_B_dia_data, int* d_B_dia_offsets, int B_num_dias,
						   float* d_dia_dia_output_data, int* d_dia_dia_output_offsets, int C_num_dias);

void match_coosr_dia_output(coosr_matrix d_A_sparse, band_matrix* d_B_bands, int B_num_bands, coosr_matrix d_output);

void match_dia_coosr_output(band_matrix* d_A_bands, coosc_matrix d_B_sparse, int A_num_bands, coosr_matrix d_output);

void match_sparse_sparse_output(coosr_matrix d_A_sparse, coosc_matrix d_B_sparse, coosr_matrix d_output);

void match_consolidated_output(float* d_dia_dia_output_data, int* d_dia_dia_output_offsets, int num_dias, 
	coosr_matrix d_sparse_band, coosc_matrix d_band_sparse, coosr_matrix d_sparse_sparse, coosr_matrix d_coosr_reference);

// Synthetic matrix generation;
int fill_synthetic_matrix(cusp::coo_matrix<int, float, cusp::host_memory> &synth, int rows, int cols, float nnz_perc, float band_perc, float sparse_perc, int experiment, int seed);

// Verficiation related modules
void traditional_matrix_multiplication(block_matrix A, block_matrix B, block_matrix &C);
void traditional_matrix_addition(block_matrix A, block_matrix B, block_matrix &C);
void are_matrices_equal(block_matrix mat1, block_matrix mat2);


// Conversions
void generate_coosr_from_cusp_coo(cusp::coo_matrix<int, float, cusp::host_memory> cusp_mat, coosr_matrix &coosr_mat);
void band_to_block(band_matrix band_mat, block_matrix &block_mat);
void coosr_to_block(coosr_matrix coosr_mat, block_matrix &block_mat);
void coosc_to_block(coosc_matrix coosc_mat, block_matrix &block_mat);
void coosr_to_coosc_in_device(cusparseHandle_t cusparse_handle, coosr_matrix d_coosr_mat, coosc_matrix &d_coosc_mat, float &time_taken);



// PRINT functions
void print_block_matrix(block_matrix mat);
void print_band_matrix(band_matrix mat);
void print_coosr_matrix(coosr_matrix mat);
void print_coosc_matrix(coosc_matrix mat);

// Allocating Empty coosr matrix on device
void allocate_and_memset_device_coosr_matrix(int rows, int cols, int count, coosr_matrix &coosr_mat);
void allocate_and_memset_device_coosc_matrix(int rows, int cols, int count, coosc_matrix &coosc_mat);
void allocate_device_coosr_matrix(int rows, int cols, int count, coosr_matrix &coosr_mat);
void allocate_device_coosc_matrix(int rows, int cols, int count, coosc_matrix &coosc_mat);

// Allocating and transferring from filled host to device Structure functions
void allocate_and_transfer_band_host_to_device(band_matrix h_mat, band_matrix &d_mat);
void allocate_and_transfer_coosr_host_to_device(coosr_matrix h_mat, coosr_matrix &d_mat);
void allocate_and_transfer_coosc_host_to_device(coosc_matrix h_mat, coosc_matrix &d_mat);

// Allocating and transferring from filled device to host Structure functions
void allocate_and_transfer_band_device_to_host(band_matrix d_mat, band_matrix &h_mat);
void allocate_and_transfer_coosr_device_to_host(coosr_matrix d_mat, coosr_matrix &h_mat);
void allocate_and_transfer_coosc_device_to_host(coosc_matrix d_mat, coosc_matrix &h_mat);

// CUSPARSE module
void perform_cusparse_coosr_coosr(cusparseHandle_t cusparse_handle, coosr_matrix A, coosr_matrix B, coosr_matrix &C, float &preprocessing_time, float &processing_time);
void perform_cusparse_coosr_coosc(cusparseHandle_t cusparse_handle, coosr_matrix A, coosc_matrix B, coosr_matrix &C, float &preprocessing_time, float &processing_time);


// Device helper
void find_dimensions_of_blocks_and_threads(int total_threads, dim3 &grids, dim3 &blocks);

void allocate_host_float_array(float** h_array, int size);
void allocate_host_int_array(int** h_array, int size);
void allocate_device_float_array(float** d_array, int size);
void allocate_device_int_array(int** d_array, int size);
void memset_device_float_array(float* d_array, float size);
void memset_device_int_array(int* d_array, int size);
void allocate_and_memset_device_float_array(float** d_array, int size);
void allocate_and_memset_device_int_array(int** d_array, int size);
void allocate_and_transfer_float_array_host_to_device(float* h_array, float** d_array, int size);
void allocate_and_transfer_int_array_host_to_device(int* h_array, int** d_array, int size);
void allocate_and_transfer_float_array_device_to_host(float* d_array, float** h_array, int size);
void allocate_and_transfer_int_array_device_to_host(int* d_array, int** h_array, int size);
void allocate_and_transfer_float_array_device_to_device(float* d_from_array, float** d_to_array, int size);
void allocate_and_transfer_int_array_device_to_device(int* d_from_array, int** d_to_array, int size);
void transfer_float_array_host_to_device(float* h_array, float* d_array, int size);
void transfer_int_array_host_to_device(int* h_array, int* d_array, int size);
void transfer_float_array_device_to_host(float* d_array, float* h_array, int size);
void transfer_int_array_device_to_host(int* d_array, int* h_array, int size);
void transfer_float_array_device_to_device(float* d_from_array, float* d_to_array, int size);
void transfer_int_array_device_to_device(int* d_from_array, int* d_to_array, int size);

// IO
void print_grids_and_blocks(dim3 grids, dim3 blocks);
void print_device_coosr_matrix(coosr_matrix d_A_rowcsr);
void print_device_coosc_matrix(coosc_matrix d_A_coosc);
void print_device_band_matrix(band_matrix d_A_band);

void print_device_float_array(float* array, int size);
void print_device_int_array(int* array, int size);
void print_host_float_array(float* array, int size);
void print_host_int_array(int* array, int size);
void print_split_matrices(coosr_matrix d_A_full, coosr_matrix d_A_rowcsr, band_matrix* d_B_bands, int num_bands);

// Utility
void get_name(char* full_name, char* &mat_name);

// timing related
double timer();

#endif

# Common Properties
DEVICE=0
USE_OVERLAP=0
FOR_ANALYSIS=1
VERIFY=0
DEBUG=0


# Synthetic matrix properites
nnz_perc=1
rows=14000
band_perc=95
band_occupancy=90
# 0=random 1=row_block 2=col_block
SPARSE_FILL_METHOD=0

num_data_points=10
strip_size=0.5


result_dir="./results/nnzperc_var-"
hyphen="-"
result=$result_dir$rows$hyphen$band_perc$hyphen$band_occupancy

<<COMMENT
if [ -f "$result" ]
then
	rm $result
else
	touch $result
fi
COMMENT

for ((  j = 0 ;  j < num_data_points;  j++  ))
	do
		./quasi "0" "$rows" "$nnz_perc" "$band_perc" "$band_occupancy" "$SPARSE_FILL_METHOD" "$DEVICE" "$FOR_ANALYSIS"  "$USE_OVERLAP" "$VERIFY" "$DEBUG" >> "$result"
		nnz_perc=$(echo "scale=2; ($nnz_perc+$strip_size)" | bc)
		
	done